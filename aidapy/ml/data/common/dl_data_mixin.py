""" This module contains MixIn utility class 
"""
from datetime import datetime
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from aidapy.aidafunc.load_data import load_data


class ReadDataMixIn(object):
    """ Mixin utility class to be able to read the 
    timeseries with the help of the AIDA data module.
    """
    def _download_data(self, download_vars):
        r"""Method to download the data from the web.

            Parameters
            ----------
            datavars : dict
                A dictionary with the arguments
                needed from the mission module

            Returns
            -------
            data : pandas array
                An pandas array with the inputs features as
                columns and the datetimes as rows.
        """
        #Reading variables
        mission = download_vars['mission']
        datetimes = download_vars['datetime']
        settings = download_vars['settings']
        features = download_vars['features']
        #Converting Datetime
        date_time_st = datetimes[0]
        date_time_end = datetimes[1]
        start_time = datetime.strptime(date_time_st, '%Y-%m-%d %H:%M:%S')
        end_time = datetime.strptime(date_time_end, '%Y-%m-%d %H:%M:%S')
        #Function to download the data
        dl_data = load_data(mission=mission,
                            start_time=start_time,
                            end_time=end_time, **settings)
        #Function to transform the data to pandas
        df = pd.DataFrame(dl_data['all1'].values,
                          index=dl_data['all1'].coords['time1'].values,
                          columns=dl_data['all1'].coords['products'].values)
        #The examined features
        data = df[features].copy()
        return data

        
