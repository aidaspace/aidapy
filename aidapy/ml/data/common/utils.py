
import numpy as np
import pandas as pd
import torch
from torch.utils.data import TensorDataset, DataLoader


def numpy_to_dataloader(input_, output_, batch_size=64):
    set_ = TensorDataset(torch.Tensor(input_), torch.Tensor(output_))
    return DataLoader(set_, batch_size=batch_size, shuffle=True)


def train_model(model, epochs, training, validation, criterion, optimizer,
                file_path=None, verbal=False):
    """Train the model, using validation set for best selection
    The model is trained on the training data. Each epoch, the model is than
    used to predict the validation data.
    The best iteration of the model is stored and returned after going through
    every epoch.
    If a file_path is given, the best model configuration is stored in a file.
    """
    min_val = np.inf
    best_loss = np.inf
    best_epoch = 0
    best_model = None
    for t in range(epochs):
        running_loss = 0
        model.train()
        for x, y in training:
            optimizer.zero_grad()
            y_pred = model(x)
            loss = criterion(y_pred, y)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
        if verbal:
            print('Epoch {}: Training loss: {}'.format(
                t, running_loss/len(training.dataset)))

        model.eval()
        with torch.no_grad():
            val_loss = 0
            for val_in, val_out in validation:
                y_pred = model(val_in)
                loss = criterion(val_out, y_pred)
                val_loss += loss.item()
            if verbal:
                print('Validation loss: ', val_loss / len(validation.dataset))
            if val_loss < min_val:
                min_val = val_loss
                best_loss = running_loss/len(training.dataset)
                best_epoch = t
                best_model = model.state_dict()
    if file_path is not None:
        torch.save(best_model, file_path)
    print('Best loss: {}, Min validation loss: {}, Epoch: {}'.format(
        best_loss, min_val, best_epoch))

    model.load_state_dict(best_model)


def run_model(model, data_in, file_path=None):
    if file_path is not None:
        model.load_state_dict(torch.load(file_path))
    model.eval()
    pred = model(torch.Tensor(data_in))
    return pred.detach().numpy()


def extract_continuous_intervals(table):
    r'''Check lookup table for time discontinuities
    output:
        Returns list of continouos times inside the lookup table
    '''
    lookup = pd.DataFrame(data=np.arange(table.shape[0]),
                          index=pd.to_datetime(table[:, 0]))
    lookup.index = pd.DatetimeIndex(lookup.index)
    # split = [g for n,g in lookup.groupby(pd.Grouper(freq='M')) if g.shape[0] != 0]

    min_size = 10
    timeseries = []

    # for month in split:
    series = lookup.index
    while len(series) > 0:
        # We can assume that the series starts from non-missing values, so the first diff gives sizes of continous intervals
        diff = pd.date_range(series[0], series[-1], freq='H').difference(
            series)
        if len(diff) > 0:
            if pd.Timedelta(
                    diff[0] - pd.Timedelta('1h') - series[0]) / pd.Timedelta(
                    '1h') > min_size:
                v1 = lookup.loc[series[0]][0]
                v2 = lookup.loc[diff[0] - pd.Timedelta('1h')][0]
                timeseries.append([v1, v2])
            if pd.Timedelta(
                    series[-1] - diff[-1] - pd.Timedelta('1h')) / pd.Timedelta(
                    '1h') > min_size:
                v1 = lookup.loc[diff[-1] + pd.Timedelta('1h')][0]
                v2 = lookup.loc[series[-1]][0]
                timeseries.append([v1, v2])
            diff = pd.date_range(diff[0], diff[-1], freq='H').difference(diff)
        else:
            # Only when diff is empty
            v1 = lookup.loc[series[0]][0]
            v2 = lookup.loc[series[-1]][0]
            timeseries.append([v1, v2])
        series = diff

    return np.array(timeseries)


def reformat_dtw_res(df, filename=None):
    '''Normalize the result from the dtw measure
    '''
    res = df.div(df.sum(axis=1), axis=0)

    shifts = np.array(['t+{}h'.format(i+1) for i in np.arange(res.shape[0])])
    res['Prediction'] = shifts.T
    res = res.set_index('Prediction')
    res.columns = ['{}h'.format(i) for i in res.columns]
    res = res.apply(lambda x: round(x, 3))
    if filename:
        res.to_csv('{}reformated_{}'.format(path, filename))
    return res