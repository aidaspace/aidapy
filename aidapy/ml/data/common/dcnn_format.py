""" This module contains MixIn utility class 
"""
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from aidapy.aidafunc.load_data import load_data


class DCNN_Format(object):
    """ Mixin utility class to be able to transform 
    the timeseries into input-output batches.
    """
    def _clean_data(self, data):
        r"""Removes the NaNs from the data.

            Parameters
            ----------
            data : pandas dataframe.
                An pandas arrays with NaN elements

            Returns
            -------
            clean : pandas dataframe
                A pandas dataframe without NaN elements

            errors : pandas dataframe
                A pandas dataframe with only the NaN elements
        """
        errors = data[data.isna().any(axis=1)]
        clean = data.dropna(axis=0)
        return clean, errors

    @staticmethod
    def _format_to_dcnn_input11(xraw, yraw, window, horizon):
        r"""Function to transform the data into batches

            Parameters
            ----------
            input_ : pandas array
                The pandas array with the input features

            output_ : pandas array
                The pandas array with the output/target feature

            window : int
                The time window to look in the past

            horizon : int
                The time horizon to predict in the future.
            
            Returns
            -------
            X : numpy array
                The input data splitted in batches

            y : numpy array
                The output data splitted in batches
            
        """
        T0  = window 
        tau = T0+horizon - 1

        m   = xraw.shape[0] - tau
        Y   = np.array(yraw[tau:])

        X = np.array([xraw[i:i+T0] for i in range(m)])
        X = X.transpose((0,2,1))
        return X.astype(np.float32), Y.astype(np.float32)

    @staticmethod
    def _format_to_dcnn_input(input_, output_, window, horizon):
        r"""Function to transform the data into batches

            Parameters
            ----------
            input_ : pandas array
                The pandas array with the input features

            output_ : pandas array
                The pandas array with the output/target feature

            window : int
                The time window to look in the past

            horizon : int
                The time horizon to predict in the future.
            
            Returns
            -------
            X : numpy array
                The input data splitted in batches

            y : numpy array
                The output data splitted in batches
            
        """
        #Create the proper numpy arrays.
        num_examples = input_.shape[0] 
        num_features = input_.shape[1]
        num_outfeats = output_.shape[1]
        size = num_examples - horizon - window + 1       
        X = np.zeros((size, window, num_features), np.float32)
        y = np.zeros((size, num_outfeats), np.float32)
        #Find the idexes of the wanted timeseries
        valid_ins = input_.iloc[:, 0].rolling(str(window) + 'h').apply(
            lambda x: True if x.shape[0] == window else False, raw=True)
        #Transform the pandas array to numpy array
        input_ = input_.values
        output_ = output_.values
        ind = 0
        for i, val in valid_ins.reset_index(drop=True).iteritems():
            if val != 1:
                continue
            j = i + 1
            X[ind] = input_[j - window:j, :]
            out = output_[i:i + horizon, :].T
            if out.shape[1] != horizon:
                continue
            y[ind] = out
            if not np.isnan(X[ind]).any():
                if not np.isnan(y[ind]).any():
                    ind += 1

        X =  X[:ind].transpose((0,2,1))
        return X, y[:ind]


    @staticmethod
    def extract_from_split(split, train_ind, test_ind):
        r"""Static method to split the dataset based
            on the indices
        """
        tr_ind = split.copy()
        te_ind = split.copy()
        train_ind.sort()
        test_ind.sort()
        for ind in test_ind[::-1]:
            del tr_ind[ind]
        for ind in train_ind[::-1]:
            del te_ind[ind]
        return pd.concat(tr_ind).sort_index(), pd.concat(te_ind).sort_index()


    def controlled_train_test_split(self, data):
        r"""Method to split the dataset

            Parameters
            ----------
            data: panda dataframe with dates

            Returns
            -------
            test: pandas dataframe
                 The month July and December of each year
            train: pandas dataframe
                 The remaining months
        """
        split = [g for n, g in data.groupby(pd.Grouper(freq='M')) if
                 g.shape[0] != 0]

        test_ind = np.hstack([[3 + i * 12, 7 + i * 12, 11 + i * 12] for i in
                              range(int(len(split) / 12))])
        train_ind = list(
            filter(lambda x: x not in test_ind, np.arange(len(split))))

        train, test = self.extract_from_split(split, train_ind, test_ind)
        return train, test

    def _split_set(self, split_train_test):
        r"""Split method
        
            Returns
            -------
            milestones : list
                A list of two floats representing the parts.
        """
        splits = split_train_test
        milestones = []
        milestones = list(map(lambda x:
                              float(int(x)/100), splits.split('-')))
        return milestones

    def controlled_train_val_split_case52(self, data, split_train_test):
        r"""Method to split the dataset

            Parameters
            ----------
            data: panda dataframe with dates

            Returns
            -------
            test: pandas dataframe
                 The month July and December of each year
            train: pandas dataframe
                 The remaining months
        """
        milestones = self._split_set(split_train_test)
        midpoint = int(len(data)*milestones[0])
        train = data[:midpoint]
        test = data[midpoint:]
        return train, test

            
    def shift_and_normalize(self, data, scaler=None):
        r"""Method to clean and normalize the data

            Parameters
            ----------
            data : pandas dataframe

            Returns:
            --------
             pandas dataframe
        """
        #Clead the data from NaN values
        clean, errors = self._clean_data(data)
        if scaler is None:
            scaler = StandardScaler()
            scaler.fit(clean.values)
        scaled_values = scaler.transform(clean.values)
        clean = pd.DataFrame(data=scaled_values, index=clean.index,
                             columns=clean.columns)
        return pd.concat([clean, errors]).sort_index(), scaler

           
    def preprocess_data(self, data, scaler=None):
        r"""Extract the normalization parameters only from
           the training set. This happens in order to avoid 
           data spoofing.

           Parameters
           ----------
           data : pandas dataframe

           scaler : sklearn scaler

           Returns:
           --------
            pandas dataframe

            sklearn scaler 
        """
        if scaler is None:
            data, scaler = self.shift_and_normalize(data)
        else:
            data, _ = self.shift_and_normalize(data, scaler)
        return data, scaler
        
        
    def _transform_data(self, data, transvars, split_train_test):
        r"""Method to transform the data into batches.
        
            Parameters
            ----------
            data : pandas array
                An pandas array with the inputs features as
                columns and the datetimes as rows.
                
            transvars : dict
                A dictionary with the arguments
                needed to tranform the data.

            Returns
            -------
            tr_inputs : numpy array
                An numpy array with the training input features in batches.

            tr_targets : numpy array
                An numpy array the training output features in batches.

            val_inputs : numpy array
                    An numpy array with the validation input features in batches.
    
            val_targets : numpy array
                An numpy array the validation output features in batches.

            norm_params : sklearn scaler
                For denormalizing the data.
        """


        out_feat = transvars['target_feats']
        
        window = transvars['window']
        horizon = transvars['horizon']
        if split_train_test is not None:
            #Split the dataset into train-val set
            train, val = self.controlled_train_val_split_case52(data, split_train_test)

            #Normalize the train data
            out_train = train[out_feat].shift(-1).copy()
            df_train, norm_params = self.preprocess_data(train)

            #Normalize the validation data
            out_val = val[out_feat].shift(-1).copy()
            df_val, _ = self.preprocess_data(val, norm_params)

            ## Final conversion to batches
            #Convert the train set
            tr_inputs, tr_targets = \
            self._format_to_dcnn_input(df_train, out_train, window, horizon)
        else:
            tr_inputs = None
            norm_params = None
            tr_targets = None

            out_val = data[out_feat].shift(-1).copy()
            df_val, norm_params = self.preprocess_data(data)
        
        #Convert the validation set
        val_inputs, val_targets = \
            self._format_to_dcnn_input(df_val, out_val, window, horizon)
        return tr_inputs, tr_targets, val_inputs, val_targets, norm_params
    
