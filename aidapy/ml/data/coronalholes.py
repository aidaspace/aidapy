""" The database for the coronal holes use case.
"""
import os
import random
import glob
import cv2
import numpy as np
import albumentations as A
from albumentations import (
    Resize,
    HorizontalFlip,
    Compose,
    VerticalFlip,     
)



class CoronalHoles():
    """
        Dataloader class for CH Use Case
    """
    def __init__(self, get_data_vars=None, transform_data=None, split_train_test=None, get_data_mode=None,
                name='CoronalHoles', random_seed=None, train=True):
        r"""A simple dataset class that is responsible of handling
            the Coronal Hole dataset. 
            
            Parameters
            ----------
            cfg : ConfigParser class
                Class that contains the arguments.

            name : str
                The name of the dataloader

            train : bool
                If the dataloader is for training or validation. 
                The augmentations are changing accordingly.

            Note
            ----
            It inherits it's functionality from the GenericDB.
            See this class also for more info.
            
        """
        random.seed(random_seed)
        # Initialize the Augmentation dictionary. 
        # You need to specify the training & validation augmentations.
        self.transforms = dict()
        self.transforms['train'] = None
        self.transforms['valid'] = None
        self.transforms['train'] = Compose([
            Resize(256, 256, interpolation=1, p=1),
            HorizontalFlip(p=0.5),
            VerticalFlip(p=0.5)
            ])

        self.transforms['valid'] = Compose([
            Resize(256, 256, interpolation=1, p=1)
            ])
        # We want to affect only the image and not the mask
        self.norm_img = A.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.name = name
        self.train = train
        self.split_train_test = split_train_test
        
        #Set filesystem paths
        path = get_data_vars['data_path']
        self._set_filesystem(path)

        #Scan the filesystem and load the data
        self._scan()
        

    def _split_set(self):
        r"""A simple dataset class that is responsible of handling
                    the Coronal Hole dataset.

            Returns
            -------
            milestones : list
                A list of two floats representing the parts.
        """
        splits = self.split_train_test 
        milestones = []
        milestones = list(map(lambda x:
                              float(int(x)/100), splits.split('-')))
        return milestones

    def _set_filesystem(self, dir_data):
        r"""Set filesystem paths of your data.

            Parameters
            ----------
            dir_data : str
                A datapath.
            
        """
        self.apath = os.path.join(dir_data, self.name)
        self.dir_imgs = os.path.join(self.apath, 'IMGS')
        self.dir_masks = os.path.join(self.apath, 'MASKS')
        self.ext = ('.jpg', '.jpg')
        
    def _scan(self):
        """ Scan the filesystem paths to find the datapaths.
            After that finds the paths it splits them in
            training-test set.
        """
        imgs = sorted(glob.glob(os.path.join(self.dir_imgs, \
                                '*' + self.ext[0])))
        masks = []
        for f in imgs:
            filename, _ = os.path.splitext(os.path.basename(f))
            filename1 = filename[:-4] + "mask"
            masks.append(os.path.join(
                 self.dir_masks, '{}{}'.format(filename1, self.ext[1])))

        # Split to Train-Test set.
        # Remove the lines above, if you want to use all dataset.
        milestones = self._split_set()
        midpoint = int(len(imgs)*milestones[0])
        self.train_inputs = imgs[:midpoint]
        self.train_targets = masks[:midpoint]
        self.val_inputs = imgs[midpoint:]
        self.val_targets = masks[midpoint:]

    def _load_files(self, idx):
        r"""Set the load mechanism of the Coronal Holes batches

            Parameters
            ----------
            idx : int
                The batch idx of the iteration.

            Returns
            -------
            im : numpy array
                The image

            mask : numpy array
                The target
        """
        f_img = None
        f_mask = None
        if self.train:
            f_img = self.train_inputs[idx]
            f_mask = self.train_targets[idx]
        else:
            f_img = self.val_inputs[idx]
            f_mask = self.val_targets[idx]
        im = cv2.imread(f_img)
        mask = cv2.imread(f_mask, 0)
        return im, mask

    def augmentations(self, im, target, train):
        r"""Perform the augmentations needed for the Coronal Holes use case

            Parameters
            ----------
            im : numpy array
                The original image of the iteration.
            
            target : numpy array
                The target image of the iteration.

            train : boolean
                The target image of the iteration.

            Returns
            -------
            aug_input : numpy array
                 Augmented image data

            aug_target : numpy array
                Augmented target data

            vis_input: numpy array
                Input array for visualization in 
                tensorboard. Since visualization data 
                are dependent in the use case you have
                define the proper format.
        """
        
        if train:
            augmented = self.transforms['train'](image=im, mask=target)
        else:
            augmented = self.transforms['valid'](image=im, mask=target)
        norm_img = self.norm_img(image=augmented['image'])['image']
        aug_input = np.transpose(norm_img, (2, 0, 1)).astype(np.float32)
        vis_input = np.transpose(augmented['image'], (2, 0, 1)).astype(np.float32)
        aug_target = augmented['mask']/augmented['mask'].max()

        return aug_input, aug_target, vis_input, 


    def __getitem__(self, idx):
        """Get item method to return the augmented images 

         Parameters
            ----------
            idx : int
                The batch idx of the iteration.
            
            Returns
            -------
            aug_input : numpy array
                 Augmented image data

            aug_target : numpy array
                Augmented target data

            vis_input: numpy array
                Input array for visualization in 
                tensorboard. Since visualization data 
                are dependent in the use case you have
                define the proper format.
        """
        img, mask = self._load_files(idx)
        aug_input, aug_target, vis_input  = self.augmentations(img, mask, self.train)

        return aug_input, aug_target, vis_input


    def __len__(self):
        """ Returns the length of the DB.
        """
        if self.train:
            return len(self.train_inputs)
        else:
            return len(self.val_inputs)
