r""" High level wrapper of the data module. 
"""
import copy
from scipy.sparse.construct import rand
import torch
from importlib import import_module
from torch.utils.data import dataloader
from torch.utils.data import ConcatDataset

class MyConcatDataset(ConcatDataset):
    def __init__(self, datasets):
        r"""Class that concatenate different dataset classes
             and calculate their length.

            Parameters
            ----------
            datasets: Pytorch Dataset
                A list of of pytorch datasets
        """
        super(MyConcatDataset, self).__init__(datasets)
        self.train = datasets[0].train

    def __len__(self):
        """ Method to calculate the total length
            of all the data in your datasets.

            Returns
            -------
            sum_datalength : int
                The sum_datalength of all the data
        """
        sum_datalength = 0
        for d in self.datasets:
            sum_datalength += len(d)
        return sum_datalength


class Data:
    def __init__(self, get_data_mode=None, transform=None, get_train_mode=None, get_train_dbs=None, 
                get_batch_size_val=None, gpu_use=None, get_mode=None, 
                data_Vars=None, split=None, get_batch_size=None, get_test_dbs=None, random_seed=None, log=None):
        r"""Data wrapper class that is reponsible of handling multiple
            datasets of AIDApy. It constructs the data submodules based on the parameters of the
            cfg file. It creates 2 dataloaders, the train and the test loader, and adding
            them in memory.

            Parameters
            ----------
            cfg : ConfigParser class
                Class that contains the arguments.

            log : Logger class
                Class for logging.
        """


        self.transform=transform
        self.get_data_mode=get_data_mode
        self.get_train_dbs = get_train_dbs
        self.get_batch_size_val = get_batch_size_val
        self.gpu_use = gpu_use
        self.get_mode = get_mode
        self.data_Vars = data_Vars
        self.split = split
        self.get_batch_size = get_batch_size
        self.get_test_dbs = get_test_dbs
        self.random_seed = random_seed
        if log is None:
            self.log = False
        else:
            self.log = log
        #The dataloaders
        self.loader_train = None
        self.loader_test = []
        #Select the proper mode
        if get_train_mode:
            self._trainval_mode()
        else:
            self._train_test_mode()


    def _trainval_mode(self):
        r"""The mode that splits each dataset into train-val. 
        """
        traindatasets = []
        for d in self.get_train_dbs:
            if self.log:
                self.log.write('Dataset (Train): ' + d)
            module_name = d
            #Import each dataset sub-module
            m = import_module('aidapy.ml.data.' + \
                                 module_name.lower())
            trainval = getattr(m, module_name)(get_data_vars=self.data_Vars, transform_data = self.transform, split_train_test=self.split, 
            									get_data_mode=self.get_data_mode, name=d, random_seed=self.random_seed)
            traindatasets.append(copy.deepcopy(trainval))
            trainval.train = False
            #Create the testset.
            self.loader_test.append(dataloader.DataLoader(
                trainval,
                batch_size=self.get_batch_size_val,
                shuffle=False,
                pin_memory=not self.gpu_use,
                num_workers=0,
            ))
        if self.get_mode == 'train':
            #Construct the Concatenated Dataset
            self.loader_train = dataloader.DataLoader(
                MyConcatDataset(traindatasets),
                batch_size=self.get_batch_size_val,
                shuffle=True,
                pin_memory=self.gpu_use,
                num_workers=1)
                    


    def _train_test_mode(self):
        r"""The mode that uses each dataset either for train
            either for validation.
        """ 
        
        if self.get_mode == 'train':
            datasets = []
            for d in self.get_train_dbs:
                if self.log:
                    self.log.write('Dataset (Train): ' + d)
                module_name = d
                #Import each dataset sub-module
                m = import_module('aidapy.ml.data.' + \
                                     module_name.lower())
                datasets.append(getattr(m, module_name)(get_data_vars=self.data_Vars, transform_data = self.transform, split_train_test=self.split, 
            											get_data_mode=self.get_data_mode, name=d, random_seed=self.random_seed)) ###


            #Construct the Concatenated Dataset
            self.loader_train = dataloader.DataLoader(
                MyConcatDataset(datasets),
                batch_size=self.get_batch_size,
                shuffle=True,
                pin_memory=self.gpu_use,
                num_workers=1)

        
        for d in self.get_test_dbs:
            if self.log:
                self.log.write('Dataset (Test): ' + d)            
            module_name = d
            m = import_module('aidapy.ml.data.' + module_name.lower())
            testset = getattr(m, module_name)(get_data_vars=self.data_Vars, transform_data = self.transform, split_train_test=self.split, 
            									get_data_mode=self.get_data_mode, name=d, random_seed=self.random_seed, train=False)
            self.loader_test.append(dataloader.DataLoader(
                testset,
                batch_size=self.get_batch_size_val,
                shuffle=False,
                pin_memory=not self.gpu_use,
                num_workers=0,
            ))
