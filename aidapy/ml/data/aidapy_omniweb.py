""" Dataloader to download from the omni web
"""
from importlib import import_module
import random

from aidapy.ml.data.common import ReadDataMixIn


class Aidapy_OmniWeb(ReadDataMixIn):
    def __init__(self, get_data_vars=None, transform_data=None, split_train_test=None, get_data_mode=None,
                name='Aidapy_OmniWeb', random_seed=None, train=True):
        r"""Omni Dataloader class that downloads the data from the web  
            and transforms them into batches.

            Parameters
            ----------
            cfg : ConfigParser class
                Class that contains the arguments.

            name : str
                The name of the dataloader

            train : bool
                If the dataloader is for training or validation. The 
                augmentations are changing accordingly.

            Note
            ----
            This class inherits utilities from 3 classes. The GenericDB, the 
            ReaddMixIn which provide the utility to read the data from the memory
            or download them from the web and the TransformDataMixIn that provides 
            the utility to transform the data into batches based on the time_horizon
            and time_window.
            
        """
        random.seed(random_seed)

        super().__init__()
        data = None
        self.name = name
        self.train = train

        #Define the train and validation transformations
        #in a dictionary        
        self.transforms = dict()
        self.transforms['train'] = None
        self.transforms['valid'] = None
        self.scaler = None
        #Read the data from the ReadDataMixIn class
        if get_data_mode == 'memory':
            #Load the data from the memory
            datapath = get_data_vars['data_path']
            data = self._load_data(datapath)
        elif get_data_mode == 'download':
            #Download the data
            data = self._download_data(get_data_vars)
        else:
            raise "Data mode in the yaml file is incorrect"
        module_name = transform_data['data_mixin']
        m = import_module('aidapy.ml.data.common.' + \
                                 module_name.lower())
        data_mixin = getattr(m, module_name)()

        self.train_inputs, self.train_targets, self.val_inputs,\
                            self.val_targets, norm_params = \
                                   data_mixin._transform_data(data, transform_data, split_train_test)
        
        self.set_denorm_params(norm_params)


    def _load_files(self, idx):
        r"""Set the load mechanism of the batches.

            Parameters
            ----------
            idx : int
                The number of the iteration.

            Returns
            -------
            input_data : numpy array
                An array that contain the input features.

            target : numpy array
                An array that contain the target features.
        """
        if self.train:
            input_data = self.train_inputs[idx]
            target = self.train_targets[idx]
        else:
            input_data = self.val_inputs[idx]
            target = self.val_targets[idx]

        return input_data, target

    def set_denorm_params(self, scaler):
        r"""Method to set the scaler.
        """
        self.scaler = scaler

    def augmentations(self, input_data, target, train):
        r"""Method to perform the augmentations
        """
        
        aug_input = input_data.copy()
        aug_target = target.copy()
        vis_input = input_data.copy()
        return aug_input, aug_target, vis_input


    def __getitem__(self, idx):
        
        input, target = self._load_files(idx)
        aug_input, aug_target, vis_input  = self.augmentations(input, target, self.train)

        return aug_input, aug_target, vis_input

    def __len__(self):
        """ Returns the length of the DB.
        """
        if self.train:
            return len(self.train_inputs)
        else:
            return len(self.val_inputs)



