#Dataloader 
""" The database for the coronal holes use case.
"""
import os
import os.path as osp
import cv2
import random
import numpy as np

from aidapy.ml.utils.utils import contouring_circle


class ch_unsuper():
    """
        Dataloader class for CH Unsupervised Use Case
    """
    def __init__(self, get_data_vars=None, k=50, train_dbs=None, random_seed=None):
        r"""A simple dataset class that is responsible of handling
            the Coronal Hole dataset. 
            
            Parameters
            ----------
            log : logger

            data_vars : Whether to load dataset from memory or download it

            K: Number of randomly taken images to use  

            train_dbs : Name of the use case

        """
        self.data_vars=get_data_vars['data_path']
        self.k=k
        random.seed(random_seed)
        self.random_pick_nonmask()

    def random_pick_nonmask(self):
        """
        Random pick k-images from dataset. 
        """
        self.im_fol = osp.join(self.data_vars)
        im_list = [osp.join(self.im_fol, x) for x in os.listdir(self.im_fol)]
        im_list.sort()
    
        self.ll1 = random.choices(im_list, k=self.k)
        return self.ll1


    def load_files(self, idx): 
        r"""Set the load mechanism of the Coronal Holes 

            Parameters
            ----------
            idx : int
                The  idx of the iteration.

            Returns
            -------
            im : numpy array
                The image

        """ 
        f_img = self.ll1[idx]
        im = cv2.imread(f_img)
        return im


    def preprocess(self, image):
        r"""Preprocess data

            Parameters
            ----------
            image : numpy array 
                The image to be preprocessed

            Returns
            -------
            im : numpy array
                The denormalized image

            vectorized : numpy array
                    The preprocessed image
        """ 
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
        image_ = image.copy()
        self.image_ = cv2.threshold(image_, 30, 255, cv2.THRESH_TOZERO)[1]
        
        # Create Static Circle Around Solar Disk (assuming that Sun will be always centered on image plane)
        self.contoured_image, self.image_processed_mask = contouring_circle(image.copy())

        # Prepare contoured image for KMeans Processing
        img = self.image_processed_mask.copy()
        vectorized = img.reshape((-1,1))
        vectorized = np.float32(vectorized)
        
        return img, vectorized
        
    
    def __getitem__(self, idx):
        """Get item method to return the loaded preprocessed images 

         Parameters
            ----------
            idx : int
                The batch idx of the iteration.
            
            Returns
            -------
            vectorized : numpy array
                The preprocessed image to fit the kmeans

            denorm : numpy array
                Denormalized image
        """
        img = self.load_files(idx)
        denorm, vectorized  = self.preprocess(img)
        return vectorized, denorm


   