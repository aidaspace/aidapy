from .data import Data, MyConcatDataset

__all__ = ['Data', 'MyConcatDataset']
