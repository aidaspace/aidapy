import random
from importlib import import_module


class Data_Unsuper:
    def __init__(self, log=None, get_train_dbs=None, data_Vars=None, k=50, random_seed=None):
        r"""Data wrapper class that is reponsible of handling multiple
            datasets of AIDApy. It constructs the data submodules based on the parameters of the
            cfg file. It creates the train and the train loader, and adding
            them in memory.

            Parameters
            ----------

            log : Logger class
                Class for logging.

            get_train_dbs : List
                Contains the datasets we want to use

            data_Vars : Dict
                Containes the datamode and path 

            k : int
                Number of randomly taken images to use
        """
        random.seed(random_seed)
        self.random_seed = random_seed
        self.get_train_dbs = get_train_dbs
        self.data_Vars = data_Vars
        self.k = k

        if log is None:
            self.log = False
        else:
            self.log = log
        
        #Select the proper mode
        self.train_mode()


    def train_mode(self):
        r"""The mode that splits each dataset into train-val. 
        """
        for d in self.get_train_dbs:
            if self.log:
                self.log.write('Dataset (Train): ' + d)
            module_name = d
            #Import each dataset sub-module
            m = import_module('aidapy.ml.data.' + \
                                 module_name.lower())
            train = getattr(m, module_name)(get_data_vars=self.data_Vars, k=self.k, train_dbs=self.get_train_dbs, random_seed = self.random_seed)
            #Create the train.
            self.loader_train = train

        
