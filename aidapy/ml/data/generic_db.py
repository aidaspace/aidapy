""" Generic dataset to create dataloaders.
"""
import torch.utils.data as data
from .augmenter import Transformations


class GenericDB(data.Dataset):
    """ Abstract class for dataloader general functionalities"""
    def __init__(self, name='', train=True):
        """ A Generic DB class that can be used for
            creating you own Dataset class with inheritance. 
            All the methods existing in this class are
            required to run your example.

            Params
            ------
            cfg : ConfigParser class
                Class that contains the arguments.

            name : str 
                The name of the dataloader

            train : bool
                If the dataloader is for training or validation. The 
                augmentations are changing accordingly.
            

            Note
            ----
            
                For seeing how inheritance work please the
                 coronal_holes.py script in this folder.
        """
        self.name = name
        self.train = train
        #Define the augmentations
        self.augmentations = Transformations()(self.name)


    def _load_files(self, idx):
        """ Set the load mechanism of the batches
        """
        raise NotImplementedError


    def __getitem__(self, idx):
        """ Returns the input and target based
         on the index.
        """
        #Load data from memory
        input, target = self._load_files(idx)
        #Augment the data and bring them in the proper form
        return self.augmentations(input, target, self.train)


    def __len__(self):
        """ Returns the length of the DB.
        """
        return len(self.inputs)
