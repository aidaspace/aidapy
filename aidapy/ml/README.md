# <u>ADDING A NEW USE CASE</u>



This guide is for providing you detailed instructions of how to add a new use case in the **AIDApy ML Engine**.

It gathers all the necessary steps and explains the different parts of the interface. 

Certainly there some parts that may have been omitted in this document but we strongly believe that if you manage to perform the following steps then you will not have any problem in adding new use cases.



## <u>Configuration File</u>

The parameters of the different use cases exist in a  **configuration (.yml) file**.  The name of each configuration file is denoting the specific use case.

In the current version of **AIDApy** we provide a fix template but new options can be easily added as we will show in this section. 

An example of some configuration files are the following:

| Coronal Holes Use-Case Config                                       | DST Index Use-Case Config (1)                         |  DST Index Use-Case Config (2)                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./assets/config_ch.png" alt="image-20201208192929864" style="zoom:70%;" /> | <img src="./assets/config_lstm_1.png" alt="image-20201208193016304" style="zoom: 71%;" /> | <img src="assets/config_lstm_2.png" alt="image-20201208195029827" style="zoom:80%;" /> |

<!-- ![image-20201208172750344](assets/config_ch.png) -->

For a detailed description of each of those option please visit the **config_template.yml** that is located under the **configs folder** in the **root** directory of aidapy. To **add new options** in the configuration file the procedure is very simple.

​	1. Open with a code editor the **config_parser.py** that is located in the **./aidapy/ml/utils** directory.	

​	2. In the **config_parser.py** add a small sub-method in the **ConfigParser** class similar to the others. In the example below we want to retrieve the name    		of the model that is located under the **model section & type sub-section** in the .yml file.

<img src="assets/image-config_parser.png" alt="image-20201208172537841" style="zoom: 50%;" />

Then you can retrieve this option from anywhere in the interface  by using the command **cfg.getModelType()** in which **cfg** is an **instance** of the **ConfigParser** class.



## <u>Structure of the ML Engine</u>

In this section a more technical overview of the structure of the AIDApy ML Engine is presented. The **structure** of the ML Interface is the following:

<img src="assets/image-stracture.png" alt="image-20201208190847448" style="zoom:67%;" />

In **most** of these **folders**  exist a **master** **class** that acts as a  **wrapper** for all the different subclasses. By doing so,  we impose a needed **standardization** of how the new use cases will be added to the system. This is important since the **range** of the problems that we try to **solve** with the AIDApy python package can **differ**  **significantly**.

Apart from that, a standardize but flexible system is **easy** to **maintain** and can be **adopted easily** from new users. The flexibility streams from the fact that any subclass can be added in the form that you can find it on-line by only adding to this **1** **function**.

Next we will describe each **component** of the interface separately.



## <u>Data Module</u></u>

The data folder above contains the necessary data-management functions for integrating a new dataset to the ML Engine. It's structure is:

<img src="assets/image-data_struct.png" alt="image-20201208191756335" style="zoom: 67%;" />

The **data.py** in the data module contains a **high level wrapper** that creates the **PyTorch data-loader** and easily performs a **concatenation** of different **databases** classes. 

To complete the **registration** of new **dataset** in the **AIDApy** **ML engine** you need to register your own **data** **augmentations** class and functions and pass it in the **transformations** class which can be found in the **augmenter.py** script in the same directory.

We **highly** **recommend** to add augmentations in this way and not put them directly inside the above database class for better **maintainability** and **readability** of your code.

Also because augmentations are extremely ***dependent\*** of the ***problem*** you require to solve. So there is a great ***variation*** of functions and python packages that someone can use. So it is better to be organized in a separated script, for managing to easily ***update*** the ***dependencies*** when it is needed.

An example of a dataset-transformation ***sub-class*** is the following :

| Database Generic Class                                       | Transformation Generic class                                 | Augmentations Generic Class                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./assets/image-generic_db.png" alt="image-20201208192929864" style="zoom:90%;" /> | <img src="./assets/image-generic_trans.png" alt="image-20201208193016304" style="zoom: 110%;" /> | <img src="assets/image-generic_augs.png" alt="image-20201208195029827" style="zoom:80%;" /> |



The basic thing that you need to know is that the name of the augmentation as well as the data-loader class is the same as you define it in the **.yml** configuration file.

For example for the CoronalHoles database in the .yml file above the name of the dataloader and augmentation scripts are coronalholes.py. Also the name of the classes is the same (CoronalHoles). Please check the coronal holes example to understand this connection.

 

### <u>Model Module</u>

The model folder contains the necessary model functions for integrating a new CNN models to the ML Engine.  It's structure is: 

![image-20201208200820449](assets/image-model.png)

The **model.py** in the model module contains a **high level wrapper** that creates the  CNN model and easily performs some basic functionalities as saving the models and loading them. 

To add **a new CNN model** in the AIDApy package the only you have to do is to create a function **make_model**  and call through it the CNN model. So for example you have a script **unet.py** that contains the UNet architecture. You place this script in the Model folder and add this function in your script.

Also copy-paste the **load_state_dict** generic class **method** and place it to your **model** **class** as **method** for loading the model weights. Even if this is optional it is highly **recommended** in order for someone to be able to **start** with **pretrained weights**.



| make_model Method                                            | **load_state_dict** Method                             |
| ------------------------------------------------------------ | ------------------------------------------------------ |
| <img src="assets/image-unet.png" alt="1592569111654" style="zoom:50%;" /> | <img src="assets/load_state.png"  style="zoom:50%;" /> |



### <u>Loss Module</u>

The **loss.py** in the Loss module contains a **high level wrapper** that creates the different loss function that you are going to train your model.  For training your model, it is possible to **define** **multiple loss function** instead of one by simply adding the symbol **"+"** in the config file.

In **contrast** with the other wrappers to add a new loss you will have to **change a bit** this code.

For example, let's imagine that we want to **add** the **BCE loss** in the aidapy ML engine. Then you have to **add** a condition in the **if statement** above:

![image-20201208201841895](assets/image-loss.png)



Also it would be for the best to follow the **specific** **format** of the loss functions. You can see detailed examples in the loss.py. 



### <u>Metrics Module</u>

**Metrics** module has almost identical **interface** with the **Loss module** and the new **addition** of **metrics**  can be realized in the **same way**. 



### <u>PostProcess Module</u></u>

**Post process** module has been created since there a lot of different ways to process the output of  a CNN network and is heavily dependent with the problem that you are up to. It contains only a script (__init__**.py**) that you can register  there your custom post process method.

<img src="assets/image-postprocess.png" style="zoom:50%;" />

For example, in the figure above we see that **seg_1cls** function can be used for post processing. If you also check the **config_coronal_holes.yml file** at the beginning of this document you will figure out that we have defined there this post processing method.

 The class that is responsible for managing the different methods is the **PostProcessor**.  



### <u>Callbacks Module</u></u>

The callbacks module is responsible of adding specific functionalities in specific parts of the training/validation process. 

