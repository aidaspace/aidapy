""" Callback to save the best checkpoint in each epoch
"""
from .callback import Callback


class MetricsCallback(Callback):
    """Checkpoint callback for saving the 
       best model.
    """
    def __init__(self, cfg):
        self.cfg = cfg

    def on_validation_init_dataloader(self, engine):
        total_d = engine.total_d
        engine.metric.number_of_samples(total_d)
               
    def on_validation_end_batch(self, engine):
        engine.metric.forward(engine.output, engine.target)

