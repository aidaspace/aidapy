""" Class to make a Grid for images for tensorboard
"""
import cv2
import numpy as np 
import os
import os.path as osp

class SaveMakerKMeans:
    """ Creating a grid for displaying
    timeseries forecasting in tensorboard.
    """
    def __init__(self, cfg):
        self.cfg = cfg

    def __call__(self, image_, contoured_image, output, savedir):
        result = np.concatenate([image_, contoured_image, output[0], output[2], np.invert(output[1])], axis=1)
        h1, w1 = result.shape[:2]
        
        # Put Image State for better understanding
        step = int(w1 / 5)
        DICT_MES = ['original', 'contoured', 'kmeans clusters', 'kmeans mask', 'corrected_mask']
        wid = step // 2
        for op in range(0, 5):
            cv2.putText(result, DICT_MES[op], (wid, h1-10), 1, 1.2, (255,255,255), 1, 1)
            wid += step
        
        cv2.imwrite(savedir, result)  
                
