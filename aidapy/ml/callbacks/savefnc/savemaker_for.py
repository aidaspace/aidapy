""" Class to make a Grid for images for tensorboard
"""
from aidapy.ml.utils.ts2plot import ts2plot2im
import cv2

class SaveMakerFor:
    """ Creating a grid for displaying
    timeseries forecasting in tensorboard.
    """
    def __init__(self, cfg):
        self.cfg = cfg

    def __call__(self, denorm_input, target, tensor, savedir):
        img = ts2plot2im(target.cpu().detach().numpy(), tensor)
        cv2.imwrite(savedir, img.astype('uint8'))   
            
