""" Class to make a Grid for images for tensorboard
"""
import cv2
import numpy as np

class SaveMakerSeg:
    """ Creating a grid for displaying
    segmentation images in tensorboard.
    """
    def __init__(self, cfg):
        self.cfg = cfg
        self.mode = [m for m in self.cfg.getSaveMode()]

    def __call__(self, denorm_input, target, tensor, savedir):
        # Tranform in the right dimension
        tensor = tensor.cpu().squeeze().permute((1, 2, 0)).numpy()
        h, w, c = tensor.shape

        #Create the Empty Image
        total_width = len(self.mode)*w
        new_img = np.zeros((h, total_width, c))
        img = None
        for i in range(0, len(self.mode)):
            if self.mode[i] == 'Pred':
                img = tensor * 255
            elif self.mode[i] == 'Target':
                try:
                    img = np.transpose(target.cpu().numpy().squeeze(0), (1, 2, 0)) * 255
                except:
                    img = target.cpu().numpy().squeeze(0) * 255
                    
                    if (c==3) and (len(img.shape)<3):
                        img = cv2.merge((img, img, img))
                         
            elif self.mode[i] == 'Input':
                denorm_input = np.transpose(denorm_input.cpu().numpy().squeeze(0), (1, 2, 0))
                img = denorm_input[:, :, ::-1]
            else:
                raise("Error") 
                
            new_img[:h, i*w:(i+1)*w, :] = img
        cv2.imwrite(savedir, new_img.astype('uint8'))

