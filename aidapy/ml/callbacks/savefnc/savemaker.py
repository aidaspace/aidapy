""" Class to make a grid of images for tensorboard visualization
"""
import sys

from numpy.lib.npyio import save
from .savemaker_seg import SaveMakerSeg
from .savemaker_for import SaveMakerFor
from .savemaker_kmeans import SaveMakerKMeans

class SaveMaker:
    """ Creating a grid for displaying
    in tensorboard.
    """
    def __init__(self, cfg):
        self.cfg = cfg
        self.SaveM = self.savemaker_picker(self.cfg.getVisName())

    def savemaker_picker(self, savename):
        if savename == 'vis_seg':
            return SaveMakerSeg(self.cfg)
        elif savename == 'vis_fc':
            return SaveMakerFor(self.cfg)
        elif savename == 'vis_kmeans':
            return SaveMakerKMeans(self.cfg)
        else:
            print("Please create your own tensorboard grid")
            sys.exit(1)

    def run(self, input, target, pred, savepath):
        self.SaveM(input, target, pred, savepath)
 
   
 