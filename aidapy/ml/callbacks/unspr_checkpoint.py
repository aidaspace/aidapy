""" Callback to save the checkpoint for the unsupervised engine
"""
from .callback import Callback

class UnsupervisedCheckpoint(Callback):
    """Checkpoint callback for saving the 
       model.
    """
    def __init__(self):
        pass
            
    
    def on_validation_end_dataloader(self, engine):
        engine.model.save(mname="model_")
    
    
