""" Callback to save the best checkpoint in each epoch
"""
from .callback import Callback

class Checkpoint(Callback):
    """Checkpoint callback for saving the 
       best model.
    """
    def __init__(self, get_metr_direction=None):
        self.is_best = False
        self.direction = get_metr_direction
        if self.direction == "maximize":
            self.best_score = -1E5
        else:
            self.best_score = 1E5
            
    
    def on_validation_end_epoch(self, engine):
        epoch = engine.optimizer.get_last_epoch()
        ave_score = engine.metric.get_ave_score(epoch)
        
        if self.direction == "maximize":
            flag = ave_score > self.best_score
        else:
            flag = ave_score < self.best_score

        if flag:
            self.best_score = ave_score
            self.is_best = True
            engine.model.save(epoch, is_best=True,
                              mname="model_")
    
    
