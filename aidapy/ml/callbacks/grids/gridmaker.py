""" Class to make a grid of images for tensorboard visualization
"""
import sys
from .gridmaker_seg import GridMakerSeg
from .gridmaker_for import GridMakerFor


class GridMaker:
    """ Creating a grid for displaying
    in tensorboard.
    """
    def __init__(self, cfg):
        self.GridM = self.gridmaker_picker(cfg.getVisName())

    @staticmethod
    def gridmaker_picker(gridname):
        if gridname == 'vis_seg':
            return GridMakerSeg()
        elif gridname == 'vis_fc':
            return GridMakerFor()
        else:
            print("Please create your own tensorboard grid")
            sys.exit(1)

    def run(self, input, target, pred):
        self.GridM.run(input, target, pred)
 
    def get_grid(self):
        grid = self.GridM.get_grid()
        return grid
