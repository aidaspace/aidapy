""" Class to make a Grid for images for tensorboard
"""
import torch
from aidapy.ml.utils.ts2plot import ts2plot2im


class GridMakerFor:
    """ Creating a grid for displaying
    timeseries forecasting in tensorboard.
    """
    def __init__(self):
        self.grid = None
        self.ts = []
        self.preds = []

    def run(self, input, target, pred):
        self.ts.append(target)
        self.preds.append(pred)

    def img2tensor(self, img):
        r"""Method to convert the img into tensor.

            Parameters
            ----------
            img: numpy array

            Returns:
            --------
            tensor : pytorch tensor
        """      
        tensor = torch.from_numpy(img).permute((2,0,1))
        return tensor.unsqueeze(0)

    def create_grid(self):
        """Create grid from the lists
        """
        img = ts2plot2im(self.ts[0].cpu().detach().numpy(), self.preds[0])
        tensor = self.img2tensor(img)
        self.grid = tensor
        for i in range(1, len(self.ts)):
            temp_img = ts2plot2im(self.ts[i].cpu().detach().numpy(), self.preds[i])
            temp_tensor = self.img2tensor(temp_img)
            self.grid = torch.cat((self.grid, temp_tensor), 0)

    def get_grid(self):
        self.create_grid()
        return self.grid
