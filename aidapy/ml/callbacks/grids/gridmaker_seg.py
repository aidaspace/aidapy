""" Class to make a Grid for images for tensorboard
"""
import torch

class GridMakerSeg:
    """ Creating a grid for displaying
    segmentation images in tensorboard.
    """
    def __init__(self):
        self.grid = None
        self.imgs = []
        self.masks = []

    def run(self, img, target, mask):
        self.imgs.append(img)
        self.masks.append(mask.cpu())

    def create_grid(self):
        """Create grid from the lists
        """
        ## Init grid for visualization purposes
        self.grid = torch.cat((self.imgs[0]/255, self.masks[0]), 0)
        for i in range(0, len(self.imgs)):
            # Divide with 255 for Tensorboard visualization proper functionality
            temp_tensor = torch.cat((self.imgs[i]/255, self.masks[i]), 0) 
            self.grid = torch.cat((self.grid, temp_tensor), 0)
 
    def get_grid(self):
        self.create_grid()
        return self.grid
