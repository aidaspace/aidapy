""" Callback to visualize the validation results at each epoch
"""
import os.path as osp
from .callback import Callback
from .savefnc.savemaker import SaveMaker

   
class SaveCallback(Callback):
    r"""Callback for saving of validation results in 
        the experiment folder.
    """
    def __init__(self, cfg):
        r"""Initialization of the callback

            Parameters
            ----------
            cfg : ConfigParser class
                Class that contains the arguments.
                
        """ 
        self.cfg = cfg
        self.exp_path = osp.join(cfg.getExpDir(), cfg.getExpName())
        self.iter = 0
        self.steps = 0
        self.save = None

    def on_validation_init_dataloader(self, engine):
        total_d = engine.total_d
        self.steps = int(total_d / self.cfg.getNumValImgs()) \
            if self.cfg.getNumValImgs() < total_d else 1
        self.save = SaveMaker(self.cfg)

        
    def on_validation_end_batch(self, engine):

        if self.iter % self.steps == 0:
            savepath = osp.join(self.exp_path, 'Results', \
                                '%0.4d.png' % self.iter)
            if not self.cfg.getUnsuperflag():
                self.save.run(engine.denorm_input, engine.target,
                          engine.out_vis, savepath)
            else:
                self.save.run(engine.loader_train.image_, engine.loader_train.contoured_image, 
                            engine.output, savepath)
        self.iter += 1

    def on_validation_end_dataloader(self, engine):
        self.iter = 0
