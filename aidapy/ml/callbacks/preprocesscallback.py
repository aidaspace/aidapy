""" Callback to save the best checkpoint in each epoch
"""
from .callback import Callback

class Preprocessdata:
    """ Utility class.
    """
    def __init__(self, deviceid):
        self.device = deviceid
        self.preprocess = None

    def prepare(self, *args):
        """ Load tensors on the selected device
        """
        def _prepare(tensor):
            return tensor.to(self.device)
        return [_prepare(a) for a in args]
        

class PreProcessCallback(Callback):
    """ Preprocess callback for preparing
        the input data.
    """
    def __init__(self):
        self.device = 0

    def on_init_epoch(self, engine):
        self.device = engine.device
        self.preprocess = Preprocessdata(deviceid=self.device)
               
    def on_train_init_batch(self, engine):
        engine.input, engine.target = \
                    self.preprocess.prepare(engine.input, engine.target)

    def on_validation_init_batch(self, engine):
        engine.input, engine.target = \
                     self.preprocess.prepare(engine.input, engine.target)
        
