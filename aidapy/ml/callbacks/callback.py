""" Generic Class that is used for the callbacks.
"""
from abc import ABC


class Callback(ABC):
    """ Callback Generic Function.
    """
    def setup(self):
        """ Generic func """
        pass

    def on_init_epoch(self, engine):
        """ Generic func """
        pass

    def on_end_epoch(self, engine):
        """ Generic func """
        pass

    def on_validation_init_epoch(self, engine):
        """ Generic func """
        pass

    def on_validation_end_epoch(self, engine):
        """ Generic func """
        pass

    def on_validation_init_dataloader(self, engine):
        """ Generic func """
        pass

    def on_validation_end_dataloader(self, engine):
        """ Generic func """
        pass
            
    def on_validation_init_batch(self, engine):
        """ Generic func """
        pass

    def on_validation_end_batch(self, engine):
        """ Generic func """
        pass

    def on_train_init_batch(self, engine):
        """ Generic func """
        pass
    
    def on_train_end_batch(self, engine):
        """ Generic func """
        pass

    def on_train_init_epoch(self, engine):
        """ Generic func """
        pass
        
    def on_train_end_epoch(self, engine):
        """ Generic func """
        pass


