""" Callback to visualize the validation results at each epoch
"""
from .callback import Callback
from .grids.gridmaker import GridMaker

   
class ImgsCallback(Callback):
    r""" Visualization callback for adding
        grids of validation results in tensorboard.
    """
    def __init__(self, cfg):
        r"""Initialization of the callback

            Parameters
            ----------
            cfg : ConfigParser class
                Class that contains the arguments.
                
        """ 
        self.cfg = cfg
        self.iter = 0
        self.steps = 0
        self.grid = None

    def on_validation_init_dataloader(self, engine):
        """
            Evaluation handling before starting process
        """
        total_d = engine.total_d
        self.steps = int(total_d / self.cfg.getNumValImgs()) \
            if self.cfg.getNumValImgs() < total_d else 1
        self.grid = GridMaker(self.cfg)
        
        
    def on_validation_end_batch(self, engine):
        """
            Evaluation handling during dataloader parsing
        """
        if (self.iter % self.steps) == 0:
            self.grid.run(engine.denorm_input, engine.target,
                          engine.out_vis)
        self.iter += 1

 
    def on_validation_end_dataloader(self, engine):
        """
            Evaluation handling after dataloader parsing
        """
        epoch = engine.optimizer.get_last_epoch()
        engine.writer.add_images(engine.dataset_name + '_' + engine.dataset_mode,
                                 self.grid.get_grid(), epoch)
        self.iter = 0
        
