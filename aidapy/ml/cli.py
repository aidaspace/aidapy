"""Easily Customizable Interface that combines the
    utilities of PyTorch and Optuna for performing
     training and model optimization. 

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""
import argparse
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

#Utilities
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.utils.utils import system_checks
from aidapy.ml.factory import Factory


parser = argparse.ArgumentParser(description='AIDApy Main Configuration script')
parser.add_argument('--config', type=str, default='examples/05_omni_regressor_lstm/config_omni_web.yml',
                    help='location of the configuration file')
args = parser.parse_args()

def main():
    r"""Main script of the AIDApy ML engine. Construct the 
        factory and runs the selected use case.
        
        References
        ----------
        If you use our code please cite:
    
        .. [1] TO_DO: Add a reference.
    """
    #Configuration Parameters of the Network
    cfg = ConfigParser(args.config)
    #Checking the system resources (GPU/CPU) & creates the experiment folders
    system_checks(cfg, args.config)
    print("""Building all the components for the
            {} use case""".format(cfg.getCase()))
    ## Factory of the aidapy ML Functionalities 
    t = Factory(cfg)
    ## Run the DL Engine
    t.run()


if __name__ == '__main__':
   main()
