""" Post processor wrapper for all the use cases
"""
from functools import partial
import torch
import torch.nn.functional as F

from aidapy.ml.utils.utils import reshape, cluster_corrections



def for_1cls(tensor, loader=None,  denorm=None, train=False):
    r"""Post Process Function for the DST Forecasting use case

        Parameters
        ----------
        tensor : pytorch tensor

        train : bool

        Returns
        -------
        pytorch tensor
            The tensor in the form that the metrics wanted.
    """
    if train:
        return tensor
    else:
        return tensor, tensor.cpu().detach().numpy()


def seg_1cls(tensor, loader=None, denorm=None, train=False):
    r"""Post Process Function for the Coronal Holes use case

        Parameters
        ----------
        tensor : pytorch tensor

        train : bool

        Returns
        -------
        norm_out : pytorch tensor
            The tensor in the form that the metrics wanted.

        norm_vis : pytorch tensor
            The tensor in the form that the tensorboard wanted.
    """
    if train:
        return tensor
    else:
        output = torch.sigmoid(tensor)
        norm_out = output.round()
        norm_vis = torch.stack((norm_out.squeeze(),)*3, dim=2)
        norm_vis[norm_vis<0.6] = 0

        return norm_out, norm_vis.unsqueeze(0).permute((0, 3, 1, 2))



def ch_kmeans(output, loader, img, train):
    r"""Post Process Function for the Coronal Holes use case

        Parameters
        ----------
        output : numpy array

        loader : bool

        img :  numpy array
        
        train : Boolean

        Returns
        -------
        result_image 

        img_fin 

        lab_img
    """
    lab_img = reshape(output, img)

    center = output[0]
    label = output[1]
    image_=loader.image_
    image_processed_mask = loader.image_processed_mask
    img_fin = cluster_corrections(image_.copy(), image_processed_mask, center[:,0])
    
    # KMeans output, projecting all assigned pixel to KMeans centers.
    res = center[label.flatten()]
    result_image = res.reshape((img.shape)) 
    return [result_image, img_fin, lab_img]

class PostProcessor:
    def __init__(self, get_post_process=None):
        r"""High Level class for the post-processing.

            Parameters
            ----------
            cfg : config parser class
                Contains the arguments
        """
        self.name = get_post_process
        self.runnable = partial(globals()[self.name])

    def __call__(self, output, loader=None, denorm=None, train=True):
        return self.runnable(output, loader, denorm, train)


    
