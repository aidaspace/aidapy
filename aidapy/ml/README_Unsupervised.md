# <u>ADDING A NEW USE CASE</u>



This guide is for providing you detailed instructions of how to add a new unsupervised use case in the **AIDApy ML Engine**.

It gathers all the necessary steps and explains the different parts of the interface. 

Certainly there are some parts that may have been omitted in this document but we strongly believe that if you manage to perform the following steps then you will not have any problem in adding new use cases.


## <u>Configuration File Parameters of Coronal Holes Kmeans</u>

1. The first section of the unsupervised configuration file is system parameters. In this section we define wether to use gpu or cpu and name
the experiment directory and experiment name where the results will be saved. The reset flag deletes and recreates these folders every time
a use case is run.

![](assets/cfg_unsuper/System_params.png)

2. The second section of the configuration file is dataset parameters. In this section we define the datamode of the dataset. There are 2 
options: load data from **memory** or **download** the data. The trainval parameter is used to seperate the dataset for train and validation. In the unsupervised engine we do not use that. The train_datasets parameter defines which dataloader to use in this use case. Finally, if we select the **memory** datamode, then we need to define tha path of the dataset.

![](assets/cfg_unsuper/dataset_params.png)


3. The third section contains the visualization parameters. In this section we define which parameters to visualize. The **metrics** parameter is false since we do not use metrics for this unsupervised use case. For the coronal holes kmeans use case we use the **vis_kmeans** visualizer to save the results. We can define if we want to save images on tensorboard and how many of the images to save on the results directory. The **save_images** flag decides if we want to save the final images or not.

![](assets/cfg_unsuper/visualization_params.png)


4. The fourth section is the training parameters. In this section we define which use **case** we are running, the **mode** of the use case (right now it is in demo mode), the **metrics** used for the use case (we do not use any metrics in the Kmeans CH use case. DiceCoeff is only used in order not to get any errors at this point). The **preprocess** parameter is used if we want to prepare tensors for the device that is used (gpu or cpu) and finally we define the **postprocess** method for this usecase.

![](assets/cfg_unsuper/training_params.png)


5. The final section is the model parameters. Here if we want to use the unsupervised engine we set the **unsuper_flag** to true. In the **type** parameter we set the  model we want to use (in this case kmeans). In this section we also define the maximum iterations (**max_iter**) the algorithm will run, the **epsilon** which is the accuracy we want to achieve and the number of centroids **K**. We also set the **random** parameter which decides how many random images to use for the training of the KMeans and how many attempts to run the algorithm per image. The save_all parameter is true in case we want to save the model weights and the resume is true when we want to load a pretrained model from the pretrained directory given.

![](assets/cfg_unsuper/model_params.png)

## <u>Structure of the ML Engine</u>

In this section a more technical overview of the structure of the AIDApy ML Engine is presented. The **structure** of the ML Interface is the following:

![](assets/image-stracture.png)


In **most** of these **folders**  exist a **master** **class** that acts as a  **wrapper** for all the different subclasses. By doing so,  we impose a needed **standardization** of how the new use cases will be added to the system. This is important since the **range** of the problems that we try to **solve** with the AIDApy python package can **differ**  **significantly**.

Apart from that, a standardize but flexible system is **easy** to **maintain** and can be **adopted easily** from new users. The flexibility streams from the fact that any subclass can be added in the form that you can find it on-line by only adding to this **1** **function**.

Next we will describe each **component** of the interface separately.

## <u>Data Module</u></u>

The data folder above contains the necessary data-management functions for integrating a new dataset to the ML Engine. It's starcture is:

![](assets/cfg_unsuper/data_struct.png)


For the unsupervised  we use the **data_unsuper.py** which is a wrapper that creates the data-loader.

To complete the **registration** of new dataset in the AIDApy ML unsupervised engine you need to register your own data **augmentations** inside the data-loader of your new use case as a method. In the example of coronal holes use case you can see below the data loader class **ch_unsuper** which has a method called **preprocess** that performs the augmentations: 

![](assets/cfg_unsuper/ch_unsuper.png)

The basic thing that you need to know is that the name of the data-loader class is the same as you define it in the .yml configuration file.

For example for the Coronal Holes database in the .yml file above we use the name **ch_unsuper** and the name of the dataloader scripts are **ch_unsuper.py**. Also the name of the classes is the same (ch_unsuper). Please check the coronal holes example to understand this connection.

### <u>Model Module</u>

The model folder contains the necessary model functions for integrating a new model to the ML Unsupervised Engine. It's structure is: 

![](assets/cfg_unsuper/models_struct.png)


For the unsupervised engine we are using the **model_unsuper.py** which is a wrapper that creates model and easily performs some basic functionalities such as loading the models.

To add a new model in the AIDApy package the only you have to do is to create a function **make_model** and call through it the new model. So for example you have a script **kmeans.py** that contains the kmeans architecture. You place this script in the **Model** folder and add this function in your script.

![](assets/cfg_unsuper/kmeans.png)


### <u>PostProcess Module</u></u>

**Post process** module has been created since there a lot of different ways to process the output of your model and is heavily dependent with the problem that you are up to. It contains only a script (__init__**.py**) that you can register there your custom post process method.

![](assets/cfg_unsuper/kmeans_postprocess.png)


For example, in the figure above we see that **ch_kmeans** function can be used for post processing the output of kmeans algorithm. If you also check the **config_coronal_holes_unspr.yml file** at the beginning of this document you will figure out that we have defined there this post processing method.

 The class that is responsible for managing the different methods is the **PostProcessor**. 


### <u>Callbacks Module</u></u>

The callbacks module is responsible of adding specific functionalities in specific parts of the training/validation process. The structure of the **callbacks** folder is:

![](assets/cfg_unsuper/callbacks.png)

Each callback is responsible for a specific task. You can use these callbacks to preprocess your data, save the results and visualize them.

For example, in order to save the results of a use case we use the **savecallback** module:

![](assets/cfg_unsuper/savecallback.png)


This callback creates the paths where we will save the results and uses the **savemaker** module which is responsible for picking the right visualizer:

![](assets/cfg_unsuper/savemaker.png)


In order to add a new use case and use these callbacks to visualize your results, you need to create a new module named **savemaker_'use case name'** and add this option in the **savemaker** module. For instance, in the coronal holes with kmeans use case we create a module named **savemaker_kmeans** :

![](assets/cfg_unsuper/savemaker_kmeans.png)
