import unittest

from aidapy.ml.builders import *
from aidapy.ml.utils.config_parser import ConfigParser

class TestBuilders(unittest.TestCase):
    r"""Class for unit testing builders.
        """
    def test_bulders(self):
        conf = 'examples/06_omni_regressor_lstm_D91_case5.2/config_omni_web_D91_case52_test.yml'
        cfg = ConfigParser(conf)
        vis = build_visualizer(cfg)
        log = build_logger(cfg)
        build_callbacks(cfg)
        build_dataloader(cfg, log)
        build_loss(cfg, vis, log)
        build_metrics(cfg, vis, log)
        build_postprocess(cfg)
        model = build_model(cfg, vis)
        build_optimizer(cfg, model)

 
if __name__ == '__main__':
    unittest.main()
