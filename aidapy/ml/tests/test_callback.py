from aidapy.ml.callbacks import Checkpoint, ImgsCallback, MetricsCallback, \
                                PreProcessCallback, SaveCallback
from aidapy.ml.utils.config_parser import ConfigParser
import torch

cfg = ConfigParser("examples/05_omni_regressor_lstm/config_omni_web_test.yml")
cfg_seg = ConfigParser("examples/04_coronal_holes/config_coronal_holes_test.yml")

class TestOptim():
    def __init__(self):
        self.last_epoch = 0
    
    def get_last_epoch(self):
        return 1
    
class TestMetric():
    def __init__(self):
        self.ave_score = 0
    def get_ave_score(self, epoch):
        return 1E12
    def number_of_samples(self, out):
        return 1
    def forward(self, inp, out):
        return 0

class TestEngine(object):
    def __init__(self, cfg):
        self.optimizer = TestOptim()
        self.metric = TestMetric()
        self.total_d = 1
        self.input = torch.zeros((300, 300, 3))
        self.denorm_input = torch.zeros((300, 300, 3))
        self.output = torch.zeros((300, 300, 3))
        self.target = torch.zeros((300, 300, 3))
        self.out_vis = torch.zeros((300, 300, 3))
        self.device = 'cpu'

engine = TestEngine(cfg)

def test_checkpoint_callback():
    c = Checkpoint(cfg)
    c.on_validation_end_epoch(engine)

def test_image_callback():
    c = ImgsCallback(cfg)
    c.on_validation_init_dataloader(engine)
    c.on_validation_end_batch(engine)

    c = ImgsCallback(cfg_seg)
    c.on_validation_init_dataloader(engine)
    c.on_validation_end_batch(engine)


def test_metrics_callback():
    c = MetricsCallback(cfg)
    c.on_validation_init_dataloader(engine)
    c.on_validation_end_batch(engine)

def test_preprocess_callback():
    c = PreProcessCallback()
    class TestEngineLocal(object):
        def __init__(self):
            self.input = torch.zeros((300, 300, 3))
            self.target = torch.zeros((300, 300, 3))
            self.output = torch.zeros((300, 300, 3))
            self.device = 'cpu'
    
    engine_local = TestEngineLocal()
    c.on_init_epoch(engine_local)
    c.on_train_init_batch(engine_local)
    c.on_validation_init_batch(engine_local)
    

def test_save_callback():
    c = SaveCallback(cfg)
    c.on_validation_init_dataloader(engine)

    c = SaveCallback(cfg_seg)
    c.on_validation_init_dataloader(engine)


def test_general():
    test_checkpoint_callback()
    test_image_callback()
    test_metrics_callback()
    test_preprocess_callback()
    test_save_callback()

