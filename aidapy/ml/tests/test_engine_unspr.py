import unittest
from aidapy.ml.engine_unspr import Unsprvised
from aidapy.ml.builders import *
from aidapy.ml.utils.config_parser import ConfigParser
cfg = ConfigParser("examples/04_coronal_holes/config_coronal_holes_unspr.yml")

class TestEngine(unittest.TestCase):

    def test_unspr_engine(self):
        r"""Building method of the Unsupervised Engine.
                                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        #Build the Visualization module
        vis = build_visualizer(cfg)
        #Build the Logger module
        log = build_logger(cfg)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        
        #Build the DataLoader module
        #loader = build_dataloader(cfg, log)
        
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        #Set the model of the engine.     
        model = build_model(cfg)
        #Build the callbacks
        callbacks = build_callbacks(cfg)  
        
        #Build the Engine
        #self.engine = Unsprvised(cfg, loader, model, postprocess, log, vis, metrics, callbacks)
        
if __name__ == '__main__':
    unittest.main()
