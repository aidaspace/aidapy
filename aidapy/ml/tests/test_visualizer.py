from aidapy.ml.visualization.visualizer import Visualizer
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.builders import build_visualizer

""" Have to fill some code here """

conf = "examples/04_coronal_holes/config_coronal_holes_test.yml"
cfg = ConfigParser(conf)
#vis = Visualizer(cfg)
vis = build_visualizer(cfg)

vis.add_losses('test', 0, 0)
vis.add_metrics("test", 0, 0)
