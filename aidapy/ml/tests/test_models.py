import torch
import numpy as np
from aidapy.ml.models.unet import UNet
from aidapy.ml.models.lstm import LSTMnn
from aidapy.ml.models.dcnn import DCNN
from aidapy.ml.models.multilayer_perceptron import MLPnn
from aidapy.ml.models.kmeans import KMeans
from aidapy.ml.data.ch_unsuper import ch_unsuper
import unittest



class TestVisualizer(unittest.TestCase):

    def test_unet(self):
        # Testing CH usecase
    
        model = UNet(model_input_nc=3, model_output_nc=1, model_depth=4, random_seed=3)
        model.eval()
        x = torch.zeros((1, 3, 256, 256))
        output = model.forward(x)
        ref = torch.zeros((1, 1, 256, 256))
        self.assertEqual(output.size(), ref.size())
 

    def test_lstm(self):
        # Testing DST usecase
        model = LSTMnn(model_input_nc=3, model_output_nc=1, model_depth=2, random_seed=3)
        model.eval()
        x = torch.zeros((1000, 6, 3))
        output = model.forward(x)
        ref = torch.zeros((1000, 1, 1))
        self.assertEqual(output.size(), ref.size())

    def test_dcnn(self):
        model_depth=[16, 64, 128, 256, 128, 32]
        model = DCNN(model_input_nc=5, model_output_nc=1, model_depth=model_depth, random_seed=3)
        model.eval()
        x = torch.zeros((1000, 5, 6))
        output = model.forward(x)
        ref = torch.zeros((1000, 1))
        self.assertEqual(output.size(), ref.size())
    
    def test_mlp(self):
        model = MLPnn(model_input_nc=5, model_output_nc=6, model_depth=3, random_seed=3)
        model.eval()
        x = torch.zeros((1, 6, 5))
        output = model.forward(x)
        ref = torch.zeros((1, 1, 6))
        self.assertEqual(output.size(), ref.size())




if __name__ == '__main__':
    unittest.main()
