import unittest
from aidapy.ml.loss import Loss

import torch
import numpy as np

class TestLoss(unittest.TestCase):

    def test_loss(self):
        split = ["1*DiceLoss", "1*L1", "1*IoULoss", "1*MSE", "1*BCEWithLogitsLoss"]
        loss = Loss(get_print_every=1, get_loss_split=split, gpu_use=False)
        input = torch.zeros([1,6,6])
        target = torch.zeros([1,6,6])

        output = loss.forward(input, target, batch=1)


  
if __name__ == '__main__':
    unittest.main()