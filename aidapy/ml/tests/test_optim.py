import os.path as osp
from aidapy.ml.optim import make_optimizer
from aidapy.ml.visualization.visualizer import Visualizer
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.models.model import Model
from aidapy.ml.builders import *

# Testing CH usecase
def test_optim():
    conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
    cfg = ConfigParser(conf)
    
    vis = build_visualizer(cfg)
    model = build_model(cfg, vis)
    optim = build_optimizer(cfg, model)
    
    optim.get_last_epoch()
    optim.get_lr()
    optim.save(osp.join(cfg.getExpDir(), cfg.getExpName()))
    optim.load(osp.join(cfg.getExpDir(), cfg.getExpName()))
