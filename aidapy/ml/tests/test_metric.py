import unittest
from aidapy.ml.metrics import Metrics

import torch
import numpy as np

class TestMetric(unittest.TestCase):

    def test_metric(self):
        split = ["DiceCoeff", "IOU_scoring", "MSE"]
        metrics = Metrics(get_metrics_split=split, gpu_use=False)
        input = torch.zeros([1,6,6])
        target = torch.zeros([1,6,6])

        output = metrics.forward(input, target)
        get_score = metrics.get_ave_score(epoch=1)
        avg  = metrics.calc_ave_score(score=input, mtype='DiceCoeff')

  
if __name__ == '__main__':
    unittest.main()