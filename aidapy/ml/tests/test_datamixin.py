import unittest
from aidapy.ml.engine import Engine
from aidapy.ml.builders import *
from aidapy.ml.data.common.dcnn_format import DCNN_Format
from aidapy.ml.data.common.lstm_format import LSTM_Format
from aidapy.ml.data.common.dl_data_mixin import ReadDataMixIn as dl
from aidapy.ml.data.mixin_class import ReadDataMixIn

class TestDataMixin(unittest.TestCase):

    def test_dcnn(self):
        read = dl()
        datapath = {}
        datapath['mission'] = 'omni'
        datapath['datetime'] = ['2001-01-13 23:00:00', '2003-01-01 00:00:00']
        datapath['settings'] = {'prod': ['all']}
        datapath['features'] = ['DST', 'F', 'BZ_GSM', 'N', 'V']
        data = read._download_data(datapath)
        split_train_test = "80-20"
        transform_data = {
                'input_feats': ['DST', 'F', 'BZ_GSM', 'N', 'V'],
                'target_feats': ['DST'],
                'horizon': 1,
                'window': 6,
        }
        
        
        mixin = DCNN_Format()
        train_inputs, train_targets, val_inputs,\
                            val_targets, norm_params = \
                                   mixin._transform_data(data, transform_data, split_train_test)


    def test_lstm(self):
        read = dl()
        datapath = {}
        datapath['mission'] = 'omni'
        datapath['datetime'] = ['2013-01-14 00:00:00', '2016-12-31 23:00:00']
        datapath['settings'] = {'prod': ['all']}
        datapath['features'] = ['DST', 'BZ_GSM', 'IMF']
        data = read._download_data(datapath)

        transform_data = {
                'input_feats': ['DST', 'BZ_GSM', 'IMF'],
                'target_feats': ['DST'],
                'horizon': 1,
                'window': 6,
        }
        split_train_test = "60-40"
        mixin = LSTM_Format()
        train_inputs, train_targets, val_inputs,\
                                val_targets, norm_params = \
                                   mixin._transform_data(data, transform_data, split_train_test) 


    def test_datamixin(self):
        read = ReadDataMixIn()
        datapath = {}
        datapath['mission'] = 'omni'
        datapath['datetime'] = ['2013-01-14 00:00:00', '2016-12-31 23:00:00']
        datapath['settings'] = {'prod': ['all']}
        datapath['features'] = ['DST', 'BZ_GSM', 'IMF']
        data = read._download_data(datapath)

        transform_data = {
                'input_feats': ['DST', 'BZ_GSM', 'IMF'],
                'target_feats': ['DST'],
                'horizon': 1,
                'window': 6,
        }
        data = read._download_data(datapath)

if __name__ == '__main__':
    unittest.main()