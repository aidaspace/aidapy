import numpy as np
import torch
from aidapy.ml.utils.utils import *
from aidapy.ml.visualization.visualizer import Visualizer
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.models.model import Model
from aidapy.ml.utils.ts2plot import *
from aidapy.ml.utils.prune import *
from aidapy.ml.builders import *


def test_image_processes():
    x = torch.zeros((3, 256, 256))
    convert_tensor_to_img(x)

    x = torch.zeros((256, 256))
    # mask_coronals = [[x, x, x]]
    # plot_mask_coronals(mask_coronals)

    y = np.zeros((256, 256))
    pred_mask_postprocess(y)
    plot_coronals(y, y, y)
    plot_masks(y, y, y)


def test_tsplot():
    n1 = np.zeros((1,5))
    n2 = np.zeros((1,5))
    ts2plot2im(n1, n2)

# Testing CH usecase
def test_pruning():
    conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
    cfg = ConfigParser(conf)
    
    vis = build_visualizer(cfg)
    model = build_model(cfg, vis)

    print_model_sparsity(model)
    modelp1 = PruneModel(model, 0.5, 0.5, 'channel')
    modelp1 = MakePrunePermanent(modelp1)

    modelp = PruneModel(model, 0.5, 0.5, 'channel')
    _, _, _ = prune_convolutional_layers(modelp, 0.5, "channel")
    _, _, _ = prune_fc_layers(modelp, 0.5, "channel")

    report_pruning(modelp)
