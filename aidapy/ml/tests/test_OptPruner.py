from aidapy.ml.optimizations.pruner import NativePruner, CustomPruner #Pruner
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.builders import *
import unittest 

# Testing CH usecase
class Testpruner(unittest.TestCase):
    def test_optpruner(self):
        conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
        cfg = ConfigParser(conf)
        
        writer = build_visualizer(cfg)
        print('writer.')
        
        _ = NativePruner(cfg, writer) #ci/cd
        print('NativePruner.')

    def test_pruner(self):
        conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
        cfg = ConfigParser(conf)
        
        writer = build_visualizer(cfg)
        print('writer.')
        
        ##_ = Pruner(cfg, writer)
        _ = CustomPruner(cfg, writer) #ci/cd
        print('CustomPruner.')

if __name__ == '__main__':
    unittest.main()
