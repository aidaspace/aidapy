from aidapy.ml.data.aidapy_omniweb import Aidapy_OmniWeb
import unittest
import numpy 

class TestOmni(unittest.TestCase):

    def test_omniwebdata(self):
    
        datamode = 'download'
        train_test_split = "60-40" 
        
        datapath = {}
        datapath['mission'] = 'omni'
        datapath['datetime'] = ['2001-01-13 23:00:00', '2003-01-01 00:00:00']
        datapath['settings'] = {'prod': ['all']}
        datapath['features'] = ['DST', 'F', 'BZ_GSM', 'N', 'V']

        data_vars_transform = {
                'input_feats': ['DST', 'F', 'BZ_GSM', 'N', 'V'],
                'target_feats': ['DST'],
                'horizon': 1,
                'window': 6,
                'data_mixin': 'DCNN_Format'
        }
        c1 = Aidapy_OmniWeb(get_data_vars=datapath,
                        transform_data=data_vars_transform,
                        split_train_test=train_test_split,
                        get_data_mode=datamode)


        train_inputs, train_targets, val_inputs, val_targets = c1.train_inputs, c1.train_targets, c1.val_inputs, c1.val_targets
        self.assertIsInstance(train_inputs, numpy.ndarray)
        c1.__len__()
if __name__ == '__main__':
    unittest.main()