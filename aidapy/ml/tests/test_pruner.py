from aidapy.ml.pruner import CustomPruner, NativePruner  #Pruner
from aidapy.ml.utils.logger import Logger
from aidapy.ml.visualization import Visualizer
from aidapy.ml.utils.config_parser import ConfigParser
from aidapy.ml.builders import *
import unittest 

# Testing CH usecase
class Testpruner(unittest.TestCase):
    def test_pruner(self):
        conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
        cfg = ConfigParser(conf)
        
        writer = build_visualizer(cfg)
        
        #_ = Pruner(cfg, writer)
        _ = CustomPruner(cfg, writer)
    
    def test_optpruner(self):
        conf = 'examples/04_coronal_holes/config_coronal_holes_test.yml'
        cfg = ConfigParser(conf)
        
        writer = build_visualizer(cfg)
        _ = NativePruner(cfg, writer)
if __name__ == '__main__':
    unittest.main()
