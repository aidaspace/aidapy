import unittest
from aidapy.ml.loss.loss_func import DiceLoss
from aidapy.ml.loss.loss_func import IoULoss
from aidapy.ml.loss.loss_func import MSE
from aidapy.ml.loss.loss_func import *

import torch
import numpy as np

class TestLosses(unittest.TestCase):

    def test_diceLoss(self):
        dice = DiceLoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = dice.forward(input, output)
        loss = np.around(loss.numpy(), 4)
        ref = np.array((1))
        self.assertEqual(loss, ref)

    def test_IOULoss(self):
        iou = IoULoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = iou.forward(input, output)
        loss = np.around(loss.numpy(), 4)

        ref = np.array((0))
        self.assertEqual(1-loss, ref)

    def test_MSELoss(self):
        mse = MSE()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = mse.forward(input, output)
        ref = np.array((0))
        loss = np.around(loss.numpy(), 4)

        self.assertEqual(loss, ref)

    def test_BCEWithLogitsLossPadding(self):
        bce = BCEWithLogitsLossPadding()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = bce.forward(input, output)

    def test_BCEWithLogitsLoss(self):
        bce = BCEWithLogitsLoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = bce.forward(input, output)

    def test_DiceBCELoss(self):
        dBCE = DiceBCELoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = dBCE.forward(input, output)

    def test_FocalLoss(self):
        focal = FocalLoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = focal.forward(input, output)

    def test_TverskyLoss(self):
        tversky = TverskyLoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = tversky.forward(input, output)

    def test_FocalTverskyLoss(self):
        Ftversky = FocalTverskyLoss()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        loss = Ftversky.forward(input, output)


if __name__ == '__main__':
    unittest.main()