import unittest
from aidapy.ml.engine import Engine
from aidapy.ml.builders import *
from aidapy.ml.utils.config_parser import ConfigParser
cfg = ConfigParser("examples/04_coronal_holes/config_coronal_holes_test.yml")

class TestEngine(unittest.TestCase):

    def test_Engine(self):
        #Build the Visualization module
        vis = build_visualizer(cfg)
        writer = vis
        #Build the Logger module
        log = build_logger(cfg)
        #Build the DataLoader module
        loader = build_dataloader(cfg, log)
        #Build the Loss module
        criterion = build_loss(cfg, vis, log)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        #Build the callbacks
        callbacks = build_callbacks(cfg)
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        #Build the Engine
        print("Engine constructor is running")
        engine = Engine(cfg, loader, criterion,
                             metrics, vis, log, callbacks, postprocess)
        ##Set the model of the engine.
        
        model = build_model(cfg, writer)

        engine.set_model(model)
        #engine.start()
if __name__ == '__main__':
    unittest.main()
