
import unittest
from aidapy.ml.metrics.metric_func import *
import torch

class TestIOUmetric(unittest.TestCase):

    def test_IOUMetric(self):
        iou = IOU_scoring()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = iou.forward(input, output)
        self.assertEqual(result, torch.Tensor([1]))

    def test_MSEMetric(self):
        mse = MSE()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = mse.forward(input, output)
        self.assertEqual(result, torch.Tensor([0]))
  

    def test_DiceCoeffMetric(self):
        dice = DiceCoeff()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = dice.forward(input, output)
        self.assertEqual(result, torch.Tensor([1]))
  
    def test_RMSEMetric(self):
        rmse = RMSE()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = rmse.forward(input, output)
        #self.assertEqual(result, torch.Tensor([0]))


    def test_RSquared(self):
        r = RSquared()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = r.forward(input, output)
        #self.assertEqual(result, torch.Tensor([0]))
    
    def test_CC(self):
        cc = CC()
        input = torch.zeros(1, 1, 256, 256)
        output = torch.zeros(1, 1, 256, 256)
        result = cc.forward(input, output)
        #self.assertEqual(result, torch.Tensor([0]))

if __name__ == '__main__':
    unittest.main()