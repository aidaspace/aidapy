import unittest
import torch 

from aidapy.ml.postprocess import PostProcessor

class TestPostProcess(unittest.TestCase):
    def test_ch(self):
        # Testing CH usecase
        post_process = PostProcessor('seg_1cls')
        x = torch.ones((1, 1, 256, 256))
        output, out_vis = post_process(x, train=False)
        ref = torch.zeros((1, 1, 256, 256))
        self.assertEqual(output.size(), ref.size())
 

    def test_dst(self):
        # Testing DST usecase
        post_process = PostProcessor('for_1cls')
        x = torch.zeros((1000, 1, 1))
        output, out_vis = post_process(x, train=False)
        ref = torch.zeros((1000, 1, 1))
        self.assertEqual(output.size(), ref.size())


if __name__ == '__main__':
    unittest.main()
