import unittest
from aidapy.ml.hpo_optuna import HPO
from aidapy.ml.builders import *
from aidapy.ml.utils.config_parser import ConfigParser
cfg = ConfigParser("examples/04_coronal_holes/config_coronal_holes_with_hpo.yml")

class TestEngine(unittest.TestCase):

    def test_optuna_engine(self):
        r"""Building method of the Optuna Engine.
                                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        #Build the Visualization module
        vis = build_visualizer(cfg)
        self.writer = vis
        #Build the Logger module
        log = build_logger(cfg)
        
        #Build the DataLoader module
        #loader = build_dataloader(cfg, log)
        
        #Build the Loss module
        criterion = build_loss(cfg, vis, log)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        #Build the callbacks
        callbacks = build_callbacks(cfg)
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        
        #Build the Engine
        #self.engine = HPO(cfg, loader, criterion, vis, metrics, callbacks, postprocess)
        
if __name__ == '__main__':
    unittest.main()
