"""Useful functions """
import os.path as osp
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
import cv2
import torch
import torch.nn
from torchvision.transforms.functional import to_pil_image




def time_series(X_in, y_in, histsize=1, forecast_offset=0, dropna=True, predict_change=False):
    """Preprocess time series data for machine learning purposes

    Create new features X containing a history of the old features, and define
    the targets at a future time defined by forecast_offset.

    :param X_in: input features (2D array)
    :param y_in: input targets (1D or 2D array)
    :param histsize: how much history to include
    :param forecast_offset: index offset of the forecast
    :param dropna: remove entries with missing values
    :param predict_change: if true, predict change in y (y[t+dt]-y[t])
    :returns: X, y, mask
    :rtype: preprocessed features X and targets y, and the corresponding mask

    """
    assert len(X_in) == len(y_in), "X_in and y_in should have the same length"
    assert X_in.ndim == 2, "X_in should be two-dimensional"

    X = np.full([X_in.shape[0], histsize * X_in.shape[1]], np.nan)
    y = np.full_like(y_in, np.nan)

    # Fill the range where all X and y values are defined, so exclude the
    # beginning and end
    for i in range(histsize-1, X_in.shape[0]-forecast_offset):
        X[i] = X_in[i-histsize+1:i+1].flatten()
        if predict_change:
            y[i] = y_in[i+forecast_offset] - y_in[i]
        else:
            y[i] = y_in[i+forecast_offset]

    # Filter any entries with a NaN
    mask = np.ones(len(X), dtype=bool)
    if dropna and y.ndim == 1:
        mask = ~(np.any(np.isnan(X), axis=1) | np.isnan(y))
    if dropna and y.ndim == 2:
        mask = ~(np.any(np.isnan(X), axis=1) | np.any(np.isnan(y), axis=1))

    return X[mask], y[mask], mask


def convert_tensor_to_img(image_t, padding=16):
    """"""
    r"""Converts pytorch tensor into a Pillow Image. The padding will be removed
        from the resulting image

        Parameters
        ----------
        image_t : Tensor
                Contains the image tensor that youi want to transform to image.

        Returns
        ----------
        image : PIL Image
                Contains the PIL image fro Tensor.
    """
    std = torch.Tensor([0.229, 0.224, 0.225]).reshape(-1, 1, 1)
    mu_1 = torch.Tensor([0.485, 0.456, 0.406]).reshape(-1, 1, 1)
    output = image_t.mul(std)
    output.add_(mu_1)
    img = to_pil_image(output)
    w_1, h_1 = img.size
    return img.crop((padding, padding, w_1 - padding, h_1 - padding))


def plot_mask_coronals(mask_coronals, padding=16):
    """Plots coronals with their true mask, predicted mask.
    Parameters
    ----------
    mask_coronals: list of tuples (`true_mask`, `predicted_mask`, `coronal`)
    padding: int (default=16)
        Padding around mask to remove.
    """
    fig, axes = plt.subplots(len(mask_coronals), 3, figsize=(12, 10))
    for idx, (axes, mask_cell) in enumerate(zip(axes, mask_coronals), 1):
        ax1, ax2, ax3 = axes
        true_mask, predicted_mask, cell = mask_cell
        plot_mask_coronal(true_mask, predicted_mask, cell, 'Example {}'.format(idx), \
                ax1, ax2, ax3, padding=padding)
    fig.tight_layout()
    return fig, axes


def plot_mask_coronal(true_mask, predicted_mask, coronal, suffix, ax1, ax2,
                      ax3, padding=16):
    """
        Plots a single coronal with a its true mask and predicuted mask
    """
    for ax_line in [ax1, ax2, ax3]:
        ax_line.grid(False)
        ax_line.set_xticks([])
        ax_line.set_yticks([])
    ax1.imshow(true_mask[padding:-padding, padding:-padding])
    ax1.set_title('True Mask - {}'.format(suffix))
    ax2.imshow(predicted_mask[padding:-padding, padding:-padding])
    ax2.set_title('Predicted Mask - {}'.format(suffix))
    ax3.imshow(convert_tensor_to_img(coronal, padding=padding))
    ax3.set_title('Original image  - {}'.format(suffix))
    return ax1, ax2, ax3


def plot_masks(mask_1, mask_2, mask_3):
    """
        Plots three masks
    """
    fig, ((ax1, ax2, ax3)) = plt.subplots(1, 3, figsize=(12, 5))
    for ax_line in [ax1, ax2, ax3]:
        ax_line.grid(False)
        ax_line.set_xticks([])
        ax_line.set_yticks([])
    fig.suptitle("Coronal Holes Ground Truth Mask Examples", fontsize="x-large")
    ax1.imshow(mask_1, cmap='viridis')
    ax2.imshow(mask_2, cmap='viridis')
    ax3.imshow(mask_3, cmap='viridis')
    return ax1, ax2, ax3


def plot_coronals(cell_1, cell_2, cell_3):
    """
        Plots three cells
    """
    fig, ((ax1, ax2, ax3)) = plt.subplots(1, 3, figsize=(12, 5))
    for ax_line in [ax1, ax2, ax3]:
        ax_line.grid(False)
        ax_line.set_xticks([])
        ax_line.set_yticks([])
    fig.suptitle("Coronal Holes Image Examples", fontsize="x-large")
    ax1.imshow(cell_1)
    ax2.imshow(cell_2)
    ax3.imshow(cell_3)
    return ax1, ax2, ax3


def pred_mask_postprocess(pred_mask):
    """
    Predicted segmentation masks postprocessing.
    """
    pred_mask_copy = pred_mask.copy()
    h_dim, w_dim = pred_mask_copy.shape[0:2]
    mask_post = np.zeros((h_dim, w_dim))
    # pred_mask = pred_mask*255
    pred_mask_copy[pred_mask_copy >= 0.3] = 1.0
    pred_mask_copy[pred_mask_copy < 0.3] = 0.0
    pred_mask_copy = pred_mask_copy*255
    pred_mask = pred_mask*255
    mask_post[int(0.05*h_dim):int(0.95*h_dim), int(0.05*w_dim):int(0.95*w_dim)] = \
            pred_mask_copy[int(0.05*h_dim):int(0.95*h_dim), int(0.05*w_dim):int(0.95*w_dim)]
    mask_post_2 = np.zeros((h_dim, w_dim))
    mask_post_2[int(0.05*h_dim):int(0.95*h_dim), int(0.05*w_dim):int(0.95*w_dim)] = \
            pred_mask[int(0.05*h_dim):int(0.95*h_dim), int(0.05*w_dim):int(0.95*w_dim)]
    return mask_post, mask_post_2


def set_channel(*args, n_channels=3):
    """ Expand channel """
    def _set_channel(img):
        if img.ndim == 2:
            img = np.expand_dims(img, axis=2)

        _, c, _, _ = img.shape[1]
        if n_channels == 1 and c == 3:
            img = np.expand_dims(sc.rgb2ycbcr(img)[:, :, 0], 2)
        elif n_channels == 3 and c == 1:
            img = np.concatenate([img] * n_channels, 2)

        return img

    return [_set_channel(a) for a in args]


def system_checks(cfg, config_path):
    """
        Run initial procedure:
            - Select training device
            - Manage experiment folder
            - Validate config file
    """
    #Check if GPU e
    # xists
    if cfg.getUseGPU():
        if torch.cuda.is_available():
            torch.backends.cudnn.benchmark = True
            torch.cuda.set_device(cfg.getGPUid())
        else:
            raise Exception('Your system does not support GPU')

    #Create the appropriate dirs
    exp_dir = cfg.getExpDir()
    exp_folder = cfg.getExpName()

    total_dir = osp.join(exp_dir,
                         exp_folder)
    if cfg.getResetDir():
        if osp.exists(total_dir):
            shutil.rmtree(total_dir)

    os.makedirs(total_dir, exist_ok=True)
    os.makedirs(total_dir + "/checkpoints", exist_ok=True)
    os.makedirs(total_dir + "/Results", exist_ok=True)

    # Copy config file into Experiments folder
    shutil.copyfile(config_path, total_dir + "/config.yml")


def model_info(model, verbose=False, img_size=640):
    """
        Profiling PyTorch model
    """
    # Model information. img_size may be int or list
    n_p = sum(x.numel() for x in model.parameters()) 
    n_g = sum(x.numel() for x in model.parameters() if x.requires_grad) 
    if verbose:
        print('%5s %40s %9s %12s %20s %10s %10s' % \
             ('layer', 'name', 'gradient', 'parameters', 'shape', 'mu', 'sigma'))
        for i, (name, p) in enumerate(model.named_parameters()):
            name = name.replace('module_list.', '')
            print('%5g %40s %9s %12g %20s %10.3g %10.3g' %
                  (i, name, p.requires_grad, p.numel(), list(p.shape), p.mean(), p.std()))
    try:  # FLOPS
        from thop import profile
        model.eval()
        img = torch.zeros((1, 3, img_size, img_size))
        print("Profiling using input shape: {}".format(img.shape))
        _, _ = profile(model, inputs=(img,), verbose=False)
        flops = profile(model, inputs=(img,), verbose=False)[0] / 1E9
        fs = ', {} GFLOPS'.format(flops)
    except (ImportError):
        fs = ''
    print(f"Model Summary: {len(list(model.modules()))} layers, {n_p} parameters, \
           {n_g} gradients{fs}")


def disp_circle(img, center):
    thickness = -1
    line_type = 8
    img1 = img.copy()
    radius = 200
    img11 = np.zeros(img.shape)
    cv2.circle(img11,
               center,
               radius,
               (255),
               thickness,
               line_type)
    thickness = 4
    img2 = img.copy()
    cv2.circle(img2,
               center,
               radius,
               (255),
               thickness,
               line_type)
    return img11, img2


def contouring_circle(image):
    """
    Contouring the coronal holes images to keep only the region of interest
    """
    dims= image.shape
    img_zero = np.ones(dims, dtype=np.uint8) * 140

    mask, img2 = disp_circle(image.copy(), (int(dims[0]/2), int(dims[1]/2)))

    mask1 = np.uint8(mask)
    mask = np.invert(mask1)
    mask = np.bitwise_and(mask, img_zero)

    image1 = np.bitwise_and(mask1, image)
    result = mask + image1

    return img2, result


def cluster_corrections(image, mask_im, centers):
    """
    Custom function that masks coronal holes combining
    information from k-means. (It is not clearly based on kmeans)
    
    Centers here are considering standard levels of intersities on image.
    """
    image = cv2.threshold(image, 80, 255, cv2.THRESH_BINARY)[1]
    centers.sort()
    output =  np.ones(image.shape)*255
    
    im_temp = np.bitwise_and(image, mask_im)
    
    output = np.logical_and(output , np.invert(mask_im))
    output = np.uint8(output)*255
    
    output_final = mask_im.copy()
    output_final_ = output_final.copy()
    sc = 1.0
    output_final[output_final_<centers[0]*sc] = 0
    output_final[output_final_>=centers[0]*sc] = 255
    return output_final


def reshape(output, img):
    """ 
    Reshaping images to prepare for the visualization
    """
    center = output[0]
    label = output[1]
    center = np.uint8(center)
    cent_min_loc = np.argmin(center)
    label1 = label.copy()
    lab_img = label1.reshape((img.shape))
    lab_img_ = lab_img.copy()
    lab_img[lab_img_ == cent_min_loc] = 255
    lab_img[lab_img_ != cent_min_loc] = 0
    temp = center.T
    temp = temp.tolist()
    temp = temp[0]
    temp.sort()
    return lab_img