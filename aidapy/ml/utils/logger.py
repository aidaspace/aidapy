import os.path as osp


class Logger:
    """
        Class that keep in txt file all Interface messages
    """
    def __init__(self, experiment_dir=None, experiment_name=None):
        """ Init Logger """
        
        self.path = osp.join(experiment_dir, experiment_name, "log.txt")
        self.log = open(self.path, "a")

    def write(self, message):
        """ Pass message to Logger """
        self.log = open(self.path, "a")
        self.log.write(message+" \n")
        print(message)
        self.log.close()
