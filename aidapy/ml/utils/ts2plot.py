""" Utilities function that can be used from more than one
callback.
"""
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import numpy as np
import io

def ts2plot2im(truth, pred):
    """ Function to turn the timeseries into plot
        and the plot into image.

        Parameters
        ----------
        truth : numpy array

        pred : numpy array

        Returns
        -------
        img : numpy array
            An img of the plot.
    
    """
    truth = truth.squeeze()
    pred = pred.squeeze()
    df = pd.DataFrame({'Pred': pred, 'Truth': truth})
    print (df)
    fig = df.plot(style=['+-', 'o-'], 
                  title='Comparison of Prediction vs Ground Truth')
    fig.set_xlabel("Future timesteps")
    fig.set_ylabel("Values")
   
   #Create a buffer to save a figure then create an image as numpy array 
  
    buf = io.BytesIO()
    fig.figure.savefig(buf, format="png", dpi=180)
    buf.seek(0)
    img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
    buf.close()
    img = cv2.imdecode(img_arr, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  
    
    return img


