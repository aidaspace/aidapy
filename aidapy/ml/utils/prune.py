import numpy as np
import torch
import torch.nn.utils.prune as prune
#from IPython.core.debugger import set_trace


def print_model_sparsity(model):
    """
    Simple log functions.
    """
    for name, module in model.named_modules():
        if isinstance(module, torch.nn.Conv2d)\
            or isinstance(module, torch.nn.Linear):
            print_sparsity(name, module)
        elif isinstance(module, torch.nn.LSTM):
            print_sparsity_LSTM(name, module)


def print_sparsity_LSTM(module_name, module):
    """
    Simple log functions
    """
    c1 = []
    for el in module.named_parameters():
        if "weight" not in el[0]: continue
        c1.append(float(torch.sum(el[1].data == 0)) / float(el[1].data.nelement()))
    print("Sparsity in {} : {:.2f}%".format(module_name, \
          100. * float(sum(c1) / len(c1) )))
    


def print_sparsity(module_name, module):
    """
    Simple log functions
    """
    # e.g. pass model.conv1.weight
    print("Sparsity in {} : {:.2f}%".format(module_name, \
          100. * float(torch.sum(module.weight == 0)) \
            / float(module.weight.nelement())))



def PruneModel(model, conv_amount, fcn_ammount, prune_dimension):
    """
    Prune model using PyTorch internal tools.
    """
    if prune_dimension == "filter":
        pdid = 0
    elif prune_dimension == "channel":
        pdid = 1
    else:
        assert ValueError
    
    for _, module in model.named_modules():
        if isinstance(module, tuple): 
            for el in module:
                if isinstance(module, torch.nn.Conv2d):
                    print("Convolution 2D layer found")
                    prune.ln_structured(module, name="weight", amount=conv_amount,
                                        n=1, dim=pdid)
                elif isinstance(module, torch.nn.Linear):
                    print("Linear layer found")
                    prune.ln_structured(module, name="weight", amount=fcn_ammount,
                                        n=1, dim=pdid)
                elif isinstance(module, torch.nn.LSTM):
                    print("LSTM layer found")
                    for el1 in module.named_parameters():
                        if "weight" in el1[0]:
                            prune.ln_structured(module, name=el1[0], amount=fcn_ammount, n=1, dim=pdid)
        else:
            if isinstance(module, torch.nn.Conv2d):
                print("Convolution 2D layer found")
                prune.ln_structured(module, name="weight", amount=conv_amount,
                                    n=1, dim=pdid)
            elif isinstance(module, torch.nn.Linear):
                print("Linear layer found")
                prune.ln_structured(module, name="weight", amount=fcn_ammount,
                                    n=1, dim=pdid)
            elif isinstance(module, torch.nn.LSTM):
                print("LSTM layer found")
                modulelist = list(module.named_parameters())
                
                param_names = []
                for el in modulelist:
                    param_names.append(el[0])
                el1 = modulelist
                for idx in param_names:
                    if "weight" in idx:
                        el1 = modulelist[param_names.index(idx)]
                        prune.ln_structured(module, name=idx, amount=fcn_ammount, n=1, dim=pdid)
    return model



def MakePrunePermanent(model):
    """
    Makes prune on model permanent (PyTorch implementation structure)
    """
    for _, module in model.named_modules():
        if isinstance(module, torch.nn.LSTM):
            modulelist = list(module.named_parameters())
            param_names = []
            for el in modulelist:
                param_names.append(el[0])
            el1 = modulelist
            for idx in param_names:
                if "weight" in idx:
                    idx = idx.replace("_orig", "")
                    prune.remove(module, idx)
        elif isinstance(module, torch.nn.Conv2d):
            prune.remove(module, "weight")
        elif isinstance(module, torch.nn.Linear): 
            prune.remove(module, "weight")
    return model



def report_pruning(model):
    """
    Analytical report on terminal abount pruned layers.
    """
    print("\n\n")
    print("***"*10 + " Prune Report " + "***"*10)
    for name, module in model.named_modules():
        if isinstance(module, torch.nn.Conv2d):
            print('Convolution layer:' + name + " Sparsity: " + str(100. * float(
                torch.sum(module.weight == 0)) / float(module.weight.nelement()))+"%")
    print("***"*9 + " End of Prune Report " + "***"*9)
    print("\n\n")



def prune_convolutional_layers(model, conv_amount, mode, allkernels=True,
                               ch=0):
    """
    Function that prunes Convolution layers based on selected method and percentage.
    """
    weights_per_layer = []
    num_2_keep = []
    layer_name = []
    print("[INFO]  Pruning is starting. Mode: {}".format(mode))
    for name, module in model.named_modules():
        ct = 0
        if isinstance(module, torch.nn.Conv2d):
            weights = module.weight.clone()
            tensor = weights.cpu().detach()
            f, c, w, h = tensor.shape
            sparsity = str(100. * float(torch.sum(module.weight == 0)) / float(module.weight.nelement()))
            layer_map = tensor.numpy()
            if mode == "filter":
                for i in range(f):
                    sublayer = layer_map[i, :, :, :]
                    x = np.count_nonzero(sublayer)
                    if x == 0:
                        ct += 1
                counter = f - ct
            elif mode == "channel":
                for i in range(c):
                    sublayer = layer_map[:, i, :, :]
                    x = np.count_nonzero(sublayer)
                    if x != 0:
                        ct += 1
                counter = c - ct

            if counter == 0:
                pruned = "No"
                shapeid = 0 if mode == "filter" else 1
                xa = layer_map
                counter = layer_map.shape[shapeid]
            else:
                pruned = "Yes"
                if mode == "filter":
                    xa = np.zeros((int(counter), c, w, h))
                    co = 0
                    for i in range(f):
                        sublayer = layer_map[i, :, :, :]
                        x = np.count_nonzero(sublayer)
                        if x != 0:
                            xa[co, :, :, :] = sublayer
                            co += 1
                elif mode == "channel":
                    xa = np.zeros((f, int(counter), w, h))
                    co = 0
                    for i in range(layer_map.shape[1]):
                        sublayer = layer_map[:, i, :, :]
                        x = np.count_nonzero(sublayer)
                        if x != 0:
                            xa[:, co, :, :] = sublayer
                            co += 1
            print("Layer info: {}, Filters: {}, Channels: {}, Pruned: {}, FINAL Filters: {}, \
                  FINAL Channels: {}, Sparsity: {}%".format(name, f, c, pruned, xa.shape[0], \
                  xa.shape[1], sparsity))
            num_2_keep.append(counter)
            weights_per_layer.append(xa)
            layer_name.append(name)
    print("[INFO]  Pruning is over")
    print("\n\n")
    return weights_per_layer, num_2_keep, layer_name


def prune_fc_layers(model, fc_amount, mode):
    """
    Function that prunes Fully-Connected layers based on selected method and percentage.
    """
    weights_per_layer = []
    num_2_keep = []
    names = []
    k = 0
    for name, module in model.named_modules():
        counter = 0
        if isinstance(module, torch.nn.Linear):
            weights = module.weight.clone()
            tensor = weights.cpu().detach()
            layer_map = tensor.numpy()
            xa = np.zeros(layer_map.shape)
            co = 0
            if mode == "filter":
                for i in range(layer_map.shape[0]):
                    sublayer = layer_map[i, :]
                    x = np.count_nonzero(sublayer)
                    if x != 0:
                        counter += 1
                        xa[co, :] = sublayer
                        co += 1
            elif mode == "channel":
                for i in range(layer_map.shape[1]):
                    sublayer = layer_map[:, i]
                    x = np.count_nonzero(sublayer)
                    if x != 0:
                        counter += 1
                        xa[:, co] = sublayer
                        co += 1
            if k == 0:
                num_2_keep.append(int(counter*(fc_amount)))
            else:
                num_2_keep.append(int(counter*(fc_amount/num_2_keep[-1])))
            weights_per_layer.append(xa)
            names.append(name)
            k += 1
    return weights_per_layer, num_2_keep, names
