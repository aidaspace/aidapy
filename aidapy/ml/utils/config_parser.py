""" Config Parser class responsible to read the arguments from the
.yaml files and transform in the proper format.
"""

import os, sys
import yaml


class ConfigParser:
    """ Config handler """
    def __init__(self, filepath):
        r"""Config Parser Class for reading the options
            from the yml file.

            Parameters
            ----------
            filepath : str 
                Path of the yml file.
        """
        self.data = []
        if os.path.exists(filepath) == False:
            print('please create a yml file with the experiment parameters !!!')
            sys.exit()
        with open(filepath, "r") as file_descriptor:
            self.data=yaml.safe_load(file_descriptor)

    ###############################################
    ########### System Variables ##################
    
    def getExpName(self):
        return self.data['system']['experiment_name']

    def getExpDir(self):
        return self.data['system']['experiment_dir']

    def getResetDir(self):
        return self.data['system']['reset']

    def getUseGPU(self):
        return self.data['system']['use_gpu']

    def getGPUid(self):
        return int(self.data['system']['gpu_id'])
    
    def getRandomSeed(self):
        return int(self.data['system']['random_seed'])
        
    ###############################################
    ################## Data Params ##################    
    def getDataMode(self):
        return self.data['datasets']['datamode']

    def getTrainMode(self):
        try:
            return self.data['datasets']['trainval']
        except:
            return False

    def getTrainDbs(self):
        return self.data['datasets']['train_datasets']

    def getTestDbs(self):
        return self.data['datasets']['test_datasets']

    def getTrainTestSplit(self):
        return self.data['datasets']['split_parts']
        
    ################## Datamode ###################

    def getDataVars(self, name):
        return self.data['datamode'][name]
    
    ###############################################
    ################## Visualization ##############

    def getAddVisLoss(self):
        return self.data['visualization']['losses']

    def getAddModel(self):
        return self.data['visualization']['model_graph']

    def getAddMetrics(self):
        return self.data['visualization']['metrics']

    def getAddValImgs(self):
        try:
            return self.data['visualization']['val_images']
        except:
            return 1

    def getPreprocess(self):
        try:
            return self.data['training']['preprocess']
        except:
            return 1

    def getNumValImgs(self):
        try:
            return int(self.data['visualization']['num_images'])
        except:
            return 1

    def getSaveImgs(self):
        try:
            return self.data['visualization']['save_images']
        except:
            return 1

    def getSaveMode(self):
        return self.data['visualization']['save_mode']

    def getVisName(self):
        return self.data['visualization']['visualizer']

    def getPrintEvery(self):
        return int(self.data['training']['print_every'])

    ###############################################
    ################## Training ###################

    def getMetricDirection(self):
        try:
            return self.data['training']['direction']
        except:
            return "maximize"
        
    def getMode(self):
        return self.data['training']['mode']
    
    def getBatchSize(self):
        return int(self.data['training']['batchSize'])

    def getBatchSize_Val(self):
        try:
            val = int(self.data['training']['batchSize_Val'])
            return val
        except:
            return 1
         
    def getPostProcess(self):
        return self.data['training']['postprocess']

    def getCase(self):
        return self.data['training']['case']

    def getMetrics(self):
        return self.data['training']['metrics']

    def getLossF(self):
        return self.data['training']['loss']

    def getEpochs(self):
        return int(self.data['training']['n_epochs'])

    ###############################################
    ################## Optimizer ##############

    def getLR(self):
        return float(self.data['optimizer']['lr'])

    def getWeightDecay(self):
        try:
            return float(self.data['optimizer']['weight_decay'])
        except:
            return 0

    def getOptim(self):
        return self.data['optimizer']['optim']

    def getMomentum(self):
        try:
            return float(self.data['optimizer']['momentum'])
        except:
            return False
        
    def getBetas(self):
        return (float(self.data['optimizer']['betas'][0]),
                float(self.data['optimizer']['betas'][1]))

    def getEpsilon(self):
        return float(self.data['optimizer']['epsilon'])

    def getDecay(self):
        return self.data['optimizer']['lr_decay_iters']

    def getGamma(self):
        return float(self.data['optimizer']['gamma'])


    ###############################################
    ########### Model Variables ##################
    def getUnsuperflag(self):
        try:
            item = self.data['model']['unsuper_flag']
            return item
        except:
            return False

    def getDataRandom(self):
        return int(self.data['model']['random'])

    def getModelCentroids(self):
        return int(self.data['model']['K']) 

    def getModelAttempts(self):
        return int(self.data['model']['attempts'])    

    def getMaxIter(self):
        return int(self.data['model']['max_iter'])

    def getAccuracy(self):
        return int(self.data['model']['epsilon'])

    def getModelType(self):
        return self.data['model']['type']

    def getModelDepth(self):
        return self.data['model']['depth']
        
            
    def getModelArchitecture(self):
        return self.data['hpo_params']['architecture']

    def getModelInputNc(self):
        return int(self.data['model']['input_nc'])

    def getModelOutputNc(self):
        return int(self.data['model']['output_nc'])

    def getNoNlocalMode(self):
        return self.data['model']['nonlocal_mode']
    
    def getCheckDir(self):
        return self.data['model']['checkpoints_dir']

    def getCheckSaveAll(self):
        return self.data['model']['save_all']

    def getPreTrained(self):
        return self.data['model']['pretrained_dir']

    def getCheckResume(self):
        return self.data['model']['resume']

        
    ###############################################
    ################## HPO ########################
    def getHPOlrs(self):
        return self.data['hpo_params']['lr_range']

    def getHPOoptims(self):
        return self.data['hpo_params']['optimizers']

    def getHPOflag(self):
        try:
            item = self.data['hpo']['hpo_flag']
            return item
        except:
            return False

    def getHPOdirection(self):
        return self.data['hpo']['direction']
        
    def getHPOntrials(self):
        return self.data['hpo']['n_trials']

    def getHPOtimeout(self):
        return self.data['hpo']['timeout']       

    ###############################################
    ################## Pruning #####################
    
    def getPruningFlag(self):
        try:
            item = self.data['pruning']['prune_flag']
            return item
        except:
            return False

    def getPruningPercent(self):
        return self.data['pruning']['percent']

    def getPruningFtEpochs(self):
        return self.data['pruning']['finetune_epochs']

    def getPruningMode(self):
        return self.data['pruning']['prune_mode']

    def getPruningType(self):
        return self.data['pruning']['prune_type']
    
    def getPruner(self):
        return self.data['pruning']['pruner']

    ###############################################
    ############### Quantization ##################
    
    def getQuantizationFlag(self):
        try:
            item = self.data['quantization']['quantization_flag']
            return item
        except:
            return False

    def getQuantizationType(self):
        return self.data['quantization']['quantization_type']

    def getQuantizationBits(self):
        return self.data['quantization']['quantization_bits']
