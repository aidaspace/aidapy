import torch
import torch.nn as nn
import torch.nn.functional as F
from importlib import import_module


class Loss(nn.modules.loss._Loss):
    """
        High Level wrapper that creates the losses
     that you have defined in the config file.
    """
    def __init__(self, vis=None, log=None, get_print_every=None, get_loss_split=None, gpu_use=None):
        r"""Initiallization of the Loss Class. Constructs
            the wrapper.
                        
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.

            vis : Visualizer class
                Contains all the visualization techniques.

            log : Logger class
                Contains all the logging techniques.
        """
        super(Loss, self).__init__()
        self.loss = []
        self.loss_module = nn.ModuleList()
        if vis is not None:
            self.writer = vis
        else:
            self.writer = False
        if log is not None:
            self.logger = log
            self.logger.write('Preparing loss function:')
        else:
            self.logger = False
        self.print_every = get_print_every
        for loss in get_loss_split:
            weight, loss_type = loss.split('*')
            m = import_module('aidapy.ml.loss.loss_func')            
            loss_function = getattr(m, loss_type)()
    
            self.loss.append({
                'type': loss_type,
                'weight': float(weight),
                'function': loss_function})

        if len(self.loss) > 1:
            self.loss.append({'type': 'Total', 'weight': 0,
                              'function': None})

        for l in self.loss:
            if l['function'] is not None:
                if self.logger:
                    self.logger.write('{:.3f} * {}'.format(l['weight'], l['type']))
                self.loss_module.append(l['function'])
                self.loss_module.append(l['function'])

        device = torch.device('cpu' if not gpu_use else 'cuda')
        self.loss_module.to(device)

    def forward(self, x, y, batch):
        r"""Forward function of the loss wrapper
            
            Parameters
            ----------
            x : tensor
                The input tensor.

            y : tensor
                The ground truth tensor

            batch : int
                The iteration number

            Returns
            -------
            loss_sum : tensor
                The loss after the calculation.
        """

        losses = []
        for i, l in enumerate(self.loss):
            if l['function'] is not None:
                loss = l['function'](x, y)
                effective_loss = l['weight'] * loss
                losses.append(effective_loss)
                if batch % self.print_every == 0:
                    name = '{:.3f}_{}'.format(l['weight'], l['type'])
                    self.display_loss(name, effective_loss, batch)
        loss_sum = sum(losses)
        if batch % self.print_every == 0:
            self.display_loss('Total_Loss', loss_sum/len(losses), batch)
            self.log_loss(loss_sum/len(losses), batch)
        return loss_sum

    def step(self):
        r"""Scheduler of the loss
        """
        for l in self.get_loss_module():
            if hasattr(l, 'scheduler'):
                l.scheduler.step()

    def display_loss(self, name, effective_loss, batch):
        r"""Function to send the loss in tensorboard

            Parameters
            ----------
            name : str
                The name of the loss

            effective_loss : float
                The value of the loss

            batch : int
                The number of the iteration
        """
        if self.writer:
            self.writer.add_losses(name,
                               effective_loss, batch)

    def log_loss(self, loss_sum, batch):
        r"""Function to log the loss
        
            Parameters
            ----------
            effective_loss : float
                The value of the loss

            batch : int
                The number of the iteration
        """
        if self.logger:
            self.logger.write('Total Loss in iter {} \
                is {:.4f}'.format(batch, loss_sum))

    def get_loss_module(self):
        r"""Returns the loss module
        """
        return self.loss_module
