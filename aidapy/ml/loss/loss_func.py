import torch
import torch.nn as nn
import torch.nn.functional as F


class BCEWithLogitsLossPadding(nn.Module):
    """
    Implements BCEWithLogitsLossPadding using padding 16, due to
    UNet behavior.
    """
    def __init__(self, padding=16):
        super().__init__()
        self.padding = padding

    def forward(self, input_value, target):
        """ calculation function """
        input_val = input_value.squeeze_(
            dim=1)[:, self.padding:-self.padding, self.padding:-self.padding]
        target_val = target.squeeze_(
            dim=1)[:, self.padding:-self.padding, self.padding:-self.padding]
        return F.binary_cross_entropy_with_logits(input_val, target_val)





class BCEWithLogitsLoss(nn.Module):
    """
    Implements the BCE loss
    """
    def __init__(self):
        super().__init__()

    def forward(self, input_val, target):
        """ calculation function """
        return F.binary_cross_entropy_with_logits(input_val, target)



class IoULoss(nn.Module):
    """ Loss class for IoU """
    def __init__(self, weight=None, size_average=True):
        super(IoULoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        """ calculation function """
        # comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        # intersection is equivalent to True Positive count
        # union is the mutually inclusive area of all labels & predictions
        intersection = (inputs * targets).sum()
        total = (inputs + targets).sum()
        union = total - intersection

        IoU = (intersection + smooth)/(union + smooth)

        return 1 - IoU


#PyTorch
class DiceBCELoss(nn.Module):
    """ Loss class for DiceBSE """
    def __init__(self, weight=None, size_average=True):
        super(DiceBCELoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        """ calculation function """
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        intersection = (inputs * targets).sum()
        dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)
        BCE = F.binary_cross_entropy(inputs, targets, reduction='mean')
        Dice_BCE = BCE + dice_loss

        return Dice_BCE


#PyTorch
class DiceLoss(nn.Module):
    """ Loss class for DICE """
    def __init__(self, weight=None, size_average=True):
        super(DiceLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        """ calculation function """
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        intersection = (inputs * targets).sum()
        dice = (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)

        return 1 - dice



class FocalLoss(nn.Module):
    """ Loss class for FocalLoss """
    def __init__(self, weight=None, size_average=True):
        super(FocalLoss, self).__init__()

    def forward(self, inputs, targets, alpha=0.8, gamma=2, smooth=1):
        """ calculation function """
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        #first compute binary cross-entropy
        BCE = F.binary_cross_entropy(inputs, targets, reduction='mean')
        BCE_EXP = torch.exp(-BCE)
        focal_loss = alpha * (1-BCE_EXP)**gamma * BCE

        return focal_loss


class TverskyLoss(nn.Module):
    """ Loss class for TverskyLoss """
    def __init__(self, weight=None, size_average=True):
        super(TverskyLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1, alpha=0.5, beta=0.5):
        """ calculation function """
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        #True Positives, False Positives & False Negatives
        TP = (inputs * targets).sum()
        FP = ((1-targets) * inputs).sum()
        FN = (targets * (1-inputs)).sum()

        Tversky = (TP + smooth) / (TP + alpha*FP + beta*FN + smooth)

        return 1 - Tversky




class FocalTverskyLoss(nn.Module):
    """ Loss class for FocalTverskyLoss """
    def __init__(self, weight=None, size_average=True):
        super(FocalTverskyLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1, alpha=0.5, beta=0.5,
                gamma=1):
        """ calculation function """
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = F.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        #True Positives, False Positives & False Negatives
        TP = (inputs * targets).sum()
        FP = ((1-targets) * inputs).sum()
        FN = (targets * (1-inputs)).sum()

        Tversky = (TP + smooth) / (TP + alpha*FP + beta*FN + smooth)
        FocalTversky = (1 - Tversky)**gamma

        return FocalTversky




class ComboLoss(nn.Module):
    """ Loss class for combination of losses """
    def __init__(self, weight=None, size_average=True):
        # ALPHA = 0.5 # < 0.5 penalises FP more, > 0.5 penalises FN more
        # BETA = 0.5 #weighted contribution of modified CE loss compared to Dice loss
        super(ComboLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1, alpha=0.5, beta=0.5):
        """ calculation function """
        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        #True Positives, False Positives & False Negatives
        intersection = (inputs * targets).sum()
        dice = (2. * intersection + smooth)\
            / (inputs.sum() + targets.sum() + smooth)

        inputs = torch.clamp(inputs, 1e-1, 1.0 - 1e-1)
        out = - (alpha * ((targets * torch.log(inputs)) + \
                ((1 - alpha) * (1.0 - targets) * torch.log(1.0 - inputs))))
        weighted_ce = out.mean(-1)
        combo = (beta * weighted_ce) - ((1 - beta) * dice)

        return combo

class L1(nn.Module):
    """ Loss class for L1  """
    def __init__(self):
        super().__init__()
        self.lossfun = nn.L1Loss()

    def forward(self, inputs, targets):
        """ calculation of metric """
        return self.lossfun(inputs, targets)

class MSE(nn.Module):
    """ Loss class for MSE """
    def __init__(self, weight=None, size_average=True):
        super(MSE, self).__init__()
        self.lossfun = nn.MSELoss()

    def forward(self, inputs, targets):
        #flatten label and prediction tensors
        inputs = inputs.double()
        targets = targets.double()
        val = self.lossfun(inputs, targets)
        # val = val.to(torch.float64)
        # val = val.double()
        return val