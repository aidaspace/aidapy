"""This module implements a simple CNN
    author: Katakis Sofoklis
"""
import torch
import torch.nn as nn
import random

def make_model(model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None, pruning_percent=None, gpu_use=None, random_seed=None):
    return DCNN(model_input_nc, model_output_nc, model_depth, pruning_flag, pruning_percent, gpu_use, random_seed)

class DCNN(nn.Module):
    def __init__(self, model_input_nc=None, model_output_nc=None, model_depth=None, 
                        pruning_flag=None, pruning_percent=None, gpu_use=None, random_seed=None):
        """Initialize a multilayer perceptron

        :param layer_sizes: list of layer sizes
        :param afunc: activation function

        """
        
        random.seed(random_seed)
        torch.manual_seed(random_seed)
        torch.backends.cudnn.deterministic = True

        super().__init__()
        input_size = model_input_nc
        output_size = model_output_nc
        
        layer_sizes = model_depth
        afunc= [nn.ReLU, nn.LeakyReLU, nn.LeakyReLU, nn.LeakyReLU, nn.LeakyReLU, nn.ReLU]
        
        assert len(layer_sizes) == len(afunc)

        layers_siz = list()
        funcs = list()
        for i, layer in enumerate(layer_sizes):
            layers_siz.append(layer)
            funcs.append(afunc[i])

        self.conv1 = nn.Conv2d(input_size,
                     layer_sizes[0], kernel_size=(1,1), padding=1, groups=1, bias=False)
       
        n_layers = len(layers_siz) -1
        self.net = nn.Sequential()

        for i in range(n_layers):
            self.net.add_module('layer_{}'.format(i),
                        nn.Conv2d(layers_siz[i], 
                        layers_siz[i+1], kernel_size=(3, 1),
                         padding=1, groups=1, bias=False))
            
            # Add activation function, except for the last layer
             
            if i%3==0 and i>0:
                self.net.add_module('afunc_{}'.format(i), funcs[i]())
                self.net.add_module('pool_{}'.format(i), nn.MaxPool2d((3,1),1))
                self.net.add_module('dropout_{}'.format(i), nn.Dropout(0.2))
                
        self.conv2 = nn.Conv2d(layer_sizes[-1], 8, kernel_size=1, padding=1, groups=1, bias=False)

        
        self.out1 = nn.Linear(960, output_size, bias=False)
        self.net.apply(self.init_weights)

    def init_weights(self, m):
        torch.manual_seed(999)
        if type(m) == nn.Conv2d:
            nn.init.xavier_uniform_(m.weight.data, gain=1)

    def forward(self, x):

        x_t = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)
        x = self.conv1(x_t)
        x = self.net(x)
        x = self.conv2(x)
        x = x.reshape(x.shape[0], x.shape[2]*x.shape[1]*x.shape[3])
        x = self.out1(x)
        return  x
