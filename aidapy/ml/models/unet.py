import torch.nn as nn
import torch
import torch.nn.functional as F
import random

def make_model(model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None, pruning_percent=None, 
                gpu_use=None, random_seed=None):

    return UNet(model_input_nc, model_output_nc, model_depth, pruning_flag, pruning_percent, gpu_use, random_seed)


class UNetConvBlock(nn.Module):
    """
        UNet simple CNN layer block
    """
    def __init__(self, in_size, out_size, padding, batch_norm):
        """ Init and parameterized Unet CNN block"""
        super(UNetConvBlock, self).__init__()
        block = []

        block.append(nn.Conv2d(in_size, out_size, kernel_size=3,
                               padding=int(padding)))
        if batch_norm:
            block.append(nn.BatchNorm2d(out_size))
        block.append(nn.ReLU())

        block.append(nn.Conv2d(out_size, out_size, kernel_size=3,
                               padding=int(padding)))
        if batch_norm:
            block.append(nn.BatchNorm2d(out_size))
        block.append(nn.ReLU())

        self.block = nn.Sequential(*block)

    def forward(self, x):
        """feed forward"""
        out = self.block(x)
        return out


def center_crop(layer, target_size):
    if layer.dim() == 4:
        # Cropping feature maps
        _, _, layer_height, layer_width = layer.size()
        diff_y = (layer_height - target_size[0]) // 2
        diff_x = (layer_width - target_size[1]) // 2
        return layer[
            :, :, diff_y: (diff_y + target_size[0]), diff_x: (diff_x + target_size[1])
            ]

    # If dimension is not 4, assume that we are cropping ground truth labels
    assert layer.dim() == 3
    _, layer_height, layer_width = layer.size()
    diff_y = (layer_height - target_size[0]) // 2
    diff_x = (layer_width - target_size[1]) // 2
    return layer[
        :, diff_y: (diff_y + target_size[0]), diff_x: (diff_x + target_size[1])
        ]


class UNetUpBlock(nn.Module):
    """
        Class that creates expanding Convolution Layers 
          for UNet graph schematic
    """
    def __init__(self, in_size, out_size, up_mode, padding, batch_norm):
        """
            Init of layer and parameterization
        """
        super(UNetUpBlock, self).__init__()
        if up_mode == 'upconv':
            self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2,
                                         stride=2)
        elif up_mode == 'upsample':
            self.up = nn.Sequential(
                nn.Upsample(mode='bilinear', scale_factor=2),
                nn.Conv2d(in_size, out_size, kernel_size=1),
            )
        self.conv_block = UNetConvBlock(in_size, out_size, padding, batch_norm)
        self.padding = padding

    def forward(self, x, bridge):
        """
            Feed-forward
        """
        up = self.up(x)
        if self.padding:
            out = torch.cat([up, bridge], 1)
        else:
            crop1 = center_crop(bridge, up.shape[2:])
            out = torch.cat([up, crop1], 1)
        out = self.conv_block(out)
        return out


class UNet(nn.Module):
    def __init__(self, model_input_nc=None, model_output_nc=None, model_depth=None, 
                    pruning_flag=None, pruning_percent=None, gpu_use=None, random_seed=None, original=False):
        """
        Implementation of
        U-Net: Convolutional Networks for Biomedical Image Segmentation
        Args:
            in_channels (int): number of input channels
            input_size_hw: a tuple of (height, width) of the input images
            n_classes (int): number of output channels
            depth (int): depth of the network
            wf (int): number of filters in the first layer is 2**wf
            padding (bool): if True, apply padding such that the input shape
                            is the same as the output.
                            This may introduce artifacts
            batch_norm (bool): Use BatchNorm prior to layers with an
                               activation function
            up_mode (str): one of 'upconv' or 'upsample'.
                           'upconv' will use transposed convolutions for
                           learned upsampling.
                           'upsample' will use bilinear upsampling.
        """
        random.seed(random_seed)
        torch.manual_seed(random_seed)
        torch.backends.cudnn.deterministic = True

        super(UNet, self).__init__()

        self.in_channels = model_input_nc
        self.n_classes = model_output_nc
    
        self.depth = model_depth
        self.wf = 6
        if original or not pruning_flag:
            self.scale = 1.0
        else:
            self.scale = pruning_percent

        self.padding = True
        self.batch_norm = True
        self.up_mode = 'upconv'
        self.down_path = nn.ModuleList()
        self.up_path = nn.ModuleList()

        assert self.up_mode in ('upconv', 'upsample')

        prev_channels = self.in_channels
        for i in range(self.depth):
            new_channels = int((2 ** (self.wf + i)) * self.scale)
            self.down_path.append(
                UNetConvBlock(prev_channels, new_channels, self.padding,
                              self.batch_norm)
            )
            prev_channels = new_channels

        for i in reversed(range(self.depth - 1)):
            new_channels = int((2 ** (self.wf + i)) * self.scale)
            self.up_path.append(
                UNetUpBlock(prev_channels, new_channels, self.up_mode,
                            self.padding, self.batch_norm)
            )
            prev_channels = new_channels

        self.last = nn.Conv2d(prev_channels, self.n_classes, kernel_size=1)

    def forward(self, x):
        """ Feed forward""" 
        blocks = []
        for i, down in enumerate(self.down_path):
            x = down(x)
            if i != len(self.down_path) - 1:
                blocks.append(x)
                x = F.max_pool2d(x, 2)

        for i, up in enumerate(self.up_path):
            x = up(x, blocks[-i - 1])

        x = self.last(x)
        return x
