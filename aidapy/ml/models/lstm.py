"""LSTM model used in the dst prediction
Neural network model consisting of a single LSTM and a dense layer.
The input is a three-dimensional torch Tensor of dimensions (batches,
 time_backward, input_size),nas described in the torch documentations
  (https://pytorch.org/docs/stable/nn.html#lstm)
The output is a three-dimensionsal torch Tensor of dimensions (batches, 1,
 output_size)
author: Brecht Laperre
contributor: Romain Dupuis
e-mail: brecht.laperre@kuleuven.be
"""
import torch
import torch.nn as nn
import random

def make_model(model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None, pruning_percent=None, gpu_use=None, random_seed=None):
    """ Call model constructor """
    return LSTMnn(model_input_nc, model_output_nc, model_depth, pruning_flag, pruning_percent, gpu_use, random_seed)


class LSTMnn(nn.Module):
    def __init__(self, model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None,
                pruning_percent=None, gpu_use=None, random_seed=None, num_layers=2, hidden_size=256):
 
        """Model with a single LSTM module and a dense layer
        Input:
            input_size: Number of expected features for x
            num_layers: Number of layers in the LSTM
            hidden_size: Number of neurons in the hidden layer of the LSTM
            output_size: Number of timesteps in the future
        """
        # input_size, num_layers, hidden_size, output_size
        # train_in.shape[2], 1, 50, train_out.shape[2]
        random.seed(random_seed)
        torch.manual_seed(random_seed)
        torch.backends.cudnn.deterministic = True

        super(LSTMnn, self).__init__()
        self.cpu = not gpu_use
        self.device = torch.device('cpu' if self.cpu else 'cuda')
        
        num_layers = model_depth
        input_size = model_input_nc
        output_size = model_output_nc

    
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers,
                            batch_first=True)

        self.hidden2_to_output = nn.Linear(hidden_size, output_size)
        self.num_layers = num_layers
        self.hidden_size = hidden_size

    def init_hidden(self, batch_size):
        """ Create hidden LSTM layers """
        return (torch.randn(self.num_layers, batch_size,
                            self.hidden_size).to(self.device),
                torch.randn(self.num_layers, batch_size,
                            self.hidden_size).to(self.device))

    def forward(self, x):
        """Input:
            x: Tensor of shape (batch_size, time_steps, input_size)
        Output:
            Tensor of shape (batch_size, 1, output_size)
        """
        batch_size = x.shape[0]
        hidden_state = self.init_hidden(batch_size)

        x = x.float()
        # out: Tensor of shape (batches, 1, hidden_size)

        out, hidden_state = self.lstm(x, hidden_state)
        # transformed to (batches, 1, output_size) using the dense layer
        return self.hidden2_to_output(out[:, -1:])
