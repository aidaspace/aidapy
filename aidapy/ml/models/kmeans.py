import cv2
import random 

def make_model(K=None, attempts=None, max_iter=None, epsilon=None, random_seed=None):
    return KMeans(K, attempts, max_iter, epsilon, random_seed)


class KMeans():
    def __init__(self, K=None, attempts=None, max_iter=10, epsilon=1.0, random_seed=None):
        """Model with a Kmeans algorithm

        Parameters
        ----------
        
            K : int
                Number of centroids

            attempts: int 
                How many time to run the Kmeans

            max_iter: int
                Maximum iterations to run the Kmeans

            epsilon: int
                Accuracy achived by the Kmeans
        """
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, max_iter, epsilon)
        self.K=K
        self.attempts = attempts
        random.seed(random_seed)

    def forward(self, vectorized):
        """Input:
            vectorized : numpy array

        Output:
            center : numpy array
                Array of centers of clusters
            label: numpy array
                Array of labels assigned 
        """
        ret, label, center = cv2.kmeans(vectorized, self.K, None, self.criteria, self.attempts, cv2.KMEANS_PP_CENTERS)
        return [center, label]