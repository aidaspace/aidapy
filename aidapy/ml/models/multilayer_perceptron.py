import torch
import torch.nn as nn
import random

def make_model(model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None, pruning_percent=None, gpu_use=None, random_seed=None):
    return MLPnn(model_input_nc, model_output_nc, model_depth, pruning_flag, pruning_percent, gpu_use, random_seed)

class MLPnn(nn.Module):
    
    def __init__(self, model_input_nc=None, model_output_nc=None, model_depth=None, pruning_flag=None, pruning_percent=None,
                 gpu_use=None, random_seed=None, hidden_size=50):
        
        random.seed(random_seed)
        torch.manual_seed(random_seed)
        torch.backends.cudnn.deterministic = True

        super().__init__()  
        self.cpu = not gpu_use
        self.device = torch.device('cpu' if self.cpu else 'cuda')
        
        input_size  = model_input_nc
        output_size = model_output_nc       
        
        self.num_layers  = model_depth    
        self.hidden_size = hidden_size
        self.activation = nn.ReLU()
        
        h = self.hidden_size
        self.add_module('lin_0', nn.Linear(input_size, h))
        if self.num_layers>2:
            for i in range(1, self.num_layers-1):
                self.add_module('lin_{}'.format(i), nn.Linear(h, h//2))
                h = h//2 
                          
        self.output = nn.Linear(h, output_size)

    def forward(self, x):
        x = x.float()
        
        for i in range(self.num_layers-1):
            x = self._modules['lin_{}'.format(i)](x)
            x = self.activation(x) if (i<self.num_layers-1) else x
        out = self.output(x[:,-1:])      
        return out

            
