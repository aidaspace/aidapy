""" This module is a high-level model wrapper.
"""
import os
import os.path as osp
from importlib import import_module
import pickle


class Model_Unsuper():
    def __init__(self, K=None, attempts=None, max_iter=None,
                            epsilon=None, get_model_type=None, expdir=None, expName=None, check_save_all=None,
                            check_resume=None, pretrained=None, random_seed=None):
        r"""High-level wrapper of the model for the unsupervised use cases.

        Parameters
        ----------

            K : int
                Number of centroids

            attempts: int 
                How many times to run the Kmeans

            max_iter: int
                Maximum iterations to run the Kmeans

            epsilon: int
                Accuracy achieved by the Kmeans

            get_model_type : str
                The model we are using
               
        """
        
        print('Making model...')

        self.expdir = expdir
        self.expName = expName
        self.resume = check_resume
        self.pre_path = pretrained

        #Create paths
        self.create_paths()
        
        #Import the model you want from this module
        module = import_module('aidapy.ml.models.' + get_model_type)

        self.model = module.make_model(K=K, attempts=attempts, max_iter=max_iter, epsilon=epsilon, random_seed=random_seed)
        
        # Load Pretrained weights if you want
        self.load()

        # Save options
        self.save_models = check_save_all

    def create_paths(self):
        r"""Creates important paths
        """
        exp_dir = self.expdir
        exp_folder = self.expName 
        total_dir = osp.join(exp_dir, exp_folder)
        self.apath = osp.join(total_dir, "checkpoints")

    def forward(self, x):
        r"""Forward method of the class
            to perform inference.

            Parameters
            ----------
            x : numpy array
                The input of the model

            Returns
            -------
            output : numpy array
                The output of the model
        """
        output = self.model.forward(x)
        return output

    def save(self, mname):
        r"""Method to save the checkpoints.

            Parameters
            ----------
            mname : str
                The name of the model

        """     
        
        if self.save_models:
            with open(os.path.join(self.apath, mname + 'latest.sav'), 'wb') as f:
                pickle.dump(self.model, f)

    def load(self):
        r"""Method to load the pre-trained weights
            into the model.
        """ 
        if self.resume:
            print("Load the model from {}".format(self.pre_path))
            self.model = pickle.load(open(self.pre_path, 'rb'))
            

        
  

    