""" This module is a high-level model wrapper.
"""
import os
import os.path as osp
from importlib import import_module
import torch
import torch.nn as nn


class Model(nn.Module):
    def __init__(self, vis=None, model_input=None, model_output=None, model_depth=None, 
                        pruning_flag=None, pruning_percent=None, gpu_use=None, expdir=None, expName=None, 
                        model_type=None, check_resume=None, pretrained=None, check_save_all=None, add_model=None, 
                        random_seed=None):
        r"""High-level wrapper of the model module and control
            the CNN models that exist in the use cases.

            Parameters
            ----------
            cfg : Config class
                        Contains all the user options.
            
            vis : Visualization class
                The visualization utility class.
               
        """
      
        super(Model, self).__init__()
        print('Making model...')

        self.expdir = expdir
        self.expName = expName
        #self.t_number = t_number
        #Create paths
        self.create_paths()
        
        #Choose Device
        self.cpu = not gpu_use
        self.device = torch.device('cpu' if self.cpu else 'cuda')
        
        #Import the model you want from this module
        module = import_module('aidapy.ml.models.' + model_type)

        self.model = module.make_model(model_input_nc=model_input, model_output_nc=model_output, 
                                        model_depth=model_depth, pruning_flag=pruning_flag, pruning_percent=pruning_percent,
                                        gpu_use=gpu_use, random_seed=random_seed)
        self.model = self.model.to(self.device)
        
        # Load Pretrained weights if you want
        self.resume = check_resume
        self.pre_path = pretrained
        self.load()
        
        # Save options
        self.save_models = check_save_all
        
        #  options
        self.writer = vis
        self.flag_graph = add_model

    def create_paths(self):
        r"""Creates important paths
        """
        exp_dir = self.expdir
        exp_folder = self.expName 
        total_dir = osp.join(exp_dir, exp_folder)
        self.apath = osp.join(total_dir, "checkpoints")

    def forward(self, x):
        r"""Forward method of the class
            to perform inference.

            Parameters
            ----------
            x : tensor
                The input tensor

            Returns
            -------
            tensor
             The output tensor.
        """
        if self.flag_graph:
            self.add_graph(x)
            self.flag_graph = False
        return self.model(x)

    def save(self, epoch, mname, is_best=False):
        r"""Method to save the checkpoints.

            Parameters
            ----------
            epoch : int
                The current epoch

            mname : str
                The name of the model

            is_best : bool
                If it is the best models so far.
        """
        """
        save_dirs = [os.path.join(self.apath, mname + '{}-latest.pt'.format(self.t_number))]
        if is_best:
            save_dirs.append(os.path.join(self.apath, mname + '{}-best.pt'.format(self.t_number)))
        if self.save_models:
            save_dirs.append(
                os.path.join(self.apath, mname + '{}.pt'.format(epoch))
            )
        for s in save_dirs:
            torch.save(self.model.state_dict(), s)
            """
        save_dirs = [os.path.join(self.apath, mname + 'latest.pt')]
        if is_best:
            save_dirs.append(os.path.join(self.apath, mname + 'best.pt'))
        if self.save_models:
            save_dirs.append(
                os.path.join(self.apath, mname + '{}.pt'.format(epoch))
            )
        for s in save_dirs:
            torch.save(self.model.state_dict(), s)



    def add_graph(self, x):
        r"""Method to send the model graph in the
            tensorboard

            Parameters
            ----------
            x : tensor
                The tensor to perform forward
        """
        self.writer.add_model(self.model, x)

    def load(self):
        r"""Method to load the pre-trained weights
            into the model.
        """
        load_from = None
        kwargs = {}
        if self.cpu:
            kwargs = {'map_location': lambda storage, loc: storage}

        if self.resume:
            load_from = torch.load(
                os.path.join(self.pre_path),
                **kwargs
            )

        if load_from:
            print("Load the model from {}".format(self.pre_path))
            self.model.load_state_dict(load_from)
