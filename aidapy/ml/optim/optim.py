""" Generic module to create the optimizer.
"""
import os
import torch
import torch.optim as optim
import torch.optim.lr_scheduler as lrs


def make_optimizer(model=None, LR=None, weight_decay=None, getoptim=None, momentum=None, betas=None, epsilon=None, decay_split=None, gamma=None,
                     flag=True, lr=1e-3, optimizer_name='Adam'):
    r"""Function to build the optimizer needed for training.
    
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        model : Model class
            The CNN model to extract the learnable parameters.

        flag : bool
            If you want to use optuna compatible optimizer.
            
        lr : float
            The learning rate of the training.

        optimizer_name : str
            The name of the optimizer. Must be compatible with with 
            pytorch optimizers name.       

        Returns
        ----------
        optimizer : Optimizer PyTorch class
            Class compatible with pytorch for the training procedure.
    """

    # optimizer
    trainable = filter(lambda x: x.requires_grad, model.parameters())
    if flag:
        kwargs_optimizer = {'lr': LR,
                            'weight_decay': weight_decay}
        if getoptim == 'SGD':
            optimizer_class = optim.SGD
            kwargs_optimizer['momentum'] = momentum
        elif getoptim == 'ADAM':
            optimizer_class = optim.Adam
            kwargs_optimizer['betas'] = betas
            kwargs_optimizer['eps'] = epsilon
        elif getoptim == 'RMSprop':
            optimizer_class = optim.RMSprop
            kwargs_optimizer['eps'] = epsilon
    else:
        optimizer_class = getattr(optim, optimizer_name)
        kwargs_optimizer = {}
        kwargs_optimizer['lr'] = lr
        kwargs_optimizer['weight_decay'] = weight_decay

    
    # scheduler
    milestones = list(map(lambda x: int(x), decay_split))
    kwargs_scheduler = {'milestones': milestones, 'gamma': gamma}
    scheduler_class = lrs.MultiStepLR

    class CustomOptimizer(optimizer_class):
        def __init__(self, *args, **kwargs):
            super(CustomOptimizer, self).__init__(*args, **kwargs)
            self.scheduler = None

        def _register_scheduler(self, scheduler_class, **kwargs):
            """ Register scheduler """
            self.scheduler = scheduler_class(self, **kwargs)

        def save(self, save_dir):
            """ Save scheduler """
            torch.save(self.state_dict(), self.get_dir(save_dir))

        def load(self, load_dir, epoch=1):
            """ Load scheduler """
            self.load_state_dict(torch.load(self.get_dir(load_dir)))
            if epoch > 1:
                for _ in range(epoch): 
                    self.scheduler.step()

        def get_dir(self, dir_path):
            """ Load scheduler dir from config """
            return os.path.join(dir_path, 'optimizer.pt')

        def schedule(self):
            """ Run scheduler """
            self.scheduler.step()

        def get_lr(self):
            """ Return current LR """
            return self.scheduler.get_last_lr()[0]

        def get_last_epoch(self):
            """ Return last epoch """
            return self.scheduler.last_epoch
    
    optimizer = CustomOptimizer(trainable, **kwargs_optimizer)
    optimizer._register_scheduler(scheduler_class, **kwargs_scheduler)
    return optimizer
