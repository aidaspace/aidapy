""" Module that contains the builders

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""
from aidapy.ml.utils.logger import Logger
from aidapy.ml.visualization import Visualizer
from aidapy.ml.data import Data
from aidapy.ml.data.data_unsuper import Data_Unsuper
from aidapy.ml.loss import Loss
from aidapy.ml.metrics import Metrics
from aidapy.ml.postprocess import PostProcessor
from aidapy.ml.callbacks import *
from aidapy.ml.callbacks.unspr_checkpoint import UnsupervisedCheckpoint
from aidapy.ml.optim import make_optimizer
from aidapy.ml.models import Model
from aidapy.ml.models.model_unsuper import Model_Unsuper

def build_visualizer(cfg):
    r"""Function to build the Visualization class for the use cases.

        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        Returns
        ----------
        vis : Visualizer class
            Contains the visualization utility class.
    """
    exp_dir = cfg.getExpDir() #Variable with experiment directory
    exp_name = cfg.getExpName() #Variable with experiment name
    vis = Visualizer(experiment_dir=exp_dir, experiment_name=exp_name)
    return vis

def build_logger(cfg):
    r"""Function to build the Logger class for the use cases.
    
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        Returns
        ----------
        log : Logger class
            Contains the logger utility class.
    """
    exp_dir = cfg.getExpDir()
    exp_name = cfg.getExpName() 
    log = Logger(experiment_dir=exp_dir,experiment_name=exp_name)
    return log

def build_dataloader(cfg, log=None):
    r"""Function to build the Dataloader class for the use cases.
        
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        log : Logging class
            The logging utility class.

        Returns
        ----------
        data : Data class
            Contains the dataloader class.
    """
    datamode = cfg.getDataMode()
    datapath = cfg.getDataVars(datamode)
    random_seed = cfg.getRandomSeed()

    try:
        train_dbs = cfg.getTrainDbs() #Error with 5.3
    except:
        train_dbs = None

    if cfg.getUnsuperflag():
        random = cfg.getDataRandom()
        train_dbs = cfg.getTrainDbs()
        data = Data_Unsuper(log, get_train_dbs=train_dbs, data_Vars=datapath, k=random, random_seed=random_seed)
    else:
        try:
            train_mode = cfg.getTrainMode() #Error with 5.3
        except:
            train_mode = None
        
        batch_size_val = cfg.getBatchSize_Val()
        gpu_use = cfg.getUseGPU()
        mode = cfg.getMode()
       
        try:
            train_test_split = cfg.getTrainTestSplit() #Error with 5.3
        except:
            train_test_split = None
        batch_size = cfg.getBatchSize()

        
        try:
            test_dbs = cfg.getTestDbs() #Error with coronal/5.2
        except:
            test_dbs = None
        try:
            data_vars_transform = cfg.getDataVars('transform')
        except:
            data_vars_transform = None
        data = Data(get_data_mode=datamode, transform=data_vars_transform, get_train_mode=train_mode, 
                    get_train_dbs=train_dbs, get_batch_size_val=batch_size_val, gpu_use=gpu_use, get_mode=mode,
                    data_Vars=datapath, split=train_test_split, get_batch_size=batch_size, get_test_dbs=test_dbs,
                    random_seed=random_seed, log=None)
    return data

def build_loss(cfg, vis, log):
    r"""Function to build the loss class for the use cases.
            
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        vis : Visualization class
            The visualization utility class.

        log : Logging class
            The logging utility class.

        Returns
        ----------
        loss : Loss class
            Contains the loss class.
    """
    get_print = cfg.getPrintEvery()
    lossF = cfg.getLossF().split('+')
    gpu_use = cfg.getUseGPU()


    loss = Loss(vis, log, get_print_every=get_print, get_loss_split=lossF, gpu_use=gpu_use)

    return loss

def build_metrics(cfg, vis, log):
    r"""Function to build the Metrics class for the use cases.
                
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        vis : Visualization class
            The visualization utility class.

        log : Logging class
            The logging utility class.

        Returns
        ----------
        metrs : Metrics class
            Contains the metrics utility class.
    """
    get_metrics = cfg.getMetrics().split('+')
    gpu_use = cfg.getUseGPU()
    add_metrics = cfg.getAddMetrics()

    metrs = Metrics(vis, log, get_metrics_split=get_metrics, gpu_use=gpu_use, 
                    get_add_metrics=add_metrics)
    return metrs

def build_postprocess(cfg):
    r"""Function to build the Post Process class for the use cases.
                    
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        Returns
        ----------
        postp : Post Processing class
            Contains the functions for the different post processings
            based on the use case.
    """
    post_process = cfg.getPostProcess().lower()
    
    postp = PostProcessor(get_post_process=post_process)
    return postp
    

def build_model(cfg, writer=None, model_depth=None, t_number=None):
    r"""Function to build the Model class for the use cases.
                    
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        Returns
        ----------
        final_model : Model class
            
    """
    get_hpo_flag = cfg.getHPOflag()
    get_random_seed = cfg.getRandomSeed()
    get_model_type = cfg.getModelType().lower()
    get_exp_dir = cfg.getExpDir()
    get_exp_name = cfg.getExpName()
    get_check_save_all = cfg.getCheckSaveAll()
    get_check_resume = cfg.getCheckResume()
    get_pretrained = cfg.getPreTrained()
    
    if cfg.getUnsuperflag():
        model_centrods = cfg.getModelCentroids()
        model_attempts = cfg.getModelAttempts()
        max_iter= cfg.getMaxIter()
        epsilon = cfg.getAccuracy()
        
        final_model = Model_Unsuper(K=model_centrods, attempts=model_attempts, max_iter=max_iter,
                             epsilon=epsilon, get_model_type=get_model_type, expdir=get_exp_dir, expName=get_exp_name,
                            check_save_all=get_check_save_all, check_resume=get_check_resume, pretrained=get_pretrained,
                            random_seed=get_random_seed)
    else:
        get_model_input_nc = cfg.getModelInputNc()
        get_model_output_nc = cfg.getModelOutputNc()
        try:
            if get_hpo_flag:
                get_model_depth = model_depth
            else:
                get_model_depth = cfg.getModelDepth()
        except:
                get_model_depth = None

        get_pruning_flag = cfg.getPruningFlag()
        try:
            get_pruning_percent = cfg.getPruningPercent()
        except:
            get_pruning_percent = None
        get_use_gpu = cfg.getUseGPU()
        get_add_model = cfg.getAddModel()
        

        final_model = Model(writer, model_input=get_model_input_nc, model_output=get_model_output_nc,
                            model_depth=get_model_depth, pruning_flag=get_pruning_flag, pruning_percent=get_pruning_percent, 
                            gpu_use=get_use_gpu, expdir=get_exp_dir, expName=get_exp_name, model_type=get_model_type, 
                            check_resume=get_check_resume, pretrained=get_pretrained, check_save_all=get_check_save_all, 
                            add_model=get_add_model,random_seed=get_random_seed)
    return final_model

def build_optimizer(cfg, model, flag=True, lr=None, optimizer_name=None):
    r"""Function to build the optimizer needed for training
                    
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.
        
        model : Model Class

        flag : bool
            If you want to use optuna compatible optimizer.

        lr : float
            The learning rate of the training.

        optimizer_name : str
                    
        Returns
        ----------
        optimizer :  Optimizer PyTorch class
            
    """
    get_LR = cfg.getLR()
    get_weight_decay = cfg.getWeightDecay()
    get_optim = cfg.getOptim()
    get_momentum = cfg.getMomentum()
    get_betas = cfg.getBetas()
    get_epsilon = cfg.getEpsilon()
    get_decay_split = cfg.getDecay().split('-')
    get_gamma = cfg.getGamma()


    optimizer = make_optimizer(model=model, LR=get_LR, weight_decay=get_weight_decay, getoptim=get_optim, 
                                momentum=get_momentum, betas=get_betas, epsilon=get_epsilon, decay_split=get_decay_split, 
                                gamma=get_gamma, flag=flag, lr=lr, optimizer_name=optimizer_name)
    return optimizer

def build_callbacks(cfg):
    r"""Function to build the Callbacks class for the use cases.
                        
        Parameters
        ----------
        cfg : Config class
            Contains all the user options.

        Returns
        ----------
        callbacks : Callbacks class
            Callback utility class to add new features
            to the use cases with small functions.
    """
    #Default callbacks
    callbacks = []
    if cfg.getUnsuperflag():
        callbacks.append(UnsupervisedCheckpoint())
    #Preprocess callback is needed to run inference
    if cfg.getPreprocess():
        callbacks.append(PreProcessCallback())
    if cfg.getMode() in ['test', 'train']:
        #To save the best model
        metric_direction = cfg.getMetricDirection()
        callbacks.append(Checkpoint(get_metr_direction=metric_direction))
        #Add metrics
        callbacks.append(MetricsCallback(cfg))
        
    if cfg.getAddValImgs():
        #Visualize the Validation Images
        callbacks.append(ImgsCallback(cfg))

    if cfg.getSaveImgs():
        #Save the Validation Images 
        callbacks.append(SaveCallback(cfg))
        
    return callbacks
