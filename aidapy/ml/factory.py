"""Factory is responsible for building and controling the 
        functionalities of aidapy ML engine.

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""

from importlib import import_module

from aidapy.ml import postprocess
#Builders
from aidapy.ml.engine_unspr import Unsprvised
from aidapy.ml.engine import Engine
from aidapy.ml.hpo_optuna import HPO
from aidapy.ml.builders.builders import *

class Factory:
    r"""Factory class to construct the different supported
         ML engines (Supervised, Optuna, Unsupervised).
    
        Methods
        -------
        _set_model(self)
            Setter for setting the type of CNN model
                 (pruned or initial).
                 
        _default_engine(self, cfg)
            The building function of the Supervised ML engine.
            
        _optuna_engine(self, cfg)
            The building function of the Optuna ML engine.

        _create_engine(self)
            Helper function to select and create the ML engine.

        run(self)
           Running the ML engine.
    """
    def __init__(self, cfg):
        r"""Initiallization of the Factory Class. Constructs
                the engine.
                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        self.writer = None
        self.engine = None
        self.cfg = cfg
        self.prune = cfg.getPruningFlag()
        self.quant = cfg.getQuantizationFlag()
        self.hpo = cfg.getHPOflag()
        self.unsuper = cfg.getUnsuperflag() 
        self._create_engine()

    def _set_model(self): 
        r"""Method to select the CNN model.
        """
        if self.prune or self.quant:
            if self.prune:
                pruner = getattr(import_module('aidapy.ml.optimizations.pruner'), \
                                self.cfg.getPruner())(self.cfg, self.writer)
                final_model = pruner.get_model()
                final_model.save(1, is_best=True,
                              mname="model_pruned_")
            if self.quant:
                quantizer = getattr(import_module('aidapy.ml.optimizations.quantizer'), \
                                "Quantizer")(self.cfg, self.writer)
                final_model = quantizer.get_model()
                final_model.save(1, is_best=True,
                              mname="model_quantized_")
        else:
            model = build_model(self.cfg, self.writer)
            final_model = model
        self.engine.set_model(final_model)


    def _default_engine(self, cfg):
        r"""Building method of the ML Supervised Engine.
                        
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        #Build the Visualization module
        vis = build_visualizer(cfg)
        self.writer = vis
        #Build the Logger module
        log = build_logger(cfg)
        #Build the DataLoader module
        loader = build_dataloader(cfg, log)
        #Build the Loss module
        criterion = build_loss(cfg, vis, log)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        #Build the callbacks
        callbacks = build_callbacks(cfg)
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        #Build the Engine
        print("Engine constructor is running")
        self.engine = Engine(cfg, loader, criterion,
                             metrics, vis, log, callbacks, postprocess)
        ##Set the model of the engine.
        self._set_model()
            
    def _optuna_engine(self, cfg):
        r"""Building method of the Optuna Engine.
                                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        #Build the Visualization module
        vis = build_visualizer(cfg)
        self.writer = vis
        #Build the Logger module
        log = build_logger(cfg)
        #Build the DataLoader module
        loader = build_dataloader(cfg, log)
        #Build the Loss module
        criterion = build_loss(cfg, vis, log)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        #Build the callbacks
        callbacks = build_callbacks(cfg)
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        #Build the Engine
        self.engine = HPO(cfg, loader, criterion,
                          vis, metrics, callbacks, postprocess)
        ##Set the model of the engine.     
        #self._set_model()


    def _unspr_engine(self, cfg):
        r"""Building method of the Unsupervised Engine.
                                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
        """
        print("Engine constructor is running")
        #Build the Visualization module
        vis = build_visualizer(cfg)
        #Build the Logger module
        log = build_logger(cfg)
        #Build the Metrics module
        metrics = build_metrics(cfg, vis, log)
        #Build the DataLoader module
        loader = build_dataloader(cfg, log)
        #Build the postprocess
        postprocess = build_postprocess(cfg)
        #Set the model of the engine.     
        model = build_model(cfg)
        #Build the callbacks
        callbacks = build_callbacks(cfg)  
        
        #Build the Engine
        self.engine = Unsprvised(cfg, loader, model, postprocess, log, vis, metrics, callbacks)    


    def _create_engine(self):
        r"""Helper function to select and create the ML engine.
        """
        if self.unsuper:
            self._unspr_engine(self.cfg) 
        elif self.hpo:
            self._optuna_engine(self.cfg)
        else:
            self._default_engine(self.cfg)
        

    def run(self):
        r""" Method to run the engine.
        """
        self.engine.start()
