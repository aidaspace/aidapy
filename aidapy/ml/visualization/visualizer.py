""" Visualizer module responsible for tensorboard.
"""
import os.path as osp
from torch.utils.tensorboard import SummaryWriter
import torchvision


class Visualizer:
    """
        Visualizer handler for ML Use Cases
    """
    def __init__(self, experiment_dir=None,
                    experiment_name=None):
        """ Class to visualize the results of the use cases
             in tensorboard.
        """
        # Summary Writer to write model
        
        self.exp_path = osp.join(experiment_dir, experiment_name)
        self.writer = SummaryWriter(self.exp_path, flush_secs=1)

    def add_model(self, model, x):
        """
            Visualize model`s graph
        """
        self.writer.add_graph(model, x)

    def add_losses(self, name, loss, iteration):
        """
            Visualize loss evolution through training steps.
        """
        fname = osp.join('Train', name)
        self.writer.add_scalar(fname, loss, iteration)

    def add_metrics(self, name, score, iteration):
        """
            Visualize metrics evolution through training steps.
        """
        fname = osp.join('Test', name)
        self.writer.add_scalar(fname, score, iteration)

    def add_images(self, name, seq, epoch):
        """
            Visualize results (images) evolution through training steps.
        """
        fname = osp.join('Test_Databases', name)
        grid_img = torchvision.utils.make_grid(seq, nrow=6)
        self.writer.add_image(fname, grid_img, epoch)

    def close(self):
        """
            Close visualizer
        """
        self.writer.close()
