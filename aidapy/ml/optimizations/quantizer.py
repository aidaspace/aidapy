"""This script is responsible for quantizing a model
"""
import torch
import torch.nn as nn
import torch.quantization as quanti
from aidapy.ml.models import Model


class Quantizer(object):
    def __init__(self, cfg, writer, load_from=False, model=None):
        if model is None:
            temp_model =  build_model(cfg, writer)
            if load_from:
                load_path = torch.load(cfg.getPreTrained())
                temp_model.load_state_dict(load_path)
        else:
            temp_model = model
            del model
        
        quant_type = cfg.getQuantizationType()
        print(quant_type)
        
        if quant_type == "dynamic":
            quantized_model = quanti.quantize_dynamic(
                                    temp_model, 
                                    {nn.LSTM, nn.Linear},
                                    dtype=torch.qint8
                                )
        else:
            temp_model.qconfig = quanti.get_default_qconfig('fbgemm')
            temp_model = quanti.prepare(temp_model)

            # Convert the observed model to a quantized model. This does several things:
            # quantizes the weights, computes and stores the scale and bias value to be
            # used with each activation tensor, and replaces key operators with quantized
            # implementations.
            quantized_model = quanti.convert(temp_model)
                        
        ## Store model for later pull
        self.model = quantized_model
        
        
    def get_model(self):
        return self.model        