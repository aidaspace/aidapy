"""This script is responsible for pruning a model
"""
import torch
import torch.nn as nn
from aidapy.ml.utils.prune import *
from aidapy.ml.builders import build_model

class Pruner(object):
    def __init__(self):
        """Iniiallizer of Pruner.
        """
        super.__init(Pruner)

    def get_model(self):
        self.model.train()
        return self.model



class CustomPruner(Pruner):
    def __init__(self, cfg, writer, load_from=False, model=None):
        """Iniiallizer of Pruner.
        """
        #TO_DO:Do it more generic
        #Load in the memory the original model
        #with the pretrained weights.
        if model is None:
            temp_model =  build_model(cfg, writer)
            if load_from:
                load_path = torch.load(cfg.getPreTrained())
                temp_model.load_state_dict(load_path)
        else:
            temp_model = model
            del model
        
        ## Take Pruning Parameters
        prune_perc_conv = cfg.getPruningPercent()
        prune_mode = cfg.getPruningMode()

        # ## Profile Model before pruning
        # model_info(temp_model, img_size=)
        
        ## PRUNER INITIALIZED AND EXECUTION
        pr_model = PruneModel(temp_model, prune_perc_conv, prune_perc_conv,
                              prune_mode)
        
        ## Delete pruned weights permanently
        pr_model = MakePrunePermanent(pr_model)
        new_weights, _, layer_names = prune_convolutional_layers(
            pr_model, prune_perc_conv, prune_mode)

        ## Pruner call
        self.model = build_model(cfg, writer)
        # self.model = self.update_conv_weights(self.model, \
        #                                       layer_names, new_weights)
        self.model = self.update_layers(self.model, \
                                        layer_names, new_weights)
        t=1
              
              
    def update_conv_weights(self, model, names, new_weights):
        for name, module in model.named_modules():
            if isinstance(module, torch.nn.Conv2d) and name in names:
                rt = names.index(name)
                module.weights = new_weights[rt]
        return model
    
    
    def update_layers(self, model, layer_names, new_weights):

        for mod in model.children():
            for lname, layer in mod.named_modules():
                if isinstance(layer, nn.Sequential):
                    
                    for sublname, sublayer in layer.named_modules():
                        try:
                            c1 = sublayer.weight
                        except:
                            continue
                        if "." in sublname and len(sublname.split("."))>=2: continue
                        if isinstance(sublayer, nn.Conv2d):
                            matchname = [x for x in layer_names if sublname in x][0]
                            idx = layer_names.index(matchname)
                            new_wn = new_weights[idx]
                            new_w = torch.from_numpy(new_wn).float()
                            replacement_layer = nn.Conv2d(new_w.shape[1], new_w.shape[0], kernel_size=sublayer.kernel_size, stride=sublayer.stride)
                            setattr(layer, sublname, replacement_layer)
                        elif isinstance(sublayer, nn.Linear):
                            continue
                        else:
                            continue
                # else:
                #     for i, ley in layer.named_modules():
                #         try:
                #             c1 = ley.weight
                #         except:
                #             print("Layer has no weights")
                #             continue
                #         print(i, "     ", ley)
                #         matchname = [x for x in layer_names if i in x][0]
                #         idx = layer_names.index(matchname)
                #         new_wn = new_weights[idx]
                #         new_w = torch.from_numpy(new_wn).float()
                #         if isinstance(ley, nn.Conv2d):
                #             ley1 = nn.Conv2d(new_w.shape[1], new_w.shape[0], kernel_size=ley.kernel_size, stride=ley.stride)
                #             set_trace()
                #             setattr(ley, i, ley1)
                #         elif isinstance(ley, nn.Linear):
                #             continue
                #         else:
                #             continue
                #         # ley.weight = nn.Parameter(new_w)
                #         print("New layer", i, "     ", ley)
                #         set_trace()
                else:
                    try:
                        c1 = layer.weight
                    except:
                        print("Layer has no weights")
                        continue
                    if "." in lname and len(lname.split("."))>=2: continue
                    if isinstance(layer, nn.Conv2d):
                        matchname = [x for x in layer_names if lname in x][0]
                        idx = layer_names.index(matchname)
                        new_wn = new_weights[idx]
                        new_w = torch.from_numpy(new_wn).float()
                        ley1 = nn.Conv2d(new_w.shape[1], new_w.shape[0], kernel_size=layer.kernel_size, stride=layer.stride)
                        # set_trace()
                        setattr(mod, lname, ley1)
                    elif isinstance(layer, nn.Linear):
                        continue
                    else:
                        continue
        print(model)
        return model




class NativePruner(Pruner):
    def __init__(self, cfg, writer, load_from=False, model=None):
        """Iniiallizer of Pruner.
        """
        #TO_DO:Do it more generic
        #Load in the memory the original model
        #with the pretrained weights.
        if model is None:
            temp_model =  build_model(cfg, writer)
            if load_from:
                load_path = torch.load(cfg.getPreTrained())
                temp_model.load_state_dict(load_path)
        else:
            temp_model = model
            del model

        ## Set model in eval mode
        temp_model.eval()
        with torch.no_grad():
            print("Sparsity before pruning:")
            print_model_sparsity(temp_model)
            
            ## Get pruning configurations
            prune_perc_conv = cfg.getPruningPercent()
            prune_mode = cfg.getPruningMode()

            ## PRUNER INITIALIZED AND EXECUTION
            pr_model = PruneModel(temp_model, prune_perc_conv, prune_perc_conv,
                                prune_mode)    
            pr_model = MakePrunePermanent(pr_model)
            
            ## Report pruning results
            print("Sparsity after pruning:")
            print_model_sparsity(pr_model)
            
        self.model = pr_model
            