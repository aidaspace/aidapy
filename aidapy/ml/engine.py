""" Supervised ML engine of the AIDApy Interface.

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""

import torch
import random
from tqdm import tqdm

#AIDApy ML engine deps
from aidapy.ml.optim import make_optimizer
from aidapy.ml.callback_hook import EngineCallbackHookMixin
from aidapy.ml.builders import build_optimizer

class Engine(EngineCallbackHookMixin):
    r"""A generic Supervised ML engine class for running 
        different use cases. It can handles both training 
        and evaluation processes.
    """
    def __init__(self, cfg, loader, loss, metrics, writer,
                 logger, callbacks, postprocess):
        r"""Initiallization of the Engine Class. Constructs
                the engine from the given components.
                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.

            loader : Dataloader class
                The dataset of the use case.

            loss : Loss class
                The selected loss for training the model.

            metrics : Metrics class
                The selected metrics for evaluating the model.

            writer : Visualization class
                For visualizing the training/eval procedure.

            logger : Logger class
                For logging the training/eval procedure.

            callbacks : Callbacks class
                For running various features.

            postprocess : postprocess class
                For running the postprocess function.
        """
        self.cfg = cfg
        self.device = torch.device('cpu' if not self.cfg.getUseGPU()
                                   else 'cuda')
        # dataloaders
        self.loader = loader
        self.loader_train = loader.loader_train
        self.loader_test = loader.loader_test
        # loss
        self.loss = loss
        # metric
        self.metric = metrics
        # visualizer
        self.writer = writer
        # logger
        self.logger = logger
        # callbacks
        self.callbacks = callbacks
        # postprocess
        self.post_process = postprocess
        # other params
        self.train_iter = 0
        # random seed
        r_seed = cfg.getRandomSeed()
        random.seed(r_seed)
        torch.manual_seed(r_seed)
        torch.backends.cudnn.deterministic = True
                                     
    def set_model(self, model):
        r"""Set the CNN model for training.
                        
            Parameters
            ----------
            model :  model class
                The CNN model class.
        """
        self.model = model.to(self.device)
        self._create_optimizer()


    def _create_optimizer(self):
        r"""Creates the optimizer for the training.
        """
        self.optimizer = build_optimizer(self.cfg, self.model)
        

    def train(self):
        r""" Handles training process (model feedforwarding,
             loss computation and backpropagation)
        """
        self.loss.step()
        self.on_train_init_epoch()
        epoch = self.optimizer.get_last_epoch() + 1
        self.model.train()
        print("##"*30)
        print("Epoch: {}".format(epoch))
        for batch, (self.input, self.target, _) in \
                                 enumerate(self.loader_train):

            self.on_train_init_batch()
            self.optimizer.zero_grad()
            output = self.model(self.input)
            output = self.post_process(output, train=True)
            loss = self.loss(output, self.target,
                             self.train_iter)
            loss.backward()
            self.optimizer.step()
            self.train_iter += 1
            self.on_train_end_batch()
        self.on_train_end_epoch()
        self.optimizer.schedule()


    def test(self):
        r"""Handles evaluation process (metrics calculation, visualizations and 
           keeping checkpoints).
        """
        self.dataset_mode = self.cfg.getMode()
        torch.set_grad_enabled(False)
        self.model.eval()
        self.on_validation_init_epoch()
        for _, d in enumerate(self.loader_test):
            self.total_d = len(d)
            self.dataset_name = d.dataset.name
            self.on_validation_init_dataloader()
            for _, (self.input, self.target, self.denorm_input) in \
                                             enumerate(tqdm(d, ncols=80)):
                self.on_validation_init_batch()
                output = self.model(self.input)
                # post process function to use 
                self.output, self.out_vis = self.post_process(output, 
                                                            train=False)
                self.on_validation_end_batch()
            self.on_validation_end_dataloader() 
        self.on_validation_end_epoch()                             
        torch.set_grad_enabled(True)

    
    def terminate(self):
        r"""Function that manages the train/evaluation process.

            Returns
            -------
            boolean
              For the engine to terminate     
        """
        if self.cfg.getMode() in ['test', 'demo']:
            self.test()
            print(10 * '-', ' Finish ', 10 * '-')
            return True
        else:
            epoch = self.optimizer.get_last_epoch() + 1
            if epoch >= self.cfg.getEpochs():
                print(10 * '-', ' Finish ', 10 * '-')
                self.writer.close()
                return True
            else:
                return False


    def start(self):
        r"""Function that starts the train/evaluation process.
        """
        self.on_init_epoch()
        while not self.terminate():
            self.train()
            self.test()
        self.on_end_epoch()
