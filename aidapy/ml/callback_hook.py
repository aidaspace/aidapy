""" High level wrapper of training a CNN model with callbacks
"""

from abc import ABC


class EngineCallbackHookMixin(ABC):
    def on_init_epoch(self):
        for callback in self.callbacks:
            callback.on_init_epoch(self)
    
    def on_end_epoch(self):
        for callback in self.callbacks:
            callback.on_end_epoch(self)

    def on_validation_init_epoch(self):
        for callback in self.callbacks:
            callback.on_validation_init_epoch(self)

    def on_validation_end_epoch(self):
        for callback in self.callbacks:
            callback.on_validation_end_epoch(self)
        
    def on_validation_init_batch(self):
        for callback in self.callbacks:
            callback.on_validation_init_batch(self)

    def on_validation_end_batch(self):
        for callback in self.callbacks:
            callback.on_validation_end_batch(self)

    def on_validation_init_dataloader(self):
        for callback in self.callbacks:
            callback.on_validation_init_dataloader(self)

    def on_validation_end_dataloader(self):
        for callback in self.callbacks:
            callback.on_validation_end_dataloader(self)

    def on_train_init_batch(self):
        for callback in self.callbacks:
            callback.on_train_init_batch(self)
    
    def on_train_end_batch(self):
        for callback in self.callbacks:
            callback.on_train_end_batch(self)

    def on_train_init_epoch(self):
        for callback in self.callbacks:
            callback.on_train_init_epoch(self)
        
    def on_train_end_epoch(self):
        for callback in self.callbacks:
            callback.on_train_end_epoch(self)

    
