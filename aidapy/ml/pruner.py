"""This script is responsible for pruning a model
"""
import torch
from aidapy.ml.utils.prune import *
from aidapy.ml.builders import build_model


class Pruner(object):
    def __init__(self):
        """Iniiallizer of Pruner.
        """
        super.__init(Pruner)

    def get_model(self):
        self.model.train()
        return self.model


class CustomPruner(Pruner):
    def __init__(self, cfg, writer, load_from=False, model=None):
        """Iniiallizer of Pruner.
        """
        #TO_DO:Do it more generic
        #Load in the memory the original model
        #with the pretrained weights.
        if model is None:
            temp_model =  build_model(cfg, writer)
            if load_from:
                load_path = torch.load(cfg.getPreTrained())
                temp_model.load_state_dict(load_path)
        else:
            temp_model = model
            del model
        
        ## Take Pruning Parameters
        prune_perc_conv = cfg.getPruningPercent()
        prune_mode = cfg.getPruningMode()

        ## PRUNER INITIALIZED AND EXECUTION
        pr_model = PruneModel(temp_model, prune_perc_conv, prune_perc_conv,
                              prune_mode)
            
        pr_model = MakePrunePermanent(pr_model)
        new_weights, _, layer_names = prune_convolutional_layers(
            pr_model, prune_perc_conv, prune_mode)

        ## Pruner call
        self.model = build_model(cfg, writer)
        self.model = self.update_conv_weights(self.model, \
                                              layer_names, new_weights)
        
                     
    @staticmethod
    def update_conv_weights(model, names, new_weights):
        for name, module in model.named_modules():
            if isinstance(module, torch.nn.Conv2d) and name in names:
                rt = names.index(name)
                module.weights = new_weights[rt]
        return model



class NativePruner(Pruner):
    def __init__(self, cfg, writer, load_from=False, model=None):
        """Iniiallizer of Pruner.
        """
        #TO_DO:Do it more generic
        #Load in the memory the original model
        #with the pretrained weights.
        if model is None:
            temp_model =  build_model(cfg, writer)
            if load_from:
                load_path = torch.load(cfg.getPreTrained())
                temp_model.load_state_dict(load_path)
        else:
            temp_model = model
            del model

        ## Set model in eval mode
        temp_model.eval()
        with torch.no_grad():
            print("Sparsity before pruning:")
            print_model_sparsity(temp_model)
            
            ## Get pruning configurations
            prune_perc_conv = cfg.getPruningPercent()
            prune_mode = cfg.getPruningMode()

            ## PRUNER INITIALIZED AND EXECUTION
            pr_model = PruneModel(temp_model, prune_perc_conv, prune_perc_conv,
                                prune_mode)    
            pr_model = MakePrunePermanent(pr_model)
            
            ## Report pruning results
            print("Sparsity after pruning:")
            print_model_sparsity(pr_model)
            
        self.model = pr_model
            