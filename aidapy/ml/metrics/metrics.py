import torch
import torch.nn as nn
#Debugger
import pdb
from importlib import import_module

class Metrics:
    def __init__(self, vis=None, log=None, get_metrics_split=None, gpu_use=None, get_add_metrics=None):
        r"""The high-level wrapper class of the Metrics module.

            Parameters
            ----------
            cfg : Config class
                Contains all the user options.
    
            vis : Visualization class
                The visualization utility class.
    
            log : Logging class
                The logging utility class.
        """
        print('Preparing Metrics function(s)')
        self.metrics = []
        self.metrics_module = nn.ModuleList()
        self.ave_scores = dict()
        for metric in get_metrics_split:
            metric_type = metric
            self.ave_scores[metric_type] = 0
            m = import_module('aidapy.ml.metrics.metric_func')
            try:             
                metric_function = getattr(m, metric_type)()
            except:
                raise Exception(
                    "This metric {} is not supported".format(metric_type))
            
            self.metrics.append({
                'type': metric_type,
                'function': metric_function})
        if vis is not None:  
            self.writer = vis
        else:
            self.writer = False
        if log is not None: 
            self.logger = log
        else:
            self.logger = False
        self.nsamples = 0
        self.optscore = 0
        self.get_add_metrics=get_add_metrics

        for l in self.metrics:
            if l['function'] is not None:
                self.metrics_module.append(l['function'])

        device = torch.device('cpu' if not gpu_use else 'cuda')
        self.metrics_module.to(device)

        
    def forward(self, x, y):
        r"""Forward function of the loss wrapper
            
            Parameters
            ----------
            x : tensor
                The input tensor.

            y : tensor
                The ground truth tensor
        """
        for _, l in enumerate(self.metrics):
            if l['function'] is not None:
                effective_metric = l['function'](x, y)
                self.calc_ave_score(effective_metric, l['type'])


    def number_of_samples(self, nsamples):
        r"""Sets the number of samples

            Parameters
            ----------
            nsamples : int
                The number of the samples.
        """
        self.nsamples = nsamples


    def calc_ave_score(self, score, mtype):
        r"""Calculates the average score of each metric

            Parameters
            ----------
            score : tensor
                The input tensor.

            mtype : str
                The name of the metric
            
        """
        if isinstance(score, (list, type(torch.tensor(0)))) and score.numel() > 1:
            ave = sum(score)/len(score)
            self.ave_scores[mtype] += \
                            ave / self.nsamples
            for i in range(0, len(score)):
                try:
                    self.ave_scores[mtype + '_cls_' + str(i)] += \
                                         score[i] / self.nsamples
                except:
                    self.ave_scores[mtype + '_cls_' + str(i)] = \
                                 score[i] / self.nsamples
        else:
            self.ave_scores[mtype] += score / self.nsamples


    def get_ave_score(self, epoch):
        r"""Getter of the average score of each metric
        
            Parameters
            ----------
            epoch : int
                The current epoch

            Returns
            -------
            float
                The value of the metric
        """
        imp_metric = []
        for k, v in self.ave_scores.items():
            if self.get_add_metrics:
                self.display_metric(k, v.item(), epoch)

            self.log_metric(k, v.item(), epoch)
                
            if len(imp_metric) < 1:
                imp_metric.append(v.item())

        self.optscore = imp_metric[0]
        self.clean_score()
        return imp_metric[0]

    def optuna_avescore(self):
        r"""Getter of the average score of each metric
            Slighty modified for optuna compatibility.
            
            Returns
            -------
            float
                The value of the metric
        """
        return self.optscore

    def clean_score(self):
        r"""Zero the score for the next Eval
        """
        for k, _ in self.ave_scores.items():
            self.ave_scores[k] = 0.0

    def display_metric(self, name, effective_metric, epoch):
        r"""Function to send the metric in the tensorboard.

            Parameters
            ----------
            name : str

            effective_metric : float

            epoch : int
        """
        if self.writer:  
            self.writer.add_metrics(name, effective_metric, epoch)

    def log_metric(self, name, metric, epoch):
        r"""Function to log the metric.
        
            Parameters
            ----------
            name : str

            metric : float

            epoch : int
        """
        if self.logger: 
            self.logger.write('Score of {} in epoch {} \
                          is {:.4f}'.format(name, epoch, metric))

    def get_metric_module(self):
        r"""Returns the metric module
        """
        return self.metrics_module
        
