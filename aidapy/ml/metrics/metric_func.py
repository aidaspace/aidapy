import torch
import torch.nn as nn


class IOU_scoring(nn.Module):
    """ Class for IoU metrics score """
    def __init__(self, weight=None, size_average=True):
        super(IOU_scoring, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        """ calculation of metric """
        #comment out if your model contains a sigmoid or equivalent activation layer
        #inputs = torch.sigmoid(inputs)
        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        #intersection is equivalent to True Positive count
        #union is the mutually inclusive area of all labels & predictions
        intersection = (inputs * targets).sum()
        total = (inputs + targets).sum()
        union = total - intersection
        IoU = (intersection + smooth)/(union + smooth)
        return IoU


class DiceCoeff(nn.Module):
    """ Class for DICE metrics score """
    def __init__(self, weight=None, size_average=True):
        super(DiceCoeff, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        """ calculation of metric """
        #comment out if your model contains a sigmoid or equivalent activation layer
        #inputs = torch.sigmoid(inputs)

        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        intersection = (inputs * targets).sum()
        dice = (2.*intersection + smooth)\
            / (inputs.sum() + targets.sum() + smooth)
        return dice

class MSE(nn.Module):
    """ Class for MSE metrics score """
    def __init__(self):
        super().__init__()
        self.mse = nn.MSELoss()

    def forward(self, inputs, targets):
        """ calculation of metric """
        return self.mse(inputs, targets)

class RMSE(nn.Module):
    """ Class for RMSE metrics score """
    def __init__(self):
        super().__init__()
        self.mse = nn.MSELoss()

    def forward(self, inputs, targets):
        """ calculation of metric """
        return torch.sqrt(self.mse(inputs, targets))

class RSquared(nn.Module):
    """ Class for RSquared metrics score """
    def __init__(self):
        super().__init__()
                                    
    def forward(self, inputs, targets):
        """
        Performance test using the R2 indicator
        """
        stot = torch.sum(torch.square(targets - torch.mean(targets)))
        sres = torch.sum(torch.square(targets - inputs))
        return 1.0 - sres / (stot+1e-6)


class CC(nn.Module):
    """ Class for CC metrics score """
    def __init__(self):
        super().__init__()
                                    
    def forward(self, x, y):
        """
        Performance test using the R2 indicator
        """
        #pdb.set_trace()       
        vx = x - torch.mean(x)
        vy = y - torch.mean(y)
        
        cost = torch.sum(vx * vy) /(
                torch.sqrt(torch.sum(vx ** 2)) * torch.sqrt(torch.sum(vy ** 2)) + 1e-6)
        return cost
        