from aidapy.ml import callbacks
from aidapy.ml.postprocess import *
from aidapy.ml.callback_hook import EngineCallbackHookMixin

""" Unsupervised ML engine of the AIDApy Interface.

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""

#AIDApy Unsupervised engine deps

class Unsprvised(EngineCallbackHookMixin):
    r"""A generic Unsupervised ML engine class for running 
        different use cases. 
    """
    def __init__(self, cfg, loader, final_model, postprocess, log, vis, metrics, callbacks):
        r"""Initiallization of the Engine Class. Constructs
                the engine from the given components.
                
            Parameters
            ----------
            cfg : Config class
                Contains all the user options.

            loader : Dataloader class
                The dataset of the use case.

            final_model : Model class
                The model used for unsupervised learning
            
            logger : Logger class
                For logging the training/eval procedure.
            
            callbacks : Callbacks class
                For running various features.

            postprocess : postprocess class
                For running the postprocess function.
        """
        self.cfg = cfg
        # dataloaders
        self.loader = loader
        self.loader_train = loader.loader_train
        # model
        self.model=final_model
        # logger
        self.logger = log
        # postprocess
        self.postprocess = postprocess
        # visualizer
        self.vis = vis
        # metrics
        self.metric = metrics ####
        # callbacks
        self.callbacks = callbacks
        # other params
        self.train_iter = 0
        self.total_d = cfg.getDataRandom()

    def test(self):
        r""" Handles training process (model feedforwarding and visualizing results)
        """
        
        print("\n")
        print("####"*10)
        
        self.on_validation_init_dataloader()
        for i, (self.vectorized, self.denorm) in enumerate(self.loader_train):

            self.on_validation_init_batch()
            #Fit K-Means model
            output = self.model.forward(self.vectorized)
            #Postprocess
            self.output = self.postprocess(output, self.loader_train, self.denorm)
            #Visualize
            self.on_validation_end_batch()
        
        self.on_validation_end_dataloader()



    def start(self):
        r"""Function that starts the train/evaluation process.
        """
        self.test()
            



