""" Optuna ML engine of the AIDApy Interface.

authors : Katakis Sofoklis, Leonidas Liakopoulos
emails : katakis@iridalabs.gr, liakopoulos@iridalabs.gr
"""
import os.path as osp
from aidapy.ml.builders.builders import build_dataloader
import torch
import optuna
from optuna.samplers import RandomSampler
import joblib
import random
import os
#AIDApy ML engine deps
from aidapy.ml.optim import make_optimizer
from aidapy.ml.engine import Engine
from aidapy.ml.builders import build_optimizer
from aidapy.ml.builders import build_model

class HPO(Engine):
    r"""Optuna ML engine class for running HPO
        in different use cases. It can handles both training 
        and evaluation processes.  It inherits multiple functionalities
        for the Supervised ML engine.
    """
    def __init__(self, cfg, loader, loss, vis, metrics, 
                                    callbacks, postprocess):
        r"""Initiallization of the HPO Class. Constructs
            the engine from the given components.

            Parameters
            ----------
            cfg : Config class
                Contains all the user options.

            loader : Dataloader class
                The dataset of the use case.

            loss : Loss class
                The selected loss for training the model.

            metrics : Metrics class
                The selected metrics for evaluating the model.

            vis : Visualization class
                For visualizing the training/eval procedure.

            callbacks : Callbacks class
                For running various features.

            postprocess : postprocess class
                For running the postprocess function.
        """
        self.cfg = cfg
        self.device = torch.device('cpu' if not self.cfg.getUseGPU()
                                                else 'cuda')
        # dataloaders
        self.loader_train = loader.loader_train
        self.loader_test = loader.loader_test
        # loss
        self.loss = loss
        # metric
        self.metric = metrics
        # callbacks
        self.callbacks = callbacks
        # visualizer
        self.writer = vis
        # postprocess
        self.post_process = postprocess
        # other params
        self.train_iter = 0
        # seed 
        r_seed = cfg.getRandomSeed()
        random.seed(r_seed)
        torch.manual_seed(r_seed)
        torch.backends.cudnn.deterministic = True
        sampler = RandomSampler(r_seed)
        
        # Optuna arguments                
        self.direction = self.cfg.getHPOdirection()
        self.trials = self.cfg.getHPOntrials()
        if self.cfg.getHPOtimeout()=='None': 
            self.timeout = None
        else:
            self.timeout = self.cfg.getHPOtimeout() 
        self.study = optuna.create_study(direction=self.direction, sampler=sampler)
        self.optimizer = None
        self.optimizers = []

    def _create_learning_rates(self):
        r""" The examined for HPO LR range
        """
        self.lrs = [float(lr) for lr in 
                                self.cfg.getHPOlrs()]

    def _create_optimizer(self):
        r""" The examined for HPO optimizer methods
        """
        self.optimizers = [optim for optim in 
                                self.cfg.getHPOoptims()]

    
    def _create_architecture_search(self):
        r""" The examined for HPO architecture methods
        """
        self.model_depth = self.cfg.getModelDepth()
        
        
    def objective(self, trial):
        r"""Method that is required for the Optuna HPO
            to run. It provides the objective of each trial.

            Parameters
            ----------
            trial : Optuna trial class
               Defines the parameters of each HPO iteration.

            Returns
            ----------
            ave_score : int
                The average metric score of the evaluation test set
                at the end of the iteration.
        """
        #Learning Rate HPO
        self._create_learning_rates()
        lr = trial.suggest_float("lr", self.lrs[0], self.lrs[1], log=True)

        # Architecture HPO
        self._create_architecture_search()
        if type(self.model_depth[0])==list:
            model_depth = trial.suggest_categorical("depth", self.model_depth)
            
        else:
            model_depth = trial.suggest_int("depth", self.model_depth[0], self.model_depth[-1])

        # Build model 
        self.model = build_model(self.cfg, self.writer, model_depth, trial.number)
        self.set_model(self.model)
        
        # Optimizer HPO
        optim_name = trial.suggest_categorical("optimizer", 
                                    self.optimizers)
        
        #Call build optimizer at each iteration
        self.optimizer = build_optimizer(self.cfg, self.model, flag=False, lr=lr, optimizer_name=optim_name)
        
        ave_score = 0
        self.on_init_epoch()
        while not self.terminate():
            self.train()
            self.test()
            epoch = self.optimizer.get_last_epoch()
            ave_score = self.metric.optuna_avescore()
            print("Epoch out is: ", epoch)
            print("Avg Score is: ", ave_score)
            trial.report(ave_score, epoch)
            # Handle pruning based on the intermediate value.
            if trial.should_prune():
                raise optuna.exceptions.TrialPruned()
        self.on_end_epoch()
        
        return ave_score

   
    def start(self):
        r"""Method that starts the Optuna engine.
        """
        self.study.optimize(self.objective, n_trials=self.trials,
                                         timeout=self.timeout)      
        pruned_trials = [t for t in self.study.trials 
                    if t.state == optuna.trial.TrialState.PRUNED]
        complete_trials = [t for t in self.study.trials
                     if t.state == optuna.trial.TrialState.COMPLETE]
        print(" Study statistics: ")
        print(" Number of finished trials: ", len(self.study.trials))
        print(" Number of pruned trials: ", len(pruned_trials))
        print(" Number of complete trials: ", len(complete_trials))
        print(" Best trial:")
        trial = self.study.best_trial
        print(" Value: ", trial.value)
        print(" Params: ")
        for key, value in trial.params.items():
            print("    {}: {}".format(key, value))
        #Create the appropriate dirs
        exp_dir = self.cfg.getExpDir()
        exp_folder = self.cfg.getExpName()
        total_dir = osp.join(exp_dir, exp_folder)
        #Save the optuna study in experiment directory
        joblib.dump(self.study,
               osp.join(total_dir, "optuna_study.pkl"))
        #Visualize the results
        fig = optuna.visualization.plot_optimization_history(self.study)
        fig.show()
            
    def _trial_accuracy(self):
        r"""Method that report the trial accuracy.
        """
        trial.report(accuracy, epoch)        
        # Handle pruning based on the intermediate value.
        if trial.should_prune():
            raise optuna.exceptions.TrialPruned()
