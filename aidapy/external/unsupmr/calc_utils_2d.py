"""
Some useful functions to compute quantities

M. Sisti, S. Fadanelli - 18/11/2020
F. Finelli - 27/03/2021
e-mail: francesco.finelli@phd.uni_pi.it
"""
import numpy as np
from numba import njit


@njit("float64[:,:](float64[:,:,:])")
def norm_xyz(A):
    """
    Norm of a vector field, using (first) 3 components.
    @njit gives a x5 speed-up

    INPUTS:
    A -> numpy.ndaray of shape (n,nx,ny) with n >= 3, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    return np.sqrt(A[0] ** 2 + A[1] ** 2 + A[2] ** 2)


@njit("float64[:,:](float64[:,:,:])")
def norm_xy(A):
    """
    Norm of a vector field, using (first) 2 components.
    @njit gives a x5 speed-up

    INPUTS:
    A -> numpy.ndaray of shape (n,nx,ny) with n >= 2, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    return np.sqrt(A[0] ** 2 + A[1] ** 2)


@njit("float64[:,:](float64[:,:,:],float64[:,:,:])")
def scalar_xyz(A, B):
    """
    Scalar product of a two vector fields, using (first) 3 components.
    @njit gives a x1.5 speed-up

    INPUTS:
    A,B -> numpy.ndaray of shape (n,nx,ny) with n >= 3, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    return A[0] * B[0] + A[1] * B[1] + A[2] * B[2]


@njit("float64[:,:](float64[:,:,:],float64[:,:,:])")
def scalar_xy(A, B):
    """
    Scalar product of a two vector fields, using (first) 2 components.
    @njit gives a x1.5 speed-up

    INPUTS:
    A,B -> numpy.ndaray of shape (n,nx,ny) with n >= 2, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    return A[0] * B[0] + A[1] * B[1]


@njit("float64[:,:,:](float64[:,:,:],float64[:,:,:])")
def cross_xyz(A, B):
    """
    Cross product of a two vector fields, using (first) 3 components.
    @njit gives a x2 speed-up

    INPUTS:
    A,B -> numpy.ndaray of shape (n,nx,ny) with n >= 3, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    C = np.empty(A.shape, dtype=np.float64)
    C[0] = A[1] * B[2] - A[2] * B[1]
    C[1] = -A[0] * B[2] + A[2] * B[0]
    C[2] = A[0] * B[1] - A[1] * B[0]
    return C


@njit("float64[:,:](float64[:,:,:],float64[:,:,:])")
def cross_xy(A, B):
    """
    Cross product of a two vector fields, using (first) 2 components.
    @njit gives a x2 speed-up

    INPUTS:
    A,B -> numpy.ndaray of shape (n,nx,ny) with n >= 2, data type float64

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    return A[0] * B[1] - A[1] * B[0]


def deriv(B, dx, axis, accuracy_order=6, periodic=True):
    """
    Finite difference derivative along x or y for a scalar field.
    Works the best with C-ordred array, and njit in this case make things worse

    INPUTS:
    B              -> numpy.ndaray of shape (nx,ny)
    dx             -> float, grid spacing in the differentiation direction
    axis           -> int, 0 or 1, the differentiation direction (0 -> x; 1 -> y)
    accuracy_order -> int, 2 or 4 or 6, order of accuracy, default is 6
    periodic       -> bool, if the field is periodic in the given direction,
                      default is True

    OUTPUTS:
    numpy.ndaray of shape (nx,ny), type float64
    """
    invdx = 1.0 / dx
    if axis == 0:
        A = B
    elif axis == 1:
        A = B.T
    else:
        print("\nERROR in deriv -> axis is not 0 or 1, returning random array")
        return np.empty((1, 1))
    dAdx = np.empty(A.shape, dtype=np.float64)
    if accuracy_order == 2:
        c_1 = 1.0 / 2.0 * invdx
        if periodic:
            dAdx[0, :] = c_1 * (A[1, :] - A[-1, :])
            dAdx[1:-1, :] = c_1 * (A[2:, :] - A[:-2, :])
            dAdx[-1, :] = c_1 * (A[0, :] - A[-2, :])
        else:
            f_0 = -3.0 / 2.0 * invdx
            f_1 = 2.0 * invdx
            f_2 = -1.0 / 2.0 * invdx
            dAdx[0, :] = f_0 * A[0, :] + f_1 * A[1, :] + f_2 * A[2, :]
            dAdx[1:-1, :] = c_1 * (A[2:, :] - A[:-2, :])
            dAdx[-1, :] = -f_0 * A[-1, :] - f_1 * A[-2, :] - f_2 * A[-3, :]
    elif accuracy_order == 4:
        c_1 = 2.0 / 3.0 * invdx
        c_2 = -1.0 / 12.0 * invdx
        if periodic:
            dAdx[0, :] = c_1 * (A[1, :] - A[-1, :]) + c_2 * (A[2, :] - A[-2, :])
            dAdx[1, :] = c_1 * (A[2, :] - A[0, :]) + c_2 * (A[3, :] - A[-1, :])
            dAdx[2:-2, :] = c_1 * (A[3:-1, :] - A[1:-3, :]) + c_2 * (A[4:, :] - A[:-4, :])
            dAdx[-2, :] = c_1 * (A[-1, :] - A[-3, :]) + c_2 * (A[0, :] - A[-4, :])
            dAdx[-1, :] = c_1 * (A[0, :] - A[-2, :]) + c_2 * (A[1, :] - A[-3, :])
        else:
            f_0 = -25.0 / 12.0 * invdx
            f_1 = 4.0 * invdx
            f_2 = -3.0 * invdx
            f_3 = 4.0 / 3.0 * invdx
            f_4 = -1.0 / 4.0 * invdx
            dAdx[:2, :] = (
                f_0 * A[:2, :]
                + f_1 * A[1:3, :]
                + f_2 * A[2:4, :]
                + f_3 * A[3:5, :]
                + f_4 * A[4:6, :]
            )
            dAdx[2:-2, :] = c_1 * (A[3:-1, :] - A[1:-3, :]) + c_2 * (A[4:, :] - A[:-4, :])
            dAdx[-2:, :] = (
                -f_0 * A[-2:, :]
                - f_1 * A[-3:-1, :]
                - f_2 * A[-4:-2, :]
                - f_3 * A[-5:-3, :]
                - f_4 * A[-6:-4, :]
            )
    elif accuracy_order == 6:
        c_1 = 3.0 / 4.0 * invdx
        c_2 = -3.0 / 20.0 * invdx
        c_3 = 1.0 / 60.0 * invdx
        if periodic:
            dAdx[0, :] = (
                c_1 * (A[1, :] - A[-1, :])
                + c_2 * (A[2, :] - A[-2, :])
                + c_3 * (A[3, :] - A[-3, :])
            )
            dAdx[1, :] = (
                c_1 * (A[2, :] - A[0, :])
                + c_2 * (A[3, :] - A[-1, :])
                + c_3 * (A[4, :] - A[-2, :])
            )
            dAdx[2, :] = (
                c_1 * (A[3, :] - A[1, :])
                + c_2 * (A[4, :] - A[0, :])
                + c_3 * (A[5, :] - A[-1, :])
            )
            dAdx[3:-3, :] = (
                c_1 * (A[4:-2, :] - A[2:-4, :])
                + c_2 * (A[5:-1, :] - A[1:-5, :])
                + c_3 * (A[6:, :] - A[:-6, :])
            )
            dAdx[-3, :] = (
                c_1 * (A[-2, :] - A[-4, :])
                + c_2 * (A[-1, :] - A[-5, :])
                + c_3 * (A[0, :] - A[-6, :])
            )
            dAdx[-2, :] = (
                c_1 * (A[-1, :] - A[-3, :])
                + c_2 * (A[0, :] - A[-4, :])
                + c_3 * (A[1, :] - A[-5, :])
            )
            dAdx[-1, :] = (
                c_1 * (A[0, :] - A[-2, :])
                + c_2 * (A[1, :] - A[-3, :])
                + c_3 * (A[2, :] - A[-4, :])
            )
        else:
            f_0 = -49.0 / 20.0 * invdx
            f_1 = 6.0 * invdx
            f_2 = -15.0 / 2.0 * invdx
            f_3 = 20.0 / 3.0 * invdx
            f_4 = -15.0 / 4.0 * invdx
            f_5 = 6.0 / 5.0 * invdx
            f_6 = -1.0 / 6.0 * invdx
            dAdx[:3, :] = (
                f_0 * A[:3, :]
                + f_1 * A[1:4, :]
                + f_2 * A[2:5, :]
                + f_3 * A[3:6, :]
                + f_4 * A[4:7, :]
                + f_5 * A[5:8, :]
                + f_6 * A[6:9, :]
            )
            dAdx[3:-3, :] = (
                c_1 * (A[4:-2, :] - A[2:-4, :])
                + c_2 * (A[5:-1, :] - A[1:-5, :])
                + c_3 * (A[6:, :] - A[:-6, :])
            )
            dAdx[-3:, :] = (
                -f_0 * A[-3:, :]
                - f_1 * A[-4:-1, :]
                - f_2 * A[-5:-2, :]
                - f_3 * A[-6:-3, :]
                - f_4 * A[-7:-4, :]
                - f_5 * A[-8:-5, :]
                - f_6 * A[-9:-6, :]
            )
    else:
        msg = "\nERROR in deriv -> accuracy_order in not 2,4 or 6."
        msg += "\nReturning un-initialized array!!!\n"
        print(msg)

    if axis == 0:
        return dAdx
    return dAdx.T


def curl2d_xyz(A, dx, dy, accuracy_order=6, periodic=True):
    """
    Curl of a vector field, using (first) 3 components.
    Not njit-ed since it uses deriv

    INPUTS:
    A              -> numpy.ndaray of shape (n,nx,ny) with n >= 3
    dx,dy          -> float, grid spacing in the two directions
    accuracy_order -> int, 2 or 4 or 6, order of accuracy, default is 6
    periodic       -> bool, if the field is periodic in both directions

    OUTPUTS:
    numpy.ndaray of shape (n,nx,ny)
    """
    curlA = np.empty(A.shape, dtype=np.float64)
    curlA[0] = deriv(A[2], dy, 1, accuracy_order=accuracy_order, periodic=periodic)
    curlA[1] = -deriv(A[2], dx, 0, accuracy_order=accuracy_order, periodic=periodic)
    curlA[2] = deriv(
        A[1], dx, 0, accuracy_order=accuracy_order, periodic=periodic
    ) - deriv(A[0], dy, 1, accuracy_order=accuracy_order, periodic=periodic)
    return curlA


def hessian_eig_on_points(
    A, ij_arr, dx, dy, accuracy_order=6, periodic=True, normalization=True
):
    """
    Computes the eigenvalues and the eigenvectors of the hessian of the given
    scalar field in given grid points.
    Avoiding computing derivative over the whole box was attempted, but for more
    then 10 points it start performing up to 10x worse, probably dur to the high
    number of function calls.

    INPUTS:
    A              -> numpy.ndaray of shape (n,nx,ny) with n >= 3
    ij_arr         -> numpy.ndaray of shape (n,2), data type int(32),
                      n is the number of grid point where eigenvalues
                      and eigenvectors will be computed
    dx,dy          -> float, grid spacing in the two directions
    accuracy_order -> int, 2 or 4 or 6, order of accuracy, default is 6
    periodic       -> bool, if the field is periodic in both directions
    normalization  -> bool, if the hessiam will be normalized by the local value
                      of the A field, default is True

    OUTPUTS:
    eig_val -> numpy.ndaray of shape (2,n), data type float64
    eig_vec -> numpy.ndaray of shape (2,2,n), data type float64
    """
    npts = ij_arr.shape[0]
    dAdx = deriv(A, dx, 0, accuracy_order=accuracy_order, periodic=periodic)
    dAdy = deriv(A, dy, 1, accuracy_order=accuracy_order, periodic=periodic)
    ddAdxdx = deriv(dAdx, dx, 0, accuracy_order=accuracy_order, periodic=periodic)
    ddAdxdy = deriv(dAdx, dx, 1, accuracy_order=accuracy_order, periodic=periodic)
    ddAdydy = deriv(dAdy, dy, 1, accuracy_order=accuracy_order, periodic=periodic)
    del dAdx, dAdy

    H = np.empty((npts, 2, 2), dtype=np.float64)
    if normalization:
        for i_p in range(npts):
            i, j = ij_arr[i_p, :]
            norm = np.abs(A[i, j])
            H[i_p, 0, 0] = np.divide(ddAdxdx[i, j], norm)
            H[i_p, 0, 1] = np.divide(ddAdxdy[i, j], norm)
            H[i_p, 1, 0] = H[i_p, 0, 1]
            H[i_p, 1, 1] = np.divide(ddAdydy[i, j], norm)
        del norm
    else:
        for i_p in range(npts):
            i, j = ij_arr[i_p, :]
            H[i_p, 0, 0] = ddAdxdx[i, j]
            H[i_p, 0, 1] = ddAdxdy[i, j]
            H[i_p, 1, 0] = H[i_p, 0, 1]
            H[i_p, 1, 1] = ddAdydy[i, j]
    del ddAdxdx, ddAdxdy, ddAdydy

    eig_val, eig_vec = np.linalg.eig(H)
    del H

    return np.rollaxis(eig_val, 0, 2), np.rollaxis(eig_vec, 0, 3)
