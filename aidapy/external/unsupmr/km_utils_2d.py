"""
Finding the optimized K for KMeans and ther fit the model to retireve the clusters.

M. Sisti, F. Finelli - 18/11/2020
F. Finelli - 27/03/2021
e-mail: manuela.sisti@univ-amu.fr, francesco.finelli@phd.unipi.it
"""
from time import time as t_time
from os.path import join as os_path_join


from numpy import unique as np_unique
from pandas import read_csv as pd_read_csv
from pandas import DataFrame as pd_DataFrame
from sklearn import preprocessing
from sklearn import metrics
from sklearn.model_selection import KFold
from sklearn.cluster import KMeans as KM


from aidapy.unsupmr.check_utils import type_check, meta_check


# ---------
# scores (for unsupervised clustering)
# inertia == distortion == WSS (always computed)
scores_dict = {}
# pseudo F statistic or Variance Ratio Criterion
scores_dict["CH"] = (metrics.calinski_harabasz_score, "Calinski-Harabasz")
# the average similarity measure of each cluster
# with its most similar cluster, where similarity
# is the ratio of within-cluster distances to between-cluster distances
scores_dict["DB"] = (metrics.davies_bouldin_score, "Davies-Bouldin")
# (b-a)/max(a,b), a='mean intra-cluster distance',
# b='mean nearest-cluster distance', computed for each sample
scores_dict["S"] = (metrics.silhouette_score, "Silhouette Coefficient")


def random_state():
    """
    Generates a random state (int) using time.time()
    """
    t_t = int(t_time() * 1000.0)
    state = (
        ((t_t & 0xFF000000) >> 24)
        + ((t_t & 0x00FF0000) >> 8)
        + ((t_t & 0x0000FF00) << 8)
        + ((t_t & 0x000000FF) << 24)
    )
    return state


def preprocess_dataframe(df, preprocessing_mode):
    """
    Preprocess the dataframe, normalizing or standardizing the columns.

    INPUTS:
    df -> pandas.DataFrame, non empty
    preprocessing_mode -> string or None, oprions are:
                       - None: nothing changes
                       - "norm": df columns are min-max normalized
                       - "std": df columns are standardized
    """
    func_name = "preprocess_dataframe()"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if preprocessing_mode is None:
        return
    if type_check(
        preprocessing_mode,
        str,
        "preprocessing_mode",
        func_name,
        "Nor it is None. Returning.",
    ):
        return

    if preprocessing_mode == "norm":  # normalize the data
        scaler = preprocessing.MinMaxScaler()
    elif preprocessing_mode == "std":  # standardize the data
        scaler = preprocessing.StandardScaler()
    else:
        msg = "ERROR in preprocess_dataframe: preprocessing_mode %s" % (
            preprocessing_mode,
        )
        msg += " is not valid!"
        print(msg, "\nReturning.")
        return
    df[df.columns] = scaler.fit_transform(df[df.columns])


def drop_indices(df, drop_keys=["index_x", "index_y"]):
    """
    Drops columns from a dataframe. Main application is to remove
    the "index_x" and "index_y" columns.

    INPUTS:
    df -> pandas.DataFrame, non empty
    drop_keys -> [str, str, ...], optionale, list of columns names,
                 default is ["index_x", "index_y"]
    """
    func_name = "drop_indices()"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if type_check(drop_keys, list, "drop_keys", func_name, "Returning."):
        return

    for k in drop_keys:
        if k in df.columns:
            del df[k]


def km_crossvalidation(df, meta, cv_file_label="LOGFILE", cv_file_path="."):
    """
    Performs crossvalidation to find the optimal number of clusters (k) in order to
    partion the dataset with KMeans. Also cha test the avaiable KMeans algorithms
    ('full' and 'elkan').

    INPUTS:
    df -> pandas.DataFrame, non empty
    meta -> dictionary of metadata with at least "kmax", "n_foldings", "n_init"
    cv_file_label -> string, optional, label to charachterize the log file,
                     default is "LOGFILE"
    cv_file_path -> string, optional, path of the directory where the
                    logfile will be saved, default is "."
    """
    func_name = "km_crossvalidation()"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(cv_file_label, str, "cv_file_label", func_name, "Returning."):
        return
    if type_check(cv_file_path, str, "cv_file_path", func_name, "Returning."):
        return

    # defining the range for k
    if meta_check("kmin", meta, func_name, 1):
        return
    kmin = meta["kmin"]

    if meta_check("kmax", meta, func_name):
        return
    kmax = meta["kmax"]

    if meta_check("kstep", meta, func_name, 1):
        return
    kstep = meta["kstep"]

    krange = range(kmin, kmax + 1, kstep)

    # setting number of foldings and initialize folding
    if meta_check("n_foldings", meta, func_name):
        return
    n_foldings = meta["n_foldings"]

    k_f = KFold(n_splits=n_foldings, shuffle=True, random_state=random_state())
    train_sets = []
    for train_indices, _ in k_f.split(df):
        train_sets.append(train_indices)

    # setting number of initial states, number of parallel jobs, and maximun number of iteration
    if meta_check("n_init", meta, func_name):
        return
    n_init = meta["n_init"]

    if meta_check("max_iter", meta, func_name, 300):
        return
    max_iter = meta["max_iter"]

    # choosing algorithms ('full' and/or 'elkan')
    if meta_check("algs", meta, func_name, ["full"], True):
        return
    algs = meta["algs"]

    # setting and declaring scores
    if meta_check("scores", meta, func_name, [], True):
        return
    scores = meta["scores"]

    print("\nThe following scores will be computed:\ninertia")
    for scr in scores:
        print(scores_dict[scr][1])

    # initialize output file
    cv_file_name = "KM_cv_%s_kmin%dkmax%dkstep%dnf%dninit%d.log" % (
        cv_file_label,
        kmin,
        kmax,
        kstep,
        n_foldings,
        n_init,
    )
    if meta_check("cv_open", meta, func_name, "a"):
        return
    cv_open = meta["cv_open"]

    # compute and store scores
    with open(os_path_join(cv_file_path, cv_file_name), cv_open) as cv_file:
        cv_file.write("time\talg\tk\tfold\titer\tinertia")
        for scr in scores:
            cv_file.write("\t%s" % (scr))
        cv_file.write("\n")
        for alg in algs:
            print("\n---------------------------------------")
            print("---------------------------------------")
            print("\nalgorithm = %s" % (alg))
            for k in krange:
                print("\n---------------------------------------")
                print("\nk = %d" % (k))
                k_m = KM(
                    n_clusters=k,
                    init="k-means++",
                    n_init=n_init,
                    max_iter=max_iter,
                    random_state=random_state(),
                    algorithm=alg,
                    verbose=0,
                )
                for n in range(n_foldings):
                    print("\nn = %d" % (n))
                    x_train = df.loc[train_sets[n]]
                    print("fitting on train set...")
                    t_ = t_time()
                    k_m.fit(x_train)
                    t_ = t_time() - t_
                    actual_n_iter = k_m.n_iter_
                    inertia = k_m.inertia_
                    print("writing on log file...")
                    cv_file.write(
                        "%f\t%s\t%d\t%d\t%d\t%f"
                        % (t_, alg, k, n, actual_n_iter, inertia)
                    )
                    print("predicting labels on all data...")
                    y_k_m = k_m.predict(df)
                    print("computing scores...")
                    for scr in scores:
                        print("\t%s" % (scores_dict[scr][1]))
                        s_val = scores_dict[scr][0](df, y_k_m)
                        print("\t\twriting score on log file...")
                        cv_file.write("\t%f" % (s_val))
                    print("write and flush log file...")
                    cv_file.write("\n")
                    cv_file.flush()
                    print("cv node done")
    cv_file.close()
    print("\nCrossvalidation done!")


def load_cv_file(meta, cv_file_label="LOGFILE", cv_file_path="."):
    """
    Loads the crossvalidation results from the log file.

    INPUTS:
    meta -> dictionary of metadata with at least "kmax", "n_foldings", "n_init"
    cv_file_label -> string, optional, label to charachterize the log file,
                     default is "LOGFILE"
    cv_file_path -> string, optional, path of the directory where the
                    logfile will be saved, default is "."

    OUTPUTS:
    pandas.DataFrame (or -1 if an error is raised)
    """
    func_name = "load_cv_file()"
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return -1
    if type_check(cv_file_label, str, "cv_file_label", func_name, "Returning."):
        return -1
    if type_check(cv_file_path, str, "cv_file_path", func_name, "Returning."):
        return -1

    if meta_check("kmin", meta, func_name):
        return -1
    kmin = meta["kmin"]
    if meta_check("kmax", meta, func_name):
        return -1
    kmax = meta["kmax"]
    if meta_check("kstep", meta, func_name):
        return -1
    kstep = meta["kstep"]
    if meta_check("n_foldings", meta, func_name):
        return -1
    n_foldings = meta["n_foldings"]
    if meta_check("n_init", meta, func_name):
        return -1
    n_init = meta["n_init"]
    cv_file_name = "KM_cv_%s_kmin%dkmax%dkstep%dnf%dninit%d.log" % (
        cv_file_label,
        kmin,
        kmax,
        kstep,
        n_foldings,
        n_init,
    )
    return pd_read_csv(os_path_join(cv_file_path, cv_file_name), sep="\t", header=0)


def process_cv_df(cv_df, meta):
    """
    Process the dataframe obtained from the log file of km_crossvalidation.
    Averages over the foldings and normalizes the scores.

    INPUTS:
    cv_df -> pands.DataFrame, should be loaded from the km_crossvalidation log file
    meta -> dict, dictionary of metadataù

    OUTPUTS:
    cv_df_mean -> pands.DataFrame, cv_df averaged over the foldings
    cv_df_norm -> pands.DataFrame, cv_df_mean min-max normalized
    """
    func_name = "process_cv_df()"
    if type_check(cv_df, pd_DataFrame, "df", func_name, "Returning."):
        return -1
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return -1

    # state maximum number of iterations actually needed
    if not meta_check("max_iter", meta, func_name):
        max_iter = meta["max_iter"]
        print("\nMax number of iteration needed:")
        print("%d over %d" % (cv_df["iter"].max(), max_iter))
        if cv_df["iter"].max() == max_iter:
            msg = "\nWARNING from process_cv_df ->  maximum number of iterations "
            msg += "reached before convergence; consider increasing max_iter and retry."
            print(msg)

    # mean over k-folds
    if meta_check("scores", meta, func_name, impose_list=False):
        return -1
    scores = meta["scores"]
    if meta_check("n_foldings", meta, func_name):
        return -1
    n_foldings = meta["n_foldings"]

    tmp_col = ["time", "iter", "inertia"] + scores
    tmp_val = cv_df.loc[cv_df["fold"] == 0, tmp_col].values
    for n in range(n_foldings - 1):
        tmp_val += cv_df.loc[cv_df["fold"] == (n + 1), tmp_col].values
    tmp_val /= float(n_foldings)
    cv_df_mean = pd_DataFrame(tmp_val)
    cv_df_mean.columns = tmp_col
    
    tmp_col = ["alg", "k"]
    tmp_val = cv_df.loc[cv_df["fold"] == 0, tmp_col].values
    cv_df_mean[tmp_col] = pd_DataFrame(tmp_val)
    cv_df_mean = cv_df_mean[["time", "alg", "k", "iter", "inertia"] + scores]

    # compute normalized cv data (already fold_averaged)
    scaler = preprocessing.MinMaxScaler()
    
    ## ----------- SJ : 23/04/2021 (start)  ----------- ##
    #import copy
    #cv_df_norm = copy.deepcopy(cv_df_mean) ## force the same format of dataframe
    ## ----------- SJ : 23/04/2021  (end) ----------- ##
    
    cv_df_norm = cv_df_mean.loc[:, ["time", "alg", "k", "iter"]]
    tmp_col = ["inertia"] + scores
    cv_df_norm[tmp_col] = scaler.fit_transform(cv_df_mean[tmp_col])
    del tmp_col, tmp_val

    return cv_df_mean, cv_df_norm


def set_kopt(kopt, meta, overwrite=False):
    """
    Sets inside the metadata the optimal value of k.

    INPUTS:
    kopt -> int, optimal value of k
    meta -> dict, dictionary of metadata
    overwrite -> bool, optional, if the value could be overwritten
                 in the metadata dictionary, default is False
    """
    func_name = "set_kopt()"
    if type_check(kopt, int, "kopt", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(overwrite, bool, "overwrite", func_name, "Returning."):
        return

    if "kopt" in meta:
        print(
            "\nWARNING from set_kopt -> kopt already defined in meta (kopt = %d)"
            % meta["kopt"]
        )
        if overwrite:
            print("overwrite is set to True, updating kopt to %d" % kopt)
        else:
            print("overwrite is set to False, kopt will not be updated")
            return
    meta["kopt"] = kopt


def set_alg_opt(alg_opt, meta, overwrite=False):
    """
    Sets inside the metadata the optimal kmeans algorithm.

    INPUTS:
    alg_opt -> str, optimal kmeans algorithm
    meta -> dict, dictionary of metadata
    overwrite -> bool, optional, if the value could be overwritten
                 in the metadata dictionary, default is False
    """
    func_name = "set_alg_opt()"
    if type_check(alg_opt, str, "alg_opt", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(overwrite, bool, "overwrite", func_name, "Returning."):
        return

    if "alg_opt" in meta:
        msg = "\nWARNING from set_alg_opt -> alg_opt already defined in meta"
        msg += " (alg_opt = %s)" % meta["alg_opt"]
        print(msg)
        if overwrite:
            print("overwrite is set to True, updating alg_opt to %s" % alg_opt)
        else:
            print("overwrite is set to False, alg_opt will not be updated")
            return
    meta["alg_opt"] = alg_opt


def km_clusterization(df, meta):
    """
    Clusterize the entries of a dataframe using KMeans with given k and algoritm.

    INPUTS:
    df -> pandas.DataFrame, non empty
    meta -> dictionary of metadata with at least "kmax", "n_foldings", "n_init"
    """
    func_name = "KM_clusterizzation()"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return

    # optimal k
    if meta_check("kopt", meta, func_name):
        return
    kopt = meta["kopt"]

    # setting number of initial states, number of parallel jobs, and maximun number of iteration
    if meta_check("n_init", meta, func_name):
        return
    n_init = meta["n_init"]

    if meta_check("max_iter", meta, func_name, 300):
        return
    max_iter = meta["max_iter"]

    # optimal algorithm
    if meta_check("alg_opt", meta, func_name):
        return
    alg_opt = meta["alg_opt"]

    # clusterization
    print("\n---------------------------------------")
    print("---------------------------------------")
    print("\nalgorithm = %s" % (alg_opt))
    print("\n---------------------------------------")
    print("\nk = %d" % (kopt))
    k_m = KM(
        n_clusters=kopt,
        init="k-means++",
        n_init=n_init,
        max_iter=max_iter,
        random_state=random_state(),
        algorithm=alg_opt,
        verbose=0,
    )
    print("fitting on data set...")
    t_ = t_time()
    k_m.fit(df)
    t_ = t_time() - t_
    actual_n_iter = k_m.n_iter_
    inertia = k_m.inertia_
    print("\nCrossvalidation done!")
    print("Time to fit = %f s" % t_)
    print("Number of iterations needed = %d (max: %d)" % (actual_n_iter, max_iter))
    if actual_n_iter == max_iter:
        msg = "\nWARNING from km_clusterization ->  maximum number of iterations "
        msg += "reached before convergence; consider increasing max_iter and retry."
        print(msg)
    print("Inertia = %f" % inertia)

    # adding labels to dataframe
    df["label"] = k_m.labels_
    del k_m

    # suggesting label of "reconnections" cluster
    if meta_check("J_like", meta, func_name, df.columns[0]):
        return
    J_like = meta["J_like"]
    labels = np_unique(df["label"].values)
    lmin = labels[0]
    Jmeanmax = df[J_like].min()
    lrec = lmin - 1
    for lab in labels:
        J_ = df[df["label"] == lab][J_like].mean()
        if J_ > Jmeanmax:
            Jmeanmax = J_
            lrec = lab
    if lrec < lmin:
        msg = "\nERROR in km_clusterization -> something went wrong "
        msg += 'while looking for the "reconnections" cluster.'
        print(msg, "\nNo suggestions possible")
    else:
        msg = 'The cluster suggested as the "reconnections" one is the with '
        msg += "label = %d\nThis suggestion is based on the mean value " % lrec
        msg += "of %s in each cluster, " % J_like
        msg += "the one with the highest mean was choosen\n"
        print(msg)
    print("Use set_lab_rec() to set in metadata the desired value for the label.")


def set_lab_rec(lab_rec, meta, overwrite=False):
    """
    Sets inside the metadata the label of the "reconnection" cluster.

    INPUTS:
    lab_rec -> int, label of the "reconnection" cluster
    meta -> dict, dictionary of metadata
    overwrite -> bool, optional, if the value could be overwritten
                 in the metadata dictionary, default is False
    """
    func_name = "set_lab_rec()"
    if type_check(lab_rec, int, "lab_rec", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(overwrite, bool, "overwrite", func_name, "Returning."):
        return

    if ("lab_rec" in meta) and (meta["lab_rec"] != "NOT SET"):
        msg = "\nWARNING from set_lab_rec -> lab_rec already "
        msg += "defined in meta (lab_rec = %d)" % meta["lab_rec"]
        print(msg)
        if overwrite:
            print("overwrite is set to True, updating lab_rec to %d" % lab_rec)
        else:
            print("overwrite is set to False, lab_rec will not be updated")
            return
    meta["lab_rec"] = lab_rec
