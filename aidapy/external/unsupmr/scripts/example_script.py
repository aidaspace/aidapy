"""
Script to test the unsupervised_reconnection modules

F. Finelli - 8/04/2021
e-mail: francesco.finelli@phd.unipi.it
"""
# libs
import os
from os import makedirs as os_makedirs
from os.path import exists as os_path_exists
from os.path import join as os_path_join
from sys import exit as sys_exit

from h5py import File as h5_File
from numpy import absolute as np_abs
from numpy import array as np_array

from aidapy.unsupmr.ingester import DataIngestion
from aidapy.unsupmr.calc_utils_2d import (
    curl2d_xyz,
    norm_xyz,
    norm_xy,
    scalar_xyz,
    cross_xyz,
    hessian_eig_on_points,
)
from aidapy.unsupmr.plot_utils_2d import plot_field, plot_scores, plot_clusters, plot_regions_mask
from aidapy.unsupmr.km_utils_2d import (
    preprocess_dataframe,
    km_crossvalidation,
    load_cv_file,
    process_cv_df,
    set_kopt,
    set_alg_opt,
    km_clusterization,
    set_lab_rec,
)
from aidapy.unsupmr.dbscan_utils_2d import (
    dbscan_on_cluster,
    indices_of_regions_maxima,
    save_regions_maxima,
    load_regions_maxima,
)
from aidapy.unsupmr.ar_utils_2d import (
    structures_width,
    structures_thickness,
    compute_aspect_ratio,
    save_aspect_ratios,
    load_aspect_ratios,
)

from aidapy.unsupmr.check_utils import version_check

current_path = './aidapy/unsupmr/scripts/'

# check libraries versione
if version_check():
    sys_exit()

# start
print("\nWARNING: THIS TEST HAS THE SOLE PURPOSE TO VERIFY MODULES FUNCTIONALITY." +
      " THE DATASET INVOLVED IS TOO SMALL TO PRODUCE MEANINGFUL RESULTS.\n")
print("\nSTART OF TEST\n")

# set output dir
FLG = True
while FLG:
    #out_dir = input("Insert the path to the output directories (savestate and plots): ")
    out_dir = os.path.join(current_path, "example_outputs/")
    if not os_path_exists(out_dir):
        os.makedirs(out_dir)
        #os_makedirs(out_dir)
        #try:
        #    os_makedirs(out_dir)
        #except Exception as e_path:
        #    print(e_path)
        #    print("Something went wrong while creating out_dir, retry.")
        #    continue
    FLG = False

# -> savestate path
savestate_path = os_path_join(out_dir, "savestate")
if not os_path_exists(savestate_path):
    os_makedirs(savestate_path)

# -> plots path
plot_path = os_path_join(out_dir, "plots")
if not os_path_exists(plot_path):
    os_makedirs(plot_path)

# init ingestion system and metadata
di = DataIngestion("test")
META_PATH = os.path.join(current_path, "example_metadata.cfg")
di.get_meta(META_PATH)
di.compute_meta()
di.print_meta()

# load hdf5 file
DATA_FILE = os.path.join(current_path, "sample_data.h5")  # HARDCODED - CHANGE IF NEEDED
h5file = h5_File(DATA_FILE, "r")

for key in h5file:
    di.ingest(np_array(h5file[key]), key)

di.meta["t"] = h5file.attrs["t"]
di.meta["run_name"] = h5file.attrs["run_name"]

h5file.close()

# compute quantities
# -> norm of electric current
J = curl2d_xyz(di.data["B"], di.meta["dx"], di.meta["dy"], periodic=di.meta["periodic"])
di.ingest(J, "J")
del J

Jn = norm_xyz(di.data["J"])
di.ingest(Jn, "Jn")
del Jn
di.meta["J_like"] = "Jn"  # this is needed later

# -> norm of electron velocity in the plane x,y
ue = di.data["u"] - di.data["J"] / di.data["n"]
del di.data["u"], di.data["n"]
di.ingest(ue, "ue")
del ue

ue_plane = norm_xy(di.data["ue"])
di.ingest(ue_plane, "ue_plane")
del ue_plane

# -> electron vorticity (norm of)
vort_ue = curl2d_xyz(
    di.data["ue"], di.meta["dx"], di.meta["dy"], periodic=di.meta["periodic"]
)
vort_ue = norm_xyz(vort_ue)
di.ingest(vort_ue, "vort_ue")
del vort_ue

# -> B in plane
B_plane = norm_xy(di.data["B"])
di.ingest(B_plane, "B_plane")
del B_plane

# -> J dot E
JE = scalar_xyz(di.data["J"], di.data["E"])
di.ingest(JE, "JE")
del JE, di.data["J"]

# -> | (E + ue x B )_z | (electron decoupling, abs of z component)
e_dec = di.data["E"] + cross_xyz(di.data["ue"], di.data["B"])
del di.data["E"], di.data["ue"], di.data["B"]
e_dec = np_abs(e_dec[2])
di.ingest(e_dec, "e_dec")
del e_dec

# make a plot to check it is allright
plot_field(
    di.data["Jn"],
    di.meta,
    file_name="Jn.png",
    file_dir=plot_path,
    xlabel="x [d_i]",
    ylabel="y [d_i]",
    field_name="Jn",
)

# pass from data to dataframe
keys = list(di.data.keys())
for key in keys:
    if key == di.meta["J_like"]:  # we will nwwd 'Jn' later, not normalized/standardized
        DELETE_DATA = False  # while di.dataframe will be normalized/standardized
    else:
        DELETE_DATA = True
    di.to_dataframe(key, delete_data=DELETE_DATA)

print(di.dataframe.head())

# dataframe preprocessing
preprocess_dataframe(di.dataframe, di.meta["preprocessing_mode"])

# run crossvalidation
km_crossvalidation(
    di.dataframe, di.meta, cv_file_label="TEST", cv_file_path=savestate_path
)

# load and plot scores
CV_df = load_cv_file(di.meta, cv_file_label="TEST", cv_file_path=savestate_path)

_, CV_df_norm = process_cv_df(CV_df, di.meta)

plot_scores(CV_df_norm, di.meta, file_name="CV.png", file_dir=plot_path)

# set optimal k and algorithm
KOPT = int(input("Optimal k: "))
set_kopt(KOPT, di.meta)

ALG_OPT = input("Optimal algorithm: ")
set_alg_opt(ALG_OPT, di.meta)

# make kopt ckusters
km_clusterization(di.dataframe, di.meta)
print(di.dataframe.head())

# select "reconnections" cluster
lab_rec = int(input('label of "reconnections" cluster: '))
while lab_rec != di.meta["lab_rec"]:
    set_lab_rec(lab_rec, di.meta, overwrite=True)
    plot_clusters(
        di.dataframe,
        di.meta,
        file_name="clusters.png",
        file_dir=plot_path,
        xlabel="x [d_i]",
        ylabel="y [d_i]",
    )
    lab_rec = int(input('label of "reconnections" cluster: '))

# add grid indices to dataframe
di.grid_indices_to_dataframe()

# --------------------------------------------------------------
#     THIS IS JUST TO TEST THE ABILITY TO SAVE
#     AND THEN LOAD THE STATE
# save the state
di.save_meta("test_1.meta", meta_file_path=savestate_path)
di.dataframe_to_file("test_1.csv", df_file_path=savestate_path)

# load the state
di.get_meta("test_1.meta", cfg_file_path=savestate_path)
di.compute_meta()
di.print_meta()
di.file_to_dataframe("test_1.csv", df_file_path=savestate_path)
print(di.dataframe.head())
# ---------------------------------------------------------------

# apply dbscan
mask = dbscan_on_cluster(di.dataframe, di.meta)

# plot regions
plot_regions_mask(
    mask,
    di.meta,
    file_name="regions.png",
    file_dir=plot_path,
    xlabel="x [d_i]",
    ylabel="y [d_i]",
)

# compute regions maxima
maxima_indices = indices_of_regions_maxima(di.dataframe, di.meta["J_like"])

# --------------------------------------------------------------
#     THIS IS JUST TO TEST THE ABILITY TO SAVE
#     AND THEN LOAD THE STATE
# save the maxima positions
save_regions_maxima(maxima_indices, "max_ind.dat", max_file_path=savestate_path)

# load the maxima positions
maxima_indices = load_regions_maxima("max_ind.dat", max_file_path=savestate_path)
# --------------------------------------------------------------

# compute aspect ratio
# -> compute width
regions_width = structures_width(
    mask, di.meta["dx"], di.meta["dy"], periodic=di.meta["periodic"]
)

# -> compute thickness
eig_val, eig_vec = hessian_eig_on_points(
    di.data["Jn"],
    maxima_indices,
    di.meta["dx"],
    di.meta["dy"],
    periodic=di.meta["periodic"],
)

regions_thickness = structures_thickness(
    di.data["Jn"],
    maxima_indices,
    eig_val,
    eig_vec,
    di.meta["dx"],
    di.meta["dy"],
    periodic=di.meta["periodic"],
)

# -> aspect ratio
regions_ar = compute_aspect_ratio(regions_width, regions_thickness)

# --------------------------------------------------------------
#     THIS IS JUST TO TEST THE ABILITY TO SAVE
#     AND THEN LOAD THE STATE
# -> save aspect ratio
save_aspect_ratios(regions_ar, "aspect_ratios.dat", ar_file_path=savestate_path)

# -> load aspect ratios
regions_ar = load_aspect_ratios("aspect_ratios.dat", ar_file_path=savestate_path)
# --------------------------------------------------------------

# end
print("\nEND OF TEST :)\n")
