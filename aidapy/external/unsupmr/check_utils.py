"""
Some useful functions to make some checks about
     - libraries verion
     - variables types
     - presence in metadata

F. Finelli - 13/04/2021
e-mail: francesco.finelli@phd.unipi.it
"""
from numpy import __version__ as numpy_ver
from scipy import __version__ as scipy_ver
from sklearn import __version__ as sklearn_ver
from pandas import __version__ as pandas_ver
from matplotlib import __version__ as matplotlib_ver
from numba import __version__ as numba_ver
from packaging.version import parse as pv_parse


versions = {
    "numpy": {"current": numpy_ver, "tested": "1.19.1", "required": ""},
    "scipy": {"current": scipy_ver, "tested": "1.6.1", "required": "1.6.1"},
    "sklearn": {"current": sklearn_ver, "tested": "0.23.2", "required": ""},
    "pandas": {"current": pandas_ver, "tested": "1.1.3", "required": ""},
    "matplotlib": {"current": matplotlib_ver, "tested": "3.3.2", "required": ""},
    "numba": {"current": numba_ver, "tested": "0.51.2", "required": ""},
}


def version_check():
    """
    It checks the versions of the libraries used in the unsupervised_reconnection modules,
    aside from os, time, and collections.
    An ERROR will be displayed if a library has a version prior to the one required
    (returns 1).
    A WARNING will be displayed if a library has a version prior to the one used during
    development and testing.

    OUTPUTS:
    0 or 1 -> int, returns 1 if an ERROR is raised, 0 otherwise
    """
    err_flg = False
    for lib in versions:
        if pv_parse(versions[lib]["current"]) < pv_parse(versions[lib]["required"]):
            msg = "\nERROR in ar_utils_2D.py -> %s %s is in use" % (
                lib,
                versions[lib]["current"],
            )
            msg += " but at least version %s is required!" % (
                versions[lib]["required"],
            )
            print(msg)
            err_flg = True
        elif pv_parse(versions[lib]["current"]) < pv_parse(versions[lib]["tested"]):
            msg = "\nWARNING from ar_utils_2D.py -> %s %s is in use" % (
                lib,
                versions[lib]["current"],
            )
            msg += " but the module was tested using version %s." % (
                versions[lib]["tested"],
            )
            print(msg)
    if err_flg:
        return 1
    return 0


def type_check(obj, typ, obj_name="", func_name="", message=""):
    """
    Check the type of a given object.

    INPUTS:
    obj       -> an "object" (everything should be ok)
    type      -> a type object (int,str,float,...)
    obj_name  -> string, name referring to obj
    func_name -> string, name of the function calling type_check
    message   -> string, a message to be printed after the error

    OUTPUTS:
    0 or 1 -> returns 0 if isinstance(obj, typ) is True, 1 otherwise
    """
    if not isinstance(typ, type):
        print("ERROR in type_check -> typ is not 'type'.\nReturning.")
        return 1
    if not isinstance(obj_name, str):
        print("ERROR in type_check -> obj_name is not 'str'.\nReturning.")
        return 1
    if not isinstance(func_name, str):
        print("ERROR in type_check -> func_name is not 'str'.\nReturning.")
        return 1
    if not isinstance(message, str):
        print("ERROR in type_check -> message is not 'str'.\nReturning.")
        return 1
    if func_name != "":
        func_name = "in %s" % (func_name,)
    if not isinstance(obj, typ):
        print(
            "ERROR %s -> %s is not '%s'.\n%s"
            % (func_name, obj_name, typ.__name__, message)
        )
        return 1
    return 0


def meta_check(key, meta, func_name="", default=None, impose_list=False):
    """
    Check if a given key is present in a given (metadata) dictionary.
    If default=None, an ERROR message will be displayed if key is not
    in the dictionary, otherwise a WARNING message will be displayed
    and meta[key] will be set to the value given by the default argument.
    When the ERROR message is displayed this function returns 1, it
    returns 0 otherwise (it will return 1 also when one of the argument
    has the wrong type).

    INPUTS:
    key -> string, key to be searched for
    meta -> dict or collections.defaultdict, dictionary where key
            will be searched for
    func_name -> string, optional, name of the calling function,
                 default is ""
    default -> whatever, optional, default is None

    OUTPUTS:
    0 or 1 -> int, 1 if key is not in meta and default is None, 0 otherwise
    """
    if type_check(key, str, "key", "check_meta", "Returning."):
        return 1
    if type_check(meta, dict, "meta", "check_meta", "Returning."):
        return 1
    if type_check(func_name, str, "func_name", "check_meta", "Returning."):
        return 1
    if type_check(impose_list, bool, "impose_list", "check_meta", "Returning."):
        return 1

    ret_val = 0
    if default is None:
        prep = "in"
        head = "ERROR"
        action = "Returning 1 (could cause calling function to return)."
        new_ret_val = 1
        set_flg = False
    else:
        prep = "from"
        head = "WARNING"
        action = "Setting %s to %s" % (key, str(default))
        new_ret_val = 0
        set_flg = True
    if func_name != "":
        func_name = "%s %s" % (prep, func_name)
    if key not in meta:
        message = "%s %s -> %s is not in metadata.\n%s" % (head, func_name, key, action)
        print(message)
        if set_flg:
            meta[key] = default
        ret_val = new_ret_val
    if impose_list:
        if not isinstance(meta[key], list):
            meta[key] = [meta[key]]
            print("%s is now a list" % (key,))
    return ret_val
