"""
This is a class made to "ingest" data in a dictionary (defaultdict) of
numpy.arrays and later in a pandas.DataFrame, metadata are loaded
in a dictionary (defaultdict).

F. Finelli - 27/03/2021
e-mail: francesco.finelli@phd.unipi.it
"""

from os.path import join as os_path_join
from collections import defaultdict as cll_defaultdict


import numpy as np
from pandas import DataFrame as pd_DataFrame
from pandas import read_csv as pd_read_csv


from aidapy.unsupmr.check_utils import type_check


class DataIngestion:
    """
    This class objective is to handle data, dataframe, and metadata
    in a safe and coherent way. Also the writing on file and the loading
    of metadata and dataframe is implemented.
    """

    def __init__(self, name):
        if type_check(name, str, "name", "__init__", "Retutning."):
            return

        self.name = name  # name of the data collection
        self.data = cll_defaultdict(lambda: np.ndarray(0))  # dict containing data
        self.dataframe = pd_DataFrame()  # pandas DataFrame containing data
        self.meta = cll_defaultdict(lambda: "NOT SET")  # dict containing metadata
        self._saved_dataframe = pd_DataFrame()  # a saved copy of dataframe
        self._known_meta = {
            "int": [
                "nx",
                "ny",
                "n_init",
                "max_iter",
                "n_foldings",
                "kmin",
                "kmax",
                "kstep",
                "kopt",
                "lab_rec",
                "DB_min_samples",
            ],
            "float": ["xl", "yl", "dx", "dy", "DB_eps"],
            "bool": ["periodic"],
            "str": [
                "algs",
                "scores",
                "preprocessing_mode",
                "cv_open",
                "alg_opt",
                "J_like",
            ],
            "list": ["algs", "scores"],
        }  # dict containing names of
        # known metadata w/ relative types

    @staticmethod
    def help():
        """
        Prints out a brief description of the class.
        """
        msg = (
            "\nBelow, all attributes of the data_ingestion class will be printed out, "
        )
        msg += "with a brief description. For more details on methodos use "
        msg += "help(<self>.<method>).\nFor any problem/question, you can write "
        msg += "to <francesco.finelli@phd.unipi.it>."
        print(msg)
        print("\nPROPERTIES:")
        print("name -> distinctive name of the object")
        print("data -> defaultdict containing numpy.ndarray")
        print("dataframe -> a pandas.DataFrame")
        print("meta -> defaultdict containing metadata")
        print("\nMETHODS:")
        print("get_meta() -> reads metadata from a file")
        print("compute_meta() -> tries to compute additional metadata")
        print("print_meta() -> prints out the metadata")
        print("save_meta() -> saves the metadata on file")
        print("ingest() -> adds an array to the data dict")
        print("to_dataframe() -> adds one entry of data to dataframe")
        print("grid_indices_to_dataframe() -> adds grid indices to dataframe")
        print("dataframe_to_file() -> writes dataframe into a file")
        print("file_to_dataframe() -> load a file into dataframe")
        print("save_dataframe() -> saves internally a copy of dataframe")
        print("restore_dataframe() -> restores the internally saved copy of dataframe")
        print("")

    # pylint: disable=too-many-branches
    def get_meta(self, cfg_file_name, cfg_file_path="."):
        """
        It retrives metadata from a configuration file.

        INPUTS:
        cfg_file_name -> <str>, name of the file, e.g. 'test.cfg'
        cfg_file_path -> <str>, optional, path which will contain the file, default is '.'
        """
        func_name = "'%s'.get_meta()" % (self.name,)
        if type_check(cfg_file_name, str, "cfg_file_name", func_name, "Retutning."):
            return
        if type_check(cfg_file_name, str, "cfg_file_path", func_name, "Retutning."):
            return

        cfg_file_wholepath = os_path_join(
            cfg_file_path, cfg_file_name
        )  # create full path
        try:
            with open(cfg_file_wholepath, "r") as cfg_file:  # read file
                lines = cfg_file.readlines()
        except FileNotFoundError as e_nf:  # if open/red files, just returns, no metadata created
            print("In %s ->" % (func_name,), e_nf, "\nReturning.")
            return
        for line in lines:  # create metadata
            line = line.split("#")[0]
            if line.strip() == "":
                continue
            key, values = line.split("=")
            key = key.strip()
            values = [v.strip() for v in values.split(",")]
            nvalues = len(values)
            if key in self.meta:
                msg = "WARNING: In %s -> " % (func_name,)
                msg += "parameter '%s' was already in metadata, " % (key,)
                msg += "it will be overwritten."
                print(msg)
            if key in self._known_meta["int"]:  # cast if metadata type is known
                if nvalues == 1:
                    self.meta[key] = int(values[0])
                else:
                    self.meta[key] = list(map(int, values))
            elif key in self._known_meta["float"]:
                if nvalues == 1:
                    self.meta[key] = float(values[0])
                else:
                    self.meta[key] = list(map(float, values))
            elif key in self._known_meta["bool"]:
                if nvalues == 1:
                    self.meta[key] = bool(values[0])
                else:
                    self.meta[key] = list(map(bool, values))
            else:
                if nvalues == 1:
                    self.meta[key] = values[0]
                else:
                    self.meta[key] = values
            if (key in self._known_meta["list"]) and (nvalues == 1):
                self.meta[key] = [self.meta[key]]

    def compute_meta(self, verbose=True):
        """
        Computes additional metadata from the ones already given, if possible.

        INPUTS:
        verbose -> <boolean>, optional, if a message should be displayed when
                   metadata are added, default is True
        """
        func_name = "'%s'.compute_meta()" % (self.name,)
        if type_check(verbose, bool, "verbose", func_name, "Retutning."):
            return

        nx_in = "nx" in self.meta
        xl_in = "xl" in self.meta
        dx_in = "dx" in self.meta
        x_in = "x" in self.meta
        ny_in = "ny" in self.meta
        yl_in = "yl" in self.meta
        dy_in = "dy" in self.meta
        y_in = "y" in self.meta
        if nx_in and xl_in and (not dx_in):
            self.meta["dx"] = self.meta["xl"] / float(self.meta["nx"])
            dx_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("dx", self.name))
        elif nx_in and (not xl_in) and dx_in:
            self.meta["xl"] = self.meta["dx"] * float(self.meta["nx"])
            xl_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("xl", self.name))
        if ny_in and yl_in and (not dy_in):
            self.meta["dy"] = self.meta["yl"] / float(self.meta["ny"])
            dy_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("dy", self.name))
        elif ny_in and (not yl_in) and dy_in:
            self.meta["yl"] = self.meta["dy"] * float(self.meta["ny"])
            yl_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("yl", self.name))
        if nx_in and xl_in and dx_in and (not x_in):
            self.meta["x"] = np.linspace(
                0.0, self.meta["xl"] - self.meta["dx"], self.meta["nx"]
            )
            x_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("x", self.name))
        if ny_in and yl_in and dy_in and (not y_in):
            self.meta["y"] = np.linspace(
                0.0, self.meta["yl"] - self.meta["dy"], self.meta["ny"]
            )
            y_in = True
            if verbose:
                print("'%s' added to '%s'.meta..." % ("y", self.name))

    def print_meta(self):
        """
        Just prints all the aviable metadata, aside for 'x' and 'y',
        which are usually long arrays.
        """
        print("\n==========METADATA==========")
        for k in self.meta:
            if k in ["x", "y"]:
                continue
            print(k, "=", self.meta[k])
        print("============================\n")

    def save_meta(self, meta_file_name, meta_file_path=".", mode="w"):
        """
        Save current metadata on a file, aside from 'x' and 'y'

        INPUTS:
        meta_file_name -> <str>, name of the file, e.g. 'test.cfg'
        meta_file_path -> <str>, optional, path which will contain the file, default is '.'
        mode           -> <str>, optional, if 'w' overwrite the file if already eisting,
                           if 'a' append to it, default is 'w'
        """
        func_name = "'%s'.save_meta()" % (self.name,)
        if type_check(meta_file_name, str, "meta_file_name", func_name, "Retutning."):
            return
        if type_check(meta_file_path, str, "meta_file_path", func_name, "Retutning."):
            return
        if type_check(mode, str, "mode", func_name, "Retutning."):
            return
        if mode not in ["w", "a"]:
            msg = "In %s -> mode is not 'w' or 'a'" % (func_name,)
            print(msg, "\nReturning.")
            return

        with open(os_path_join(meta_file_path, meta_file_name), mode) as f_meta:
            for k in self.meta:
                if k in ["x", "y"]:
                    continue
                f_meta.write("%s = " % k)
                if not isinstance(self.meta[k], list):
                    f_meta.write("%s" % str(self.meta[k]))
                else:
                    f_meta.write("%s" % str(self.meta[k][0]))
                    for val in self.meta[k][1:]:
                        f_meta.write(",%s" % str(val))
                f_meta.write("\n")

    # pylint: disable=too-many-return-statements
    def ingest(
        self,
        data,
        data_name,
        verbose=False,
        force_c_order=True,
    ):
        """
        Inserta a data array in the self.data dict, under a given name (key).
        Data should be a numpy.ndarray with shape (n1,n2,...,np,nx,ny),
        with nx and ny possibly defined as metadata before the ingestion
        and (n1,...,ns) the number of p-tensor components (we refer to (n1,...,ns) as
        ``field shape''). Dimensions with length 1 will be omitted (a scalar field will
        have shape (nx,ny)).
        E.g.:   scalar field   ->     (nx,ny)
                vector field   ->   (3,nx,ny)
                2-tensor field -> (3,3,nx,ny)
        C ordering is preferred and can be imposed with force_C_order=True.

        INPUTS:
        data_name     -> <str>, name/key of a data array to be added in self.data
        verbose       -> <boolean>, optional, if a message should be displayed
                         upon completion, default is False
        force_c_order -> <boolean>, optional, if the C ordering in memory should be
                         imposed onto the ``ingested'' array, default is True
        """
        func_name = "'%s'.ingest()" % (self.name,)
        if type_check(data, np.ndarray, "data", func_name, "Retutning."):
            return
        if type_check(data_name, str, "data_name", func_name, "Retutning."):
            return
        if type_check(verbose, bool, "verbose", func_name, "Retutning."):
            return
        if type_check(force_c_order, bool, "force_c_order", func_name, "Retutning."):
            return

        if data_name in self.data:  # test input
            msg = "In %s -> data_name '%s' " % (func_name, data_name)
            msg += "already present in '%s'.data" % (self.name,)
            print(msg, "\nReturning.")
            return
        if data_name in ["index_x", "index_y"]:  # test input
            msg = "In %s -> data_name '%s' " % (func_name, data_name)
            msg += "is reserved to grid indices, please choose another name."
            print(msg, "\nReturning.")
            return
        data_shape = data.squeeze().shape  # get shape
        if len(data_shape) < 2:  # test input
            msg = "In %s -> data '%s' is not a field defined " % (
                func_name,
                data_name,
            )
            msg += " on a 2D grid ( data shape = " + str(data_shape) + " )"
            print(msg, "\nReturning.")
            return
        if ("nx" in self.meta) and ("ny" in self.meta):
            if data_shape[-2:] != (self.meta["nx"], self.meta["ny"]):  # test input
                msg = "In %s -> data '%s' is defined " % (
                    func_name,
                    data_name,
                )
                msg += "on a grid (%d,%d), " % (data_shape[-2], data_shape[-1])
                msg += "not on a grid (%d,%d) " % (self.meta["nx"], self.meta["ny"])
                print(msg, "\nReturning.")
                return
        else:  # if nx and ny are not set, data will be added but a warning will rise
            msg = "WARNING: In %s -> " % (func_name,)
            msg += "data '%s' is defined " % (data_name,)
            msg += "on a grid (%d,%d), " % (data_shape[-2], data_shape[-1])
            msg += "but nx and/or ny are not specified in metadata."
            print(msg)
        if force_c_order and (not data.squeeze().flags["C_CONTIGUOUS"]):
            self.data[data_name] = np.ascontiguousarray(data.squeeze())
        else:
            self.data[data_name] = data.squeeze()
        if verbose:
            msg = "'%s' added to '%s'.data, " % (data_name, self.name)
            msg += "field of shape " + str(data_shape[:-2]) + " "
            msg += "defined on a grid " + str(data_shape[-2:]) + "."
            print(msg)

    def to_dataframe(self, data_name, delete_data=False):
        """
        Copies an entry of the data dictionary (a numpy.ndarray) into the dataframe
        (as one or more columns). If delete_data is True, the dict entry is then deleted.
        If the dict entry is a scalar field, it will be result into a single column.
        If the dict entry is a field with shape (n1,n2,..,ns), it will result into
        n1*n2*...*ns columns which names end with s ``subscripts'', from _0_0..._0 to
        _(n1-1)_(n2-1)_..._(ns-1).
        E.g., a vector field 'V' with shape (3,) will result into trhee columns
        named 'V_0', 'V_1', and 'V_2'.
        NOTE: with ``field shape'' we refer the the ``components'' of the field,
        in the physical sense. E.g., if the a vector field 'V' has ``field shape'' (3,),
        then the array self.data['V'] has shape (3,nx,ny)

        INPUTS:
        data_name   -> <str>, name/key of the data array to include into the dataframe
        delete_data -> <boolean>, optional, if the relative entry in self.data sholud
                       be removed (to save space in memory), default is False
        """
        func_name = "'%s'.to_dataframe()" % (self.name,)
        if type_check(data_name, str, "data_name", func_name, "Retutning."):
            return
        if type_check(delete_data, bool, "delete_data", func_name, "Retutning."):
            return

        if data_name not in self.data:  # test input
            msg = "In %s -> data_name '%s' " % (func_name, data_name)
            msg += "not present in '%s'.data" % (self.name,)
            print(msg, "\nReturning.")
            return
        field_shape = self.data[data_name].shape[:-2]
        field_dims = len(field_shape)
        if field_dims == 0:
            self.dataframe[data_name] = self.data[data_name].flatten()
        else:
            for raveled_i in range(np.prod(field_shape)):
                multi_index = np.unravel_index(raveled_i, field_shape)
                column_name = data_name
                for i in multi_index:
                    column_name = column_name + "_%d" % (int(i),)
                self.dataframe[column_name] = self.data[data_name][
                    multi_index
                ].flatten()
        if delete_data:
            del self.data[data_name]

    def grid_indices_to_dataframe(self):
        """
        Adds to self.dataframe two columns containing the grid indices of each row.
        """
        if ("nx" not in self.meta) and ("ny" not in self.meta):
            msg = "In '%s'.grid_indices_to_dataframe() -> " % (self.name,)
            msg += "'nx' and/or 'ny' not present in '%s'.meta" % (self.name,)
            print(msg, "\nReturning.")
            return
        nnx, nny = np.meshgrid(
            np.arange(self.meta["nx"], dtype=np.int32),
            np.arange(self.meta["ny"], dtype=np.int32),
        )
        self.dataframe["index_x"] = nnx.T.flatten()
        self.dataframe["index_y"] = nny.T.flatten()
        del nnx, nny

    def dataframe_to_file(self, df_file_name, df_file_path=".", sep=",", mode="w"):
        """
        Saves self.dataframe into a file, by default a CSV, but the separator can be given
        as (optional) inpunt as sep=<str>.

        INPUTS:
        df_file_name -> <str>, name of the file, e.g. 'dataframe.csv'
        df_file_path -> <str>, optional, path which will contain the file, default is '.'
        sep          -> <str>, optional, separator, default is ','
        mode         -> <str>, optional, if 'w' overwrite the file if already eisting,
                        if 'a' append to it, default is 'w'
        """
        func_name = "'%s'.dataframe_to_file()" % (self.name,)
        if type_check(df_file_name, str, "df_file_name", func_name, "Returning."):
            return
        if type_check(df_file_path, str, "df_file_path", func_name, "Returning."):
            return
        if type_check(sep, str, "sep", func_name, "Returning."):
            return
        if type_check(mode, str, "mode", func_name, "Returning."):
            return
        if mode not in ["w", "a"]:
            msg = "In %s -> mode is not 'w' or 'a'" % (func_name,)
            print(msg, "\nReturning.")
            return

        df_file_wholepath = os_path_join(df_file_path, df_file_name)  # create full path
        header = mode == "w"
        self.dataframe.to_csv(
            df_file_wholepath, header=header, index=False, sep=sep, mode=mode
        )

    def file_to_dataframe(self, df_file_name, df_file_path=".", sep=",", header=0):
        """
        Reads from a file, supposedly a *SV file (CSV, TSV, ...), and loads it
        into self.dataframe.

        INPUTS:
        df_file_name -> <str>, name of the file, e.g. 'dataframe.csv'
        df_file_path -> <str>, optional, path which will contain the file, default is '.'
        sep          -> <str>, optional, separator, default is ','
        header       -> <None|int|list>, optional, default is 0, see references to pandas.read_cvs
        """
        func_name = "'%s'.dataframe_to_file()" % (self.name,)
        if type_check(df_file_name, str, "df_file_name", func_name, "Returning."):
            return
        if type_check(df_file_path, str, "df_file_path", func_name, "Returning."):
            return
        if type_check(sep, str, "sep", func_name, "Returning."):
            return
        header_in_not_none = header is not None
        header_is_not_int = not isinstance(header, int)
        header_is_not_list = not isinstance(header, list)
        header_is_not_infer = header != "infer"
        if (
            header_in_not_none
            and header_is_not_int
            and header_is_not_list
            and header_is_not_infer
        ):
            print(
                "ERROR in %s -> header is not int or list or None or 'infer'.\nReturning."
                % (func_name,)
            )
            return

        df_file_wholepath = os_path_join(df_file_path, df_file_name)  # create full path
        self.dataframe = pd_read_csv(df_file_wholepath, sep=sep, header=header)

    def save_dataframe(self):
        """
        Creates and stores internally a copy of self.dataframe
        """
        self._saved_dataframe = self.dataframe.copy()

    def restore_dataframe(self, delete_savestate=True):
        """
        Restores the laste saved copy of dataframe (into self.dataframe itself)

        INPUTS:
        delete_savestate -> <boolean>, optional, if the saved copy should be deleted,
                            default is True
        """
        func_name = "'%s'.restore_dataframe()" % (self.name,)
        if type_check(
            delete_savestate,
            bool,
            "delete_savestate",
            func_name,
            "Retutning.",
        ):
            return

        if len(self._saved_dataframe) == 0:
            msg = "In %s -> " % (func_name,)
            msg += "no internally saved dataframe to restore."
            print(msg, "\nReturning.")
            return
        self.dataframe = self._saved_dataframe.copy()
        if delete_savestate:
            self._saved_dataframe = pd_DataFrame()
