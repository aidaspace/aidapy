"""
Functions to compute (current) structures aspect ratios

M. Sisti - 18/11/2020
F. Finelli - 27/03/2021
mail to: francesco.finelli@phd.unipi.it
"""
from os.path import join as os_path_join

import numpy as np
from scipy.spatial.distance import euclidean as spd_euclidean
from scipy.ndimage import map_coordinates as ndm_map_coordinates
from scipy.signal import find_peaks as sig_find_peaks
from scipy.signal import peak_widths as sig_peak_widths
from scipy.spatial import (
    ConvexHull as spa_ConvexHull,
)  # pylint: disable=no-name-in-module
from scipy.spatial.distance import pdist as spd_pdist

from aidapy.unsupmr.check_utils import type_check


def cut_in_box(P0, P1, x_box_range, y_box_range, is_zero_th=1.0e-16):
    """
    Given a segment from P0 to P1 and a box, defined by an x range and an y range,
    this function return the portion of the segment inside the box (returning two new extremes).
    It is assumed that at least a part of the segment is inside the box.

    INPUTS:
    P0, P1 -> [float,float], coordinates of the extremes of the original segment
    x_box_range, y_box_range -> [float,float], range of the box on the x and y directions
    is_zero_th -> float, optional, numerical thresholds to consider a value as zero

    OUTPUTS:
    P0, P1 -> [float,float], coordinates of the extremes of the new segment
    """
    x0, y0 = P0
    x1, y1 = P1
    Dx, Dy = x1 - x0, y1 - y0
    xmin, xmax = x_box_range
    ymin, ymax = y_box_range
    t_list = []
    if abs(Dx) > is_zero_th:
        t_list.append((xmin - x0) / Dx)
        t_list.append((xmax - x0) / Dx)
    if abs(Dy) > is_zero_th:
        t_list.append((ymin - y0) / Dy)
        t_list.append((ymax - y0) / Dy)
    nt = len(t_list)
    if nt == 4:
        _, t0, t1, _ = sorted(t_list)
    elif nt == 2:
        t0, t1 = sorted(t_list)
    else:
        return P0, P1
    if t0 > is_zero_th:
        P0 = [x0 + t0 * Dx, y0 + t0 * Dy]
    if (1.0 - t1) > is_zero_th:
        P1 = [x0 + t1 * Dx, y0 + t1 * Dy]
    return P0, P1


def structures_thickness(
    A,
    ij_arr,
    eig_val,
    eig_vec,
    dx,
    dy,
    n_seg_pts=1000,
    e_mult=100.0,
    periodic=True,
    if_border_cross="default",
    cval=0.0,
    max_search_coverage=0.2,
    keep_log=False,
):
    """
    Computes the thickness of the peaks of a field, given the positions of said peaks
    (local maxima). It uses eigenvectors and eigenvalues of the Hessian matrix
    computed in these points.

    INPUTS:
    A               -> numpy.ndarray of shape (nx,ny), data type float(64), the scalar field
    ij_arr          -> numpy.ndaray of shape (n,2), data type int(32), n is the number
                       of local maxima considered
    eig_val         -> numpy.ndaray of shape (2,n), data type float64
    eig_vec         -> numpy.ndaray of shape (2,2,n), data type float64
    dx, dy          -> float, grid spacing in the two directions
    n_seg_pts       -> int, optional, number of points on the interpolation segment,
                       default is 1000
    e_mult          -> float, optional, multiplier factor to generate the segment from
                       the eigenvector (P0(1) = P_max +(-) e_mult * eigenvector),
                       default is 100.0
    periodic        -> bool, if the field is periodic in both directions
    if_border_cross -> protocol to follow if the segment crosses the box border,
                       default is "default", possible options:
                    - reflect: d c b a | a b c d | d c b a
                    - grid-mirror: as "reflect"
                    - constant: k k k k | a b c d | k k k k w/o interpolation beyond border
                    - grid-constant: k k k k | a b c d | k k k k w/ interpolation beyond border
                    - nearest: a a a a | a b c d | d d d d
                    - mirror: d c b | a b c d | c b a
                    - grid-wrap: a b c d | a b c d | a b c d
                    - wrap: d b c d | a b c d | b c a b
                    - default: "grid-wrap" if periodic == True else "cut-segment"
                    - return-nan: returns numpy.nan as thickness value
                    - cut-segment: cut the segment keeping only the portion inside the box
    cval            -> float, optional, value to fill past edges of input if mode is "constant"
                       or "grid-constant", default is 0.
    max_search_coverage -> float, optional, percentage of the segment where (centered
                           on the middle) to search the max for computing the half height
                           of the peak (used only if only one peak is found), default is 0.2
    keep_log        -> bool, optional, if keeping a log of the process, default is False

    OUTPUTS:
    FWHM_thickness -> numpy.ndarray of shape (#maxima), datatype float64, the thickness
                     of the different structures; returns -1 if an error is raised
    """
    func_name = "structures_thickness"
    if type_check(A, np.ndarray, "A", func_name, "Returning."):
        return -1
    if type_check(ij_arr, np.ndarray, "ij_arr", func_name, "Returning."):
        return -1
    if type_check(eig_val, np.ndarray, "eig_val", func_name, "Returning."):
        return -1
    if type_check(eig_vec, np.ndarray, "eig_vec", func_name, "Returning."):
        return -1
    if type_check(dx, float, "dx", func_name, "Returning."):
        return -1
    if type_check(dy, float, "dy", func_name, "Returning."):
        return -1
    if type_check(n_seg_pts, int, "n_seg_pts", func_name, "Returning."):
        return -1
    if type_check(e_mult, float, "e_mult", func_name, "Returning."):
        return -1
    if type_check(periodic, bool, "periodic", func_name, "Returning."):
        return -1
    if type_check(if_border_cross, str, "if_border_cross", func_name, "Returning."):
        return -1
    if type_check(cval, float, "cval", func_name, "Returning."):
        return -1
    if type_check(
        max_search_coverage, float, "max_search_coverage", func_name, "Returning."
    ):
        return -1
    if type_check(keep_log, bool, "keep_log", func_name, "Returning."):
        return -1

    # defining useful variables
    modes_list = [
        "reflect",
        "grid-mirror",
        "constant",
        "grid-constant",
        "nearest",
        "mirror",
        "grid-wrap",
        "wrap",
        "default",
        "return-nan",
        "cut-segment",
    ]
    npts = ij_arr.shape[0]
    nx, ny = A.shape
    abs_eig_val = np.abs(eig_val)

    # checking what to do if a segment crosses the box border (depending on if periodic or not)
    if if_border_cross not in modes_list:
        msg = "\nWARNING from structures_thickness: if_border_cross value not recognized, "
        msg += (
            "falling back to 'default' (if periodic 'grid-wrap', else 'cut-segment').\n"
        )
        print(msg)
        if_border_cross = "default"
    if if_border_cross == "default":
        if periodic:
            if_border_cross = "grid-wrap"
        else:
            if_border_cross = "cut-segment"
    if if_border_cross in ["return-nan", "cut-segment"]:
        ip_mode = "nearest"
    else:
        ip_mode = if_border_cross

    # logging (for inspection/debugging)
    if keep_log:
        with open("structures_thickness.log", "w") as f_log:
            f_log.write("n\txin\txfin\tyin\tyfin\tn_seg_pts\t")
            f_log.write(
                "A_ip_max[0:n_seg_pts]\tdx\tdy\thalf\tthickness\tmax_position\n"
            )

    # computing the thicknesses
    ind_off = int(n_seg_pts * max_search_coverage) // 2
    FWHM_thickness = np.empty((npts,), dtype=np.float64)
    for n in range(npts):
        # Finding the index of the biggest eigenvalue, and finding the respective eigenvector.
        # The biggest eigenvalue is related to the direction perpendicular to the direction
        # of strongest variation, thus to the "thickness"
        index_max = np.argmax(abs_eig_val[:, n])
        e_max = eig_vec[:, index_max, n]

        # Defining the segment over which we want to interpolate
        xin = (
            float(ij_arr[n, 0]) - e_mult * e_max[0]
        )  # xin, xfin, yin, and yfin are the extremes
        xfin = (
            float(ij_arr[n, 0]) + e_mult * e_max[0]
        )  # of the "interpolated grid points",
        yin = float(ij_arr[n, 1]) - e_mult * e_max[1]  # thus are floats as coords_min
        yfin = float(ij_arr[n, 1]) + e_mult * e_max[1]
        if not (
            (0.0 < xin < float(nx))
            and (0.0 < xfin < float(nx))
            and (0.0 < yin < float(ny))
            and (0.0 < yfin < float(ny))
        ):
            if if_border_cross == "return-nan":
                FWHM_thickness[n] = np.nan
                continue
            if if_border_cross == "cut-segment":
                [xin, yin], [xfin, yfin] = cut_in_box(
                    [xin, yin], [xfin, yfin], [0.0, float(nx - 1)], [0.0, float(ny - 1)]
                )
        dist_max = spd_euclidean([xin * dx, yin * dy], [xfin * dx, yfin * dy])

        # interpolation
        coords_x_max = np.linspace(xin, xfin, n_seg_pts, dtype=np.float64) % nx
        coords_y_max = np.linspace(yin, yfin, n_seg_pts, dtype=np.float64) % ny
        A_ip_max = ndm_map_coordinates(
            A, np.vstack((coords_x_max, coords_y_max)), mode=ip_mode, cval=cval
        )
        del coords_x_max, coords_y_max

        # computing full width half maximum for thickness
        ds = dist_max / float(n_seg_pts - 1)
        peaks, _ = sig_find_peaks(A_ip_max)
        if peaks.size > 1:
            fwhm, hight, _, _ = sig_peak_widths(A_ip_max, peaks)
            fwhm = fwhm * ds  # conversion in box unit length
            ind_peak = np.argmin(np.abs(peaks - float(n_seg_pts) * 0.5))
            FWHM_thickness[n] = fwhm[ind_peak]
            if keep_log:
                max_position = peaks[ind_peak]
                half = hight[ind_peak] * 0.5 + np.min(A_ip_max)
        else:
            i0_search = max(n_seg_pts // 2 - ind_off, 0)
            i1_search = min(n_seg_pts // 2 + ind_off, n_seg_pts - 1)
            half = (
                np.max(A_ip_max[i0_search:i1_search]) - np.min(A_ip_max)
            ) * 0.5 + np.min(A_ip_max)
            tmp_max = np.max(A_ip_max[i0_search:i1_search])
            max_position = np.where(A_ip_max == tmp_max)[0][0]
            first_half = A_ip_max[:max_position]
            second_half = A_ip_max[max_position:]
            ind_1 = np.argmin(np.abs(first_half - half))
            ind_2 = max_position + np.argmin(np.abs(second_half - half))
            FWHM_thickness[n] = float(abs(ind_2 - ind_1)) * ds

        # logging (for inspection/debugging)
        if keep_log:
            with open("structures_thickness.log", "a") as f_log:
                f_log.write(
                    "%d\t%f\t%f\t%f\t%f\t%d\t" % (n, xin, xfin, yin, yfin, n_seg_pts)
                )
                for i in range(n_seg_pts):
                    f_log.write("%f\t" % (A_ip_max[i],))
                f_log.write(
                    "%f\t%f\t%f\t%f\t%d\n"
                    % (dx, dy, half, FWHM_thickness[n], max_position)
                )
    del abs_eig_val

    return FWHM_thickness


def structures_width(
    mask,
    dx,
    dy,
    periodic=True,
    if_0_pts=np.nan,
    if_1_pts=0.0,
    if_2_pts=None,
    max_size_for_hull=np.inf,
):
    """
    Computes the width of the peaks of a field, given the positions of said peaks
    (local maxima).

    INPUTS:
    mask     -> numpy.ndarray of shape (nx,ny), data type int, mask containing the
                different regions
    dx, dy   -> float, grid spacing in the two directions
    periodic -> bool, if the field is periodic in both directions
    if_0_pts -> float, optional, width if the region has 0 points, default is numpy.nan
    if_1_pts -> float, optional, width if the region has 1 points, default is 0.
    if_2_pts -> float or None, optional, width if the region has 2 points,
                if None the distance between the two points is used, default is None

    OUTPUTS:
    width -> numpy.ndarray of shape (#maxima), datatype float64, the width
             of the different structures; returns -1 if an error is raised
    """
    func_name = "structures_width"
    if type_check(mask, np.ndarray, "mask", func_name, "Returning."):
        return -1
    if type_check(dx, float, "dx", func_name, "Returning."):
        return -1
    if type_check(dy, float, "dy", func_name, "Returning."):
        return -1
    if type_check(periodic, bool, "periodic", func_name, "Returning."):
        return -1
    if type_check(if_0_pts, float, "if_0_pts", func_name, "Returning."):
        return -1
    if type_check(if_1_pts, float, "if_1_pts", func_name, "Returning."):
        return -1
    if if_2_pts is not None:
        if type_check(
            if_2_pts, float, "if_2_pts", func_name, "Nor it is None. Returning."
        ):
            return -1
    if not isinstance(max_size_for_hull, float):
        if type_check(
            max_size_for_hull,
            int,
            "max_size_for_hull",
            func_name,
            "Nor it is float. Returning.",
        ):
            return -1

    mask_values = list(np.unique(mask))
    npts = len(mask_values) - 1
    nx, ny = mask.shape
    #
    width = np.empty((npts,), dtype=np.float64)
    for n, im in enumerate(mask_values[1:]):
        points_x, points_y = np.where(mask == im)
        if (
            periodic
        ):  # WARNING:fails if the structure has dimensions bigger than the box ones
            pos_unique = np.unique(points_x)
            if (0 in pos_unique) and (nx - 1 in pos_unique):
                points_x[points_x < nx // 2] += nx
            pos_unique = np.unique(points_y)
            if (0 in pos_unique) and (ny - 1 in pos_unique):
                points_y[points_y < ny // 2] += ny
            del pos_unique
        size = points_x.size
        if size > max_size_for_hull:
            width[n] = np.sqrt(
                ((np.max(points_x) - np.min(points_x)) * dx) ** 2
                + ((np.max(points_y) - np.min(points_y)) * dy) ** 2
            )
        elif size > 2:
            hull = spa_ConvexHull(np.array([points_x, points_y]).T)
            width[n] = np.max(
                spd_pdist(np.array([points_x * dx, points_y * dy]).T[hull.vertices])
            )
            del hull
        elif size == 2:
            if isinstance(if_2_pts, float):
                width[n] = if_2_pts
            else:
                width[n] = np.sqrt(
                    ((points_x[1] - points_x[0]) * dx) ** 2
                    + ((points_y[1] - points_y[0]) * dy) ** 2
                )
        elif size == 1:
            width[n] = if_1_pts
        else:
            width[n] = if_0_pts

    return width


def compute_aspect_ratio(width, thickness):
    """
    Computes aspect ratios (long dimension over short dimension) of the difefrent regions.

    INPUTS:
    width     -> numpy.ndarray of shape (#maxima), datatype float64, the width
                 of the different structures
    thickness -> numpy.ndarray of shape (#maxima), datatype float64, the thickness
                 of the different structures

    OUTPUTS:
    a_r -> numpy.ndarray of shape (#maxima), datatype float64, the aspect ratios
           of the different structures; returns -1 if an error is raised
    """
    func_name = "compute_aspect_ratio"
    if type_check(width, np.ndarray, "width", func_name, "Returning."):
        return -1
    if type_check(thickness, np.ndarray, "thickness", func_name, "Returning."):
        return -1

    a_r = np.divide(width, thickness)
    a_r[a_r < 1.0] = np.divide(1.0, a_r[a_r < 1.0])  # correction necessary when
    #                                                  the cluster is almost circular
    #                                                  and width~thickness
    return a_r


def save_aspect_ratios(a_r, ar_file_name, ar_file_path=".", ar_file_mode="w"):
    """
    Saves on file the aspect ratios array

    INPUTS:
    a_r -> numpy.ndarray of shape (#maxima), datatype float64, the aspect ratios
           of the different structures
    ar_file_name -> string, name of the file, e.g. 'aspect_ratio.dat'
    ar_file_path -> string, optional, path which will contain the file, default is '.'
    ar_file_mode -> string, optional, if 'w' overwrite the file if already eisting,
                    if 'a' append to it, default is 'w'
    """
    func_name = "save_aspect_ratio"
    if type_check(a_r, np.ndarray, "a_r", func_name, "Returning."):
        return
    if type_check(ar_file_name, str, "ar_file_name", func_name, "Returning."):
        return
    if type_check(ar_file_path, str, "ar_file_path", func_name, "Returning."):
        return
    if type_check(ar_file_mode, str, "ar_file_mode", func_name, "Returning."):
        return

    with open(os_path_join(ar_file_path, ar_file_name), ar_file_mode) as f_ar:
        f_ar.write("aspect_ratio\n")
        for val in a_r:
            f_ar.write("%s\n" % (str(val),))


def load_aspect_ratios(ar_file_name, ar_file_path="."):
    """
    Loads from file the aspect ratios

    INPUTS:
    ar_file_name -> string, name of the file, e.g. 'aspect_ratio.dat'
    ar_file_path -> string, optional, path which will contain the file, default is '.'

    OUTPUTS:
    a_r -> numpy.ndarray of shape (#maxima), datatype float64, the aspect ratios
           of the different structures; returns -1 if an error is raised
    """
    func_name = "load_aspect_ratio"
    if type_check(ar_file_name, str, "ar_file_name", func_name, "Returning."):
        return -1
    if type_check(ar_file_path, str, "ar_file_path", func_name, "Returning."):
        return -1

    with open(os_path_join(ar_file_path, ar_file_name), "r") as f_ar:
        lines = f_ar.readlines()[1:]

    a_r = np.empty((len(lines),), dtype=np.float64)
    for i, line in enumerate(lines):
        a_r[i] = float(line)

    return a_r
