"""
Applying DBscan to one selected cluster, among those found using KMeans algorithm;
"lab_rec" refers to the label of the chosen cluster among those found by KMeans.

M. Sisti, F. Finelli - 18/11/2020
F. Finelli - 7/04/2021
e-mail: manuela.sisti@univ-amu.fr, francesco.finelli@phd.unipi.it
"""
from os.path import join as os_path_join

from numpy import unique as np_unique
from numpy import empty as np_empty
from numpy import int32 as np_int32
from numpy import ndarray as np_ndarray
from pandas import DataFrame as pd_DataFrame
from sklearn.cluster import DBSCAN as skc_DBSCAN

from aidapy.unsupmr.check_utils import type_check, meta_check

def dbscan_on_cluster(df, meta, return_mask=True):
    """
    Uses DBSCAN algorithm to subdivide the "reconnections" cluster into
    different regions "isolated" in space.

    INPUTS:
    df -> pandas.DataFrame with at least "label", "index_x", and "index_y" as columns
    meta -> dict of metadata
    return_mask -> bool, optional, if the mask should be returned also as array,
                   default is True

    OUTPUTS:
    mask -> numpy.ndarray of shape (nx,ny), data type int, mask of the box
            where each region point is set to the relative region label
            (0 is the background); returned only if return_mask=True,
            however it will be added (flatten) to df under the column name "region",
            if return_mask=False, 0 is returned; -1 is returned if an ERROR is raised
    """
    func_name = "dbscan_on_cluster"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return -1
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return -1
    if type_check(return_mask, bool, "return_mask", func_name, "Returning."):
        return -1

    if meta_check("lab_rec", meta, func_name):
        return -1
    lab_rec = meta["lab_rec"]

    # selecting the 'reconnection' cluster and keeping only grid indices
    df_tmp = df.loc[df["label"] == lab_rec, ["index_x", "index_y"]]

    if meta_check("DB_eps", meta, func_name):
        return -1
    DB_eps = meta["DB_eps"]

    if meta_check("DB_min_samples", meta, func_name):
        return -1
    DB_min_samples = meta["DB_min_samples"]

    # apply DBscan
    connected_regions = skc_DBSCAN(eps=DB_eps, min_samples=DB_min_samples).fit(df_tmp)

    # merge region labels into df
    df_tmp["region"] = connected_regions.labels_ + 1
    del connected_regions
    df.loc[:, "region"] = df_tmp.loc[:, "region"]
    del df_tmp
    df["region"] = (df["region"].fillna(0)).astype(int)

    # make regions mask
    if meta_check("nx", meta, func_name):
        return -1
    nx = meta["nx"]

    if meta_check("ny", meta, func_name):
        return -1
    ny = meta["ny"]

    mask = df.loc[:, "region"].values.reshape(nx, ny)

    # apply periodicity (if periodic)
    if meta_check("periodic", meta, func_name):
        return -1
    periodic = meta["periodic"]

    if periodic:
        for i_x in range(nx):
            val0 = mask[i_x, 0]
            if val0 == 0:
                continue
            val1 = mask[i_x, -1]
            if val1 == 0:
                continue
            if val1 == val0:
                continue
            mask[mask == val1] = val0

        for i_y in range(ny):
            val0 = mask[0, i_y]
            if val0 == 0:
                continue
            val1 = mask[-1, i_y]
            if val1 == 0:
                continue
            if val1 == val0:
                continue
            mask[mask == val1] = val0

    # eliminating "gaps" in region labels
    new_region = 0
    for old_region in np_unique(mask)[1:]:
        new_region += 1
        if old_region == new_region:
            continue
        mask[mask == old_region] = new_region

    # updating dataframe
    df["region"] = mask.flatten()

    # retutning the mask
    if return_mask:
        return mask
    return 0


def indices_of_regions_maxima(df, column_name):
    """
    Computes positions of local maxima of the field (flattened) in df under the
    column name given by column_name. These maxima will be computed only inside the regions
    defined by the DBSCAN (one maximum per region).

    INPUTS:
    df          -> pandas.DataFrame with at least "region", "index_x", "index_y", and
                   column_name as colums
    column_name -> string, name of the column containing the feld of interest

    OUTPUTS:
    max_ind -> numpy.ndarray of shape (#regions, 2), data type is int32,
               array of pairs of grid indices of the maxima found;
               -1 is returned if an error is raised
    """
    func_name = "indices_of_regions_maxima"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return -1
    if type_check(column_name, str, "column_name", func_name, "Returning."):
        return -1

    regions = np_unique(df["region"])[1:]
    max_ind = np_empty((regions.size, 2), dtype=np_int32)
    for i, region in enumerate(regions):
        max_val = df.loc[df["region"] == region, column_name].max()
        max_ind[i, :] = df.loc[
            (df["region"] == region) & (df[column_name] == max_val),
            ["index_x", "index_y"],
        ].values[0]
    return max_ind


def save_regions_maxima(max_ind, max_file_name, max_file_path=".", max_file_mode="w"):
    """
    Saves on file the indices of maxima found

    INPUTS:
    max_ind       -> numpy.ndarray of shape (#maxima, 2), datatype int32, the maxima position
                     of the different regions
    max_file_name -> string, name of the file, e.g. 'max_ind.dat'
    max_file_path -> string, optional, path which will contain the file, default is '.'
    max_file_mode -> string, optional, if 'w' overwrite the file if already eisting,
                     if 'a' append to it, default is 'w'
    """
    func_name = "save_regions_maxima"
    if type_check(max_ind, np_ndarray, "max_ind", func_name, "Returning."):
        return
    if type_check(max_file_name, str, "max_file_name", func_name, "Returning."):
        return
    if type_check(max_file_path, str, "max_file_path", func_name, "Returning."):
        return
    if type_check(max_file_mode, str, "max_file_mode", func_name, "Returning."):
        return

    with open(os_path_join(max_file_path, max_file_name), max_file_mode) as f_max:
        f_max.write("index_x\tindex_y\n")
        for indices in max_ind:
            f_max.write("%d\t%d\n" % (indices[0], indices[1]))


def load_regions_maxima(max_file_name, max_file_path="."):
    """
    Loads from file the indices of maxima found

    INPUTS:
    max_file_name -> string, name of the file, e.g. 'max_ind.dat'
    max_file_path -> string, optional, path which will contain the file, default is '.'

    OUTPUTS:
    max_ind -> numpy.ndarray of shape (#maxima, 2), datatype int32, the maxima position
               of the different regions; -1 is returned if an error is raised
    """
    func_name = "load_regions_maxima"
    if type_check(max_file_name, str, "max_file_name", func_name, "Returning."):
        return -1
    if type_check(max_file_path, str, "max_file_path", func_name, "Returning."):
        return -1

    with open(os_path_join(max_file_path, max_file_name), "r") as f_max:
        lines = f_max.readlines()[1:]

    max_ind = np_empty((len(lines), 2), dtype=np_int32)
    for i, line in enumerate(lines):
        indices = list(map(int, line.split()))
        max_ind[i, 0] = indices[0]
        max_ind[i, 1] = indices[1]

    return max_ind
