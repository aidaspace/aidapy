"""
Some useful function to make plots

F. Finelli - 18/11/2020
F. Finelli - 27/03/2021
e-mail: francesco.finelli@phd.unipi.it
"""
from os.path import join as os_path_join

import numpy as np
from pandas import DataFrame as pd_DataFrame
import matplotlib as mpl
import matplotlib.pyplot as plt
from seaborn import heatmap as sns_heatmap

from aidapy.unsupmr.check_utils import type_check, meta_check


def plot_field(
    field,
    meta,
    show_flg=True,
    save_flg=True,
    file_name="field.png",
    file_dir=".",
    xlabel="x",
    ylabel="y",
    field_name="Field",
    cmap="viridis",
):
    """
    Plots a field using contourf.
    Scalar field -> 1 subplot
    Vector field -> a row of (typically 3) subplots
    2-tensor fiels -> a matrix (typically 3x3) of subplots

    INPUTS:
    field -> numpy.ndarray with shape (nx, ny) or (n, nx, ny) or (n, m, nx, ny)
    meta  -> dict of metadata
    show_flg -> bool, optional, if the plot should be shown, default is True
    save_flg -> bool, optional, if the plot should be saved, default is True
    file_name -> string, optionale, name of the file if plot is saved, default is "field.png"
    file_dir -> string, optional, where to dave the plot, default is "."
    xlabel -> string, optional, label of x axis, default is "x"
    ylabel -> string, optional, label of y axis, default is "y"
    field_name -> string, optional, name of the field to be used in titles, default is "Field"
    cmap -> string or colormap, colormap to be used, default is "viridis"
    """
    func_name = "plot_field"
    if type_check(field, np.ndarray, "field", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(show_flg, bool, "show_flg", func_name, "Returning."):
        return
    if type_check(save_flg, bool, "save_flg", func_name, "Returning."):
        return
    if type_check(file_name, str, "file_name", func_name, "Returning."):
        return
    if type_check(file_dir, str, "file_dir", func_name, "Returning."):
        return
    if type_check(xlabel, str, "xlabel", func_name, "Returning."):
        return
    if type_check(ylabel, str, "ylabel", func_name, "Returning."):
        return
    if type_check(field_name, str, "field_name", func_name, "Returning."):
        return
    if not isinstance(cmap, mpl.colors.Colormap):
        if type_check(
            cmap,
            str,
            "cmap",
            func_name,
            "Nor it is a matplotlib.colors.Colormap. Returning.",
        ):
            return

    if ("x" in meta) and ("y" in meta):
        x, y = meta["x"], meta["y"]
    else:
        if ("nx" in meta) and ("ny" in meta):
            nx, ny = meta["nx"], meta["ny"]
        else:
            nx, ny = field.shape[-2:]
        x, y = np.arange(nx), np.arange(ny)

    field_shape = field.shape[:-2]
    field_dims = len(field_shape)
    if field_dims == 0:
        npr = 1
        npc = 1
    elif field_dims == 1:
        npr = 1
        npc = field_shape[0]
    elif field_dims == 2:
        npr = field_shape[1]
        npc = field_shape[0]
    else:
        msg = "ERROR in plot_field -> field shape %s" % (str(field_shape),)
        msg += " not compatible with scalar, vector, or 2-tensor field!"
        print(msg, "\nReturnig")
        return

    fig, ax = plt.subplots(npr, npc, figsize=(8 * npc, 5 * npr))

    if field_dims == 0:
        i_m = ax.contourf(x, y, field.T, 63, cmap=cmap)
        plt.colorbar(i_m, ax=ax)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(field_name)
    else:
        for raveled_i in range(np.prod(field_shape)):
            multi_index = np.unravel_index(raveled_i, field_shape)
            title = field_name
            for i in multi_index:
                title = title + "_%d" % (int(i),)
            ax_ = ax[multi_index]
            i_m = ax_.contourf(x, y, field[multi_index].T, 63)
            plt.colorbar(i_m, ax=ax_)
            ax_.set_xlabel(xlabel)
            ax_.set_ylabel(ylabel)
            ax_.set_title(title)

    plt.tight_layout()
    if show_flg:
        plt.show()
    if save_flg:
        fig.savefig(os_path_join(file_dir, file_name))

    plt.close()


def correlation_matrix(
    df, show_flg=True, save_flg=True, file_name="correlation_matrix.png", file_dir="."
):
    """
    Plots the correlation matrix of the columns of a dataframe

    INPUTS:
    df -> pandas.DataFrame, not empty
    show_flg -> bool, optional, if the plot should be shown, default is True
    save_flg -> bool, optional, if the plot should be saved, default is True
    file_name -> string, optionale, name of the file if plot is saved,
                 default is "correlation_matrix.png"
    file_dir -> string, optional, where to dave the plot, default is "."
    """
    func_name = "correlation_matrix"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if type_check(show_flg, bool, "show_flg", func_name, "Returning."):
        return
    if type_check(save_flg, bool, "save_flg", func_name, "Returning."):
        return
    if type_check(file_name, str, "file_name", func_name, "Returning."):
        return
    if type_check(file_dir, str, "file_dir", func_name, "Returning."):
        return

    corr = df.corr()

    fig, ax = plt.subplots(figsize=(15, 8))
    _ = sns_heatmap(
        corr,
        ax=ax,
        cmap="coolwarm",
        vmin=-1.0,
        vmax=1.0,
        annot=True,
        fmt=".2f",
        annot_kws={"size": 15},
        linewidths=0.5,
    )
    ax.set_title("Correlation matrix")

    if show_flg:
        plt.show()
    if save_flg:
        fig.savefig(os_path_join(file_dir, file_name))

    plt.close()


def plot_scores(
    CV_df_norm,
    meta,
    show_flg=True,
    save_flg=True,
    file_name="crossvalidation.png",
    file_dir=".",
):
    """
    Plots the results of the KMeans crossvalidation

    INPUTS:
    CV_df_norm -> pandas.DataFrame, result of KM_crossvalidation
    meta       -> dict of metadata
    show_flg   -> bool, optional, if the plot should be shown, default is True
    save_flg   -> bool, optional, if the plot should be saved, default is True
    file_name  -> string, optionale, name of the file if plot is saved,
                  default is "crossvalidation.png"
    file_dir -> string, optional, where to dave the plot, default is "."
    """
    func_name = "plot_scores"
    if type_check(CV_df_norm, pd_DataFrame, "CV_df_norm", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(show_flg, bool, "show_flg", func_name, "Returning."):
        return
    if type_check(save_flg, bool, "save_flg", func_name, "Returning."):
        return
    if type_check(file_name, str, "file_name", func_name, "Returning."):
        return
    if type_check(file_dir, str, "file_dir", func_name, "Returning."):
        return

    if meta_check("algs", meta, func_name, impose_list=True):
        return
    algs = meta["algs"]
    n_algs = len(algs)
    if meta_check("scores", meta, func_name, impose_list=True):
        return
    scores = meta["scores"]
    fig, ax = plt.subplots(1, n_algs + 1, figsize=(5 * (n_algs + 1), 5))

    for alg in algs:
        ax[0].plot(
            CV_df_norm[CV_df_norm["alg"] == alg]["k"],
            CV_df_norm.loc[CV_df_norm["alg"] == alg, "time"],
            marker="o",
            label=alg,
        )
    ax[0].set_xlabel("Number of clusters")
    ax[0].set_ylabel("Time to fit")
    ax[0].set_title("Crossvalidation - timing")
    ax[0].legend()
    ax[0].grid(b=None, axis="y")

    i = 1
    for alg in algs:
        for scr in ["inertia"] + scores:
            ax[i].plot(
                CV_df_norm[CV_df_norm["alg"] == alg]["k"],
                CV_df_norm.loc[CV_df_norm["alg"] == alg, scr],
                marker="o",
                label=scr,
            )
        ax[i].set_xlabel("Number of clusters")
        ax[i].set_ylabel("Score")
        ax[i].set_title("Algorithm: %s" % (alg))
        ax[i].legend()
        ax[i].grid(b=None, axis="y")
        i += 1

    plt.tight_layout()

    if show_flg:
        plt.show()
    if save_flg:
        fig.savefig(os_path_join(file_dir, file_name))

    plt.close()


def plot_clusters(
    df,
    meta,
    show_flg=True,
    save_flg=True,
    file_name="clusters_in_2D.png",
    file_dir=".",
    figsize=(12, 12),
    xlabel="x",
    ylabel="y",
    base_cmap="viridis",
):
    """
    Plots cluster labels from KMeans clusterizzation

    INPUTS:
    df -> pandas.DataFrame, with at least "label" as column
    meta  -> dict of metadata
    show_flg -> bool, optional, if the plot should be shown, default is True
    save_flg -> bool, optional, if the plot should be saved, default is True
    file_name -> string, optionale, name of the file if plot is saved,
                 default is "clusters_in_2D.png"
    file_dir -> string, optional, where to dave the plot, default is "."
    figsize -> (int,int), optional, size of figure, default is (12, 12)
    xlabel -> string, optional, label of x axis, default is "x"
    ylabel -> string, optional, label of y axis, default is "y"
    base_cmap -> string or colormap, colormap to be used as base, default is "viridis"
    """
    func_name = "plot_clusters"
    if type_check(df, pd_DataFrame, "df", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(show_flg, bool, "show_flg", func_name, "Returning."):
        return
    if type_check(save_flg, bool, "save_flg", func_name, "Returning."):
        return
    if type_check(file_name, str, "file_name", func_name, "Returning."):
        return
    if type_check(file_dir, str, "file_dir", func_name, "Returning."):
        return
    if type_check(figsize, tuple, "figsize", func_name, "Returning."):
        return
    if type_check(xlabel, str, "xlabel", func_name, "Returning."):
        return
    if type_check(ylabel, str, "ylabel", func_name, "Returning."):
        return
    if not isinstance(base_cmap, mpl.colors.Colormap):
        if type_check(
            base_cmap,
            str,
            "cmap",
            func_name,
            "Nor it is a matplotlib.colors.Colormap. Returning.",
        ):
            return

    # retrieve 2D array shape
    if ("nx" in meta) and ("ny" in meta):
        nx = meta["nx"]
        ny = meta["ny"]
        ix_min = 0
        ix_max = nx - 1
        iy_min = 0
        iy_max = ny - 1
    elif ("index_x" in df) and ("index_y" in df):
        ix_min = df["index_x"].min()
        ix_max = df["index_x"].max()
        iy_min = df["index_y"].min()
        iy_max = df["index_y"].max()
        nx = ix_max - ix_min + 1
        ny = iy_max - iy_min + 1
    else:
        msg = "ERROR if plot_clusters -> unable to retrieve nx and ny"
        print(msg, "\nReturning")
        return

    # labels
    labels = np.unique(df["label"].values)
    n_lab = len(labels)
    lmin = labels[0]
    lmax = labels[-1]
    if meta_check("lab_rec", meta, func_name, "None"):
        return
    lab_rec = meta["lab_rec"]

    # 2D clusters label array
    clust2d = df["label"].values.reshape(nx, ny)

    # axis arrays
    if ("x" in meta) and ("y" in meta):
        x = meta["x"]
        y = meta["y"]
        pcmX = np.empty(nx + 1, dtype=np.float64)
        pcmY = np.empty(ny + 1, dtype=np.float64)
        if ("dx" in meta) and ("dy" in meta):
            dx = meta["dx"]
            dy = meta["dy"]
        else:
            dx = x[1] - x[0]
            dy = y[1] - y[0]
        pcmX[:-1] = x - 0.5 * dx
        pcmX[-1] = x[-1] + 0.5 * dx
        pcmY[:-1] = y - 0.5 * dy
        pcmY[-1] = y[-1] + 0.5 * dy
    else:
        pcmX = np.linspace(
            float(ix_min) - 0.5, float(ix_max) + 0.5, nx + 1, dtype=np.float64
        )
        pcmY = np.linspace(
            float(iy_min) - 0.5, float(iy_max) + 0.5, ny + 1, dtype=np.float64
        )

    # modified colormap
    if lab_rec == "None":
        cmap = base_cmap
    else:
        cmap = mpl.cm.get_cmap(base_cmap, n_lab)
        newcolors = cmap(np.linspace(0, 1, n_lab))
        pink = np.array([248.0 / 256.0, 24.0 / 256.0, 148.0 / 256.0, 1.0])
        lrec_idx = np.argmin(np.abs(labels - lab_rec))
        newcolors[lrec_idx : lrec_idx + 1, :] = pink
        cmap = mpl.colors.ListedColormap(newcolors)

        bnd = np.empty(n_lab + 1, dtype=np.float32)
        bnd[1:-1] = (labels[1:] + labels[:-1]) / 2.0
        bnd[0] = 2 * lmin - bnd[1]
        bnd[-1] = 2 * lmax - bnd[-2]
        norm = mpl.colors.BoundaryNorm(bnd, cmap.N)

    # plotting
    fig, ax = plt.subplots(1, 1, figsize=figsize)

    i_m = ax.pcolormesh(pcmX, pcmY, clust2d.T, cmap=cmap, norm=norm)
    cbar = plt.colorbar(i_m, ax=ax, ticks=(bnd[1:] + bnd[:-1]) / 2.0, label="labels")
    cbar.ax.set_yticklabels(labels.astype(np.str))

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    title = "Clusters in spatial domain"
    if lab_rec is not None:
        title += ' (pink is the "reconnections" one)'
    ax.set_title(title)
    plt.tight_layout()

    if show_flg:
        plt.show()
    if save_flg:
        fig.savefig(os_path_join(file_dir, file_name))

    plt.close()


def plot_regions_mask(
    mask,
    meta,
    show_flg=True,
    save_flg=True,
    file_name="regions_mask.png",
    file_dir=".",
    xlabel="x",
    ylabel="y",
    title="Mask of regions",
    figsize=(12, 12),
    cmap="tab20",
    colorbar=False,
):
    """
    Plots region labels from DBSCAN

    INPUTS:
    mask -> numpy.ndarray of shape (nx,ny), datatype is int
    meta  -> dict of metadata
    show_flg -> bool, optional, if the plot should be shown, default is True
    save_flg -> bool, optional, if the plot should be saved, default is True
    file_name -> string, optionale, name of the file if plot is saved,
                 default is "regions_mask.png"
    file_dir -> string, optional, where to dave the plot, default is "."
    xlabel -> string, optional, label of x axis, default is "x"
    ylabel -> string, optional, label of y axis, default is "y"
    title -> string, optional, title of the plot, default is "Mask of regions"
    figsize -> (int,int), optional, size of figure, default is (12, 12)
    cmap -> string or colormap, colormap to be used, default is "tab20"
    colorbar -> bool, optional, if colorbar should be shown, default is False
    """
    func_name = "plot_regions_mask"
    if type_check(mask, np.ndarray, "mask", func_name, "Returning."):
        return
    if type_check(meta, dict, "meta", func_name, "Returning."):
        return
    if type_check(show_flg, bool, "show_flg", func_name, "Returning."):
        return
    if type_check(save_flg, bool, "save_flg", func_name, "Returning."):
        return
    if type_check(file_name, str, "file_name", func_name, "Returning."):
        return
    if type_check(file_dir, str, "file_dir", func_name, "Returning."):
        return
    if type_check(xlabel, str, "xlabel", func_name, "Returning."):
        return
    if type_check(ylabel, str, "ylabel", func_name, "Returning."):
        return
    if type_check(title, str, "title", func_name, "Returning."):
        return
    if type_check(figsize, tuple, "figsize", func_name, "Returning."):
        return
    if not isinstance(cmap, mpl.colors.Colormap):
        if type_check(
            cmap,
            str,
            "cmap",
            func_name,
            "Nor it is a matplotlib.colors.Colormap. Returning.",
        ):
            return
    if type_check(colorbar, bool, "colorbar", func_name, "Returning."):
        return

    # retrieve 2D array shape
    if ("nx" in meta) and ("ny" in meta):
        nx = meta["nx"]
        ny = meta["ny"]
    else:
        nx, ny = mask.shape

    ix_min = 0
    ix_max = nx - 1
    iy_min = 0
    iy_max = ny - 1

    # axis arrays
    if ("x" in meta) and ("y" in meta):
        x = meta["x"]
        y = meta["y"]
        pcmX = np.empty(nx + 1, dtype=np.float64)
        pcmY = np.empty(ny + 1, dtype=np.float64)
        if ("dx" in meta) and ("dy" in meta):
            dx = meta["dx"]
            dy = meta["dy"]
        else:
            dx = x[1] - x[0]
            dy = y[1] - y[0]
        pcmX[:-1] = x - 0.5 * dx
        pcmX[-1] = x[-1] + 0.5 * dx
        pcmY[:-1] = y - 0.5 * dy
        pcmY[-1] = y[-1] + 0.5 * dy
    else:
        pcmX = np.linspace(
            float(ix_min) - 0.5, float(ix_max) + 0.5, nx + 1, dtype=np.float64
        )
        pcmY = np.linspace(
            float(iy_min) - 0.5, float(iy_max) + 0.5, ny + 1, dtype=np.float64
        )

    # make the plot
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    i_m = ax.pcolormesh(pcmX, pcmY, np.ma.masked_array(mask, mask == 0).T, cmap=cmap)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    if colorbar:
        plt.colorbar(i_m, ax=ax)
    plt.tight_layout()

    if show_flg:
        plt.show()
    if save_flg:
        fig.savefig(os_path_join(file_dir, file_name))

    plt.close()
