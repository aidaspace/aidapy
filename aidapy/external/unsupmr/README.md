# UMaReRe2D

Recognition of Magnetic Reconnection structures in 2D dataset, via Unsupervised ML techniques (in particular using KMeans and DBscan).
The codes and their instructions referring to a journal article titled "Detecting Reconnection Events in Kinetic Vlasov Hybrid Simulations Using Clustering Techniques".

## DATASET USED IN PUBLISHED WORK
The simulation data-set (TURB 2D) is available at Cineca AIDA-DB. In order to access the meta-information and the link to "TURB 2D" simulation data look at the tutorial at http://aida-space.eu/AIDAdb-iRODS.

## MODULES OVERVIEW
This package contains:
* check_utils.py -> utilities to check libraries version, variables type, presence in metdata;
* ingester.py -> containd the class DataIngestion, useful to manage data and metadata organically;
* calc_utils.py -> contains functions useful to perform computations (norms, scalar product, cross product, derivatives, curl, ...);
* plot_utils.py -> contains funtion that plots some of the intermediate results;
* km_utils_2d.py -> suit of function used to perfrom KMens clusterization, including the crossvalidation phase;
* dbscan_utils_2d.py -> DBscan algorithm applied to one selected cluster, among those found using KMeans algorithm;
* ar_utils_2d.py -> compute with, thickness and aspect ratio of the regions found by the DBscan;
* metadata_example.cfg -> an example of metadata/configuration file;
* test_script.py -> a script with perform a test of these modules. WARNING: usually on small dataset no physically meningful results are obtained;
* test_data.h5 -> a small datased useful to test the scripts functionality.

### CONFIGURATION FILE
Metadata can be easily passed via a configuration file, e.g. 'metadata_example.cfg'.
Anything can be added in the form
```
key = value
```
e.g.:
```
useful_numer = 7
another_number=3.14
my_name = Joe
a_list_of_stuff = 1,2,3,4  # this is a commment will be ignored

# also blank lines will br ignored
```
However, by default all metadata are read as string:
```
meta['key'] = 'value'
```
Yet, in 'ingester.py', the DataIngestion class knows the target type of the metadata useful for the functions defined in these modules. This information is sored inside
```
self._known_meta = {
    "int": ["nx", "ny", "n_init",
            "max_iter", "n_foldings",
            "kmin", "kmax", "kstep",
            "kopt", "lab_rec",
            "DB_min_samples"],
    "float": ["xl", "yl", "dx", "dy",
              "DB_eps"],
    "bool": ["periodic"],
    "str": ["algs", "scores",
            "preprocessing_mode",
            "cv_open", "alg_opt",
            "J_like"],
    "list": ["algs", "scores"],
}
```
So that
```
nx = 128
xl = 3.14
periodic = True
algs = full,elkan
scores = DB
preprocessing_mode = norm
```
will result into
```
meta['nx'] = 128
meta['xl'] = 3.14
meta['periodic'] = True
meta['algs'] = ['full','elkan']
meta['scores'] = ['DB']
meta['preprocessing_mode'] = 'norm'
```

Maybe this method is too ''stiff'', however fot the modules in the present form no other metadata are required. In future, if needed, some flexibility may be added including a type indication in the .cfg file or making so that DataIngestion reads the _known_meta from another file.

### TYPE CHECK
Most of the functions check the type of their arguments. This do not apply (in order to maximize performance) to:
* cut_in_box() in ar_utils_2d.py
* every function in calc_utils_2d.py

## TEST
To run the test just use:
```
python3 test_script.py
```

### OUTPUT EXAMPLES
In TEST_OUT there are the outputs, file and plots, of the modules (you should get the same running test_script.py). TEST_OUT/savestate/test_1.csv.HEAD is not the actual output but only its first 11 row (the whole file is more than 100MB).

## DEPENDCIES
Tested with:
* python = 3.8.5
* numpy = 1.19.1
* scipy = 1.6.1
* sklearn = 0.23.2
* pandas = 1.1.3
* matplotlib = 3.3.2
* numba = 0.52.1

Requires (as far we know):
* python = 3.x
* scipy >= 1.6.1

## CONTACTS
* Sisti, M. (first author, developer): manuela.sisti@univ-amu.fr
* Finelli F. (developer, mantainer): francesco.finelli@phd.unipi.it
* Pedrazzi G. (CINECA supervisor): g.pedrazzi@cineca.it
* Califano F. (UniPi supervisor): francesco.califano@unipi.it
