""" Init """

#aidapy init
from aidapy.aidafunc.load_data import load_data
from aidapy.aidafunc.load_data import get_mission_info
from aidapy.aidafunc.event_search import event_search


__version__ = '0.1.0'
