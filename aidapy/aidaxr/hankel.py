import numpy as np
import scipy.special as sci

def interpolate_hankel(fv, vec, fac, ntry=100000000):
    """
    Creates the grid of zeros of math: J_0(k_max*vec)
    
    Parameters
    ----------
    fv  : `numpy.array`
        1-D function to interpolate.
    
    vec : `numpy.array`
        Vector of the original grid.
    
    fac : `float` 
        Factor that multiplies k and increases 
        the number of zeros to find.
    
    ntry : `int`
        Number of tentatives to try to find the zeros.
    
    Returns
    -------
    fvj : `numpy.array`
        1-D interpolated function.
    
    xj : `numpy.array`
        Vector of the interpolated.
    
    k : `numpy.array`
        Wavevector.
    
    """
    nt = fv.shape[0]
    
    #Find the zeros of J_0(kmax*v)
    nvec = vec.size
    dvec = np.diff(vec)
    kmax = fac*(1/dvec[0])
    L0k = vec.max()
    zz = np.zeros(ntry)
    for i in range (1,ntry):
        zz[i] = (sci.jn_zeros(0,i).max()) / kmax
        if zz[i-1] < L0k and zz[i] > L0k :
            nk = i-1 
            break
    
    # Generating new grid
    k = np.linspace(1./vec.max(),kmax,num=nk)
    xj = (sci.jn_zeros(0,nk))/kmax
    dvj = np.diff(xj)

    # Interpolation of the function
    fvj = np.zeros([nt, xj.size])
    for it in range(nt):
        for iz in range(nk):
            for iv in range(nvec):
                if vec[iv] < xj[iz] and vec[iv+1] > xj[iz]:
                    f2 = fv[it,iv+1]
                    f1 = fv[it,iv]
                    a = xj[iz] - vec[iv]
                    b = vec[iv+1] - xj[iz]
                    fvj[it,iz] = (b*f1 + a*f2) / (a+b)
                    break

    return fvj, xj, k

def integrate_hankel(fj, xj, k, norm=False, inverse=False):
    """
    Calculates the coefficients of the Hankel transform:

    .. math::
 
        H(k) = \int (f(v) * J_0(kv) v  dv)

    Parameters
    ----------
    fj : `numpy.array`
        Function to transform.
    
    xj : `numpy.array`
        Indepentent variable.
    
    k : `numpy.array` 
        Used only when norm = True or/and inverse = True.
    
    norm : `bool`
        default = False.  Calculates the normalized transform:

        .. math::

            H(k) = \int (f(v) * J_0(kv) \sqrt{k*v}  dv) 

    inverse : `bool` 
        default = False.  Calculates the inverse transform:

        .. math::
    
            g(k) = \int (H(k) * J_0(kv) k  dk)
        
        If also norm == True :  

        .. math::

            g(k) = \int ( H(k) * J_0(kv) \sqrt{vk}  dk )
    
    Returns
    -------
    Hk : `numpy.array` 
        Hankel coefficients.
    
    """
    nt = fj.shape[0]
    nk = k.size
    nv = xj.size
    dk = np.diff(k)
    dx = np.diff(xj)
    Hk = np.zeros([nt,nk])

    if norm == False:
        for it in range(nt):
            for ik in range(nk-1):
                integr = 0.0
                J0k = sci.jv(0, k[ik]*xj)
                for iv in range(nv-1): 
                    integr = 0.5 * (fj[it,iv]*xj[iv]*J0k[iv] + fj[it,iv+1]*xj[iv+1]*J0k[iv+1]) * dx[iv]
                    Hk[it,ik] = Hk[it,ik] + integr

        if inverse == True:
            gk = np.zeros([nt,nk])
            for it in range(nt):
                for iv in range(nk):
                    integr = 0.0
                    J0k = sci.jv(0, xj[iv]*k)
                    for ik in range(nk-1):
                        integr = 0.5 * (Hk[it,ik]*k[ik]*J0k[ik] + Hk[it,ik+1]*k[ik+1]*J0k[ik+1]) * dk[ik]
                        gk[it,iv] = gk[it,iv] + integr

            return Hk, gk

        else: 
            return Hk

    else: 
        for it in range(nt):
            for ik in range(nk-1):
                integr = 0.0
                J0k = sci.jv(0, k[ik]*xj)
                for iv in range(nv - 1):
                    integr = 0.5 * (fj[it,iv]*np.sqrt(xj[iv]*k[ik])*J0k[iv] \
                        + fj[it,iv+1]*np.sqrt(xj[iv+1]*k[ik+1])*J0k[iv+1]) * dx[iv]
                    Hk[it,ik] = Hk[it,ik] + integr

        if inverse == True:
            gk = np.zeros([nt,nk])
            for it in range(nt):
                for iv in range(nk):
                    integr = 0.0
                    J0k = sci.jv(0, xj[iv]*k)
                    for ik in range(nk - 1):
                        integr = 0.5 * (Hk[it,ik]*np.sqrt(xj[iv]*k[ik])*J0k[ik] \
                            + Hk[it,ik+1]*np.sqrt(xj[iv]*k[ik+1])*J0k[ik+1]) * dk[ik]
                        gk[it,iv] = gk[it,iv] + integr

            return Hk, gk

        else: 
            return Hk 
