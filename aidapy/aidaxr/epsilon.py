"""AIDA moudule responsible for calculating deviations 
   from thermal equilibrium in VDFs
"""
import numpy as np
import plasmapy as pl
import scipy.special as sci


def compute_rho_vv(vdf, vx, vy, vz, geom=None):
    """
    Returns the density bulk velocity of a given VDF.
    
    Parameters
    ----------    
    vdf : `numpy.array`
        f(t,vx,vy,vz) in Cartesian or spherical geometry.
    
    vx, vy, vz : `numpy.array`
        set of coordinates.
    
    geom : `str`
        default = None. Choose the geometry in which to calculate the moments. 
            Implemented geometries: 
                `cart` : cartesian geometry or `spher` : spherical geometry.
                    
    Returns
    ------  
    rho : `numpy.array` 
        (t), 0th order moment.
    
    vv : `numpy.array`
        (t,3), 1st order moment containing the 3 components of the bulk speed. 
    
    """ 
    nt = vdf.shape[0]
    nx = vx.size ; ny = vy.size ; nz = vz.size
    dx = np.zeros(nx) ; dx[:-1] = np.diff(vx) ; dx[-1] = dx[-2]
    dy = np.zeros(ny) ; dy[:-1] = np.diff(vy) ; dy[-1] = dy[-2]
    dz = np.zeros(nz) ; dz[:-1] = np.diff(vz) ; dz[-1] = dz[-2]
    rho = np.zeros([nt]) ; vv = np.zeros([nt,3])

    if geom == None:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
 
    elif geom == 'cart':
        for it in range(nt):
            rho_x = np.trapz(vdf[it], vx, axis=0)
            rho_xy = np.trapz(rho_x, vy, axis=0)
            rho[it] = np.trapz(rho_xy, vz, axis=0)

            vx_x = np.trapz(vdf[it], vz, axis=2)
            vx_xy = np.trapz(vx_x, vy, axis=1)
            vv[it,0] = np.trapz(vx_xy*vx, vx, axis=0)

            vy_x = np.trapz(vdf[it], vz, axis=2)
            vy_xy = np.trapz(vy_x, vx, axis=0)
            vv[it,1] = np.trapz(vy_xy*vy, vy, axis=0)

            vz_x = np.trapz(vdf[it], vx, axis=0)
            vz_xy = np.trapz(vz_x, vy, axis=0)
            vv[it,2] = np.trapz(vz_xy*vz, vz, axis=0)

            vv[it,:] = vv[it,:] / rho[it]

        print('Cartesian density and bulk flow speed computed.')
        return rho, vv

    elif geom == 'spher':
        integ_rho = 0.0 ; integ_vx  = 0.0
        integ_vy  = 0.0 ; integ_vz  = 0.0
        # Chunk integration terms into the integrand (vdf sperical)
        vdfsp = np.zeros(vdf.shape)
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz):
                        vdfsp[it,ix,iy,iz] = vdf[it,ix,iy,iz] * np.sin(vy[iy]) \
                            * vx[ix]**2
        # Integration along phi
        rho_v_t = np.zeros(vdf.shape[0:3]) ; vx_v_t = np.zeros(vdf.shape[0:3])
        vy_v_t = np.zeros(vdf.shape[0:3])  ; vz_v_t = np.zeros(vdf.shape[0:3])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz - 1):
                        integ_rho = 0.5 * (vdfsp[it,ix,iy,iz+1]+vdfsp[it,ix,iy,iz]) * dz[iz]
                        rho_v_t[it,ix,iy] = rho_v_t[it,ix,iy] + integ_rho

                        integ_vx = 0.5 * (vdfsp[it,ix,iy,iz+1]*np.cos(vz[iz+1]) \
                            + vdfsp[it,ix,iy,iz]*np.cos(vz[iz])) * dz[iz]
                        vx_v_t[it,ix,iy] = vx_v_t[it,ix,iy] + integ_vx

                        integ_vy = 0.5 * (vdfsp[it,ix,iy,iz+1]*np.sin(vz[iz+1]) \
                            + vdfsp[it,ix,iy,iz]*np.sin(vz[iz])) * dz[iz]
                        vy_v_t[it,ix,iy] = vy_v_t[it,ix,iy] + integ_vy

                        integ_vz = integ_rho 
                        vz_v_t[it,ix,iy] = vz_v_t[it,ix,iy] + integ_vz
        # Integration along theta
        rho_v = np.zeros(vdf.shape[0:2]) ; vx_v  = np.zeros(vdf.shape[0:2])
        vy_v  = np.zeros(vdf.shape[0:2]) ; vz_v  = np.zeros(vdf.shape[0:2])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny - 1):
                    integ_rho = 0.5 * (rho_v_t[it,ix,iy+1]+rho_v_t[it,ix,iy]) * dy[iy]
                    rho_v[it,ix] = rho_v[it,ix] + integ_rho

                    integ_vx = 0.5 * (vx_v_t[it,ix,iy+1]*np.sin(vy[iy+1]) \
                        + vx_v_t[it,ix,iy]*np.sin(vy[iy])) * dy[iy]
                    vx_v[it,ix] = vx_v[it,ix] + integ_vx

                    integ_vy = 0.5 * (vy_v_t[it,ix,iy+1]*np.sin(vy[iy+1]) \
                        + vy_v_t[it,ix,iy]*np.sin(vy[iy])) * dy[iy]
                    vy_v[it,ix] = vy_v[it,ix] + integ_vy

                    integ_vz = 0.5 * (vz_v_t[it,ix,iy+1]*np.cos(vy[iy+1]) \
                        + vz_v_t[it,ix,iy]*np.cos(vy[iy])) * dy[iy]
                    vz_v[it,ix] = vz_v[it,ix] + integ_vz
        for it in range(nt): 
            # Correct integration along theta
            for ix in range(nx):
                corr_st  = 0.5 * rho_v_t[it,ix,0] * dy[iy]
                corr_end = 0.5 * rho_v_t[it,ix,ny-1] * dy[iy]
                rho_v[it,ix] = rho_v[it,ix] + corr_st + corr_end

                corr_st  = 0.5 * vx_v_t[it,ix,0] * np.sin(vy[0]) * dy[iy]
                corr_end = 0.5 * vx_v_t[it,ix,ny-1] * np.sin(vy[ny-1]) * dy[iy]
                vx_v[it,ix] = vx_v[it,ix] + corr_st + corr_end

                corr_st  = 0.5 * vy_v_t[it,ix,0] * np.sin(vy[0]) * dy[iy]
                corr_end = 0.5 * vy_v_t[it,ix,ny-1] * np.sin(vy[ny-1]) * dy[iy] 
                vy_v[it,ix] = vy_v[it,ix] + corr_st + corr_end

                corr_st  = 0.5 * vz_v_t[it,ix,0] * np.cos(vy[0]) * dy[iy]
                corr_end = 0.5 * vz_v_t[it,ix,ny-1] * np.cos(vy[ny-1]) * dy[iy] 
                vz_v[it,ix] = vz_v[it,ix] + corr_st + corr_end
        # Integration along v
        vv  = np.zeros([nt,3])
        rho = np.zeros([nt])
        for it in range(nt):
            for ix in range(nx - 1):
                integ_rho = 0.5*(rho_v[it,ix+1] + rho_v[it,ix])*dx[ix]
                rho[it] = rho[it] + integ_rho
 
                integ_vx = 0.5 * (vx_v[it,ix+1]*vx[ix+1] + vx_v[it,ix]*vx[ix]) * dx[ix]
                vv[it,0] = vv[it,0] + integ_vx
 
                integ_vy = 0.5 * (vy_v[it,ix+1]*vx[ix+1] + vy_v[it,ix]*vx[ix]) * dx[ix]
                vv[it,1] = vv[it,1] + integ_vy
 
                integ_vz = 0.5 * (vz_v[it,ix+1]*vx[ix+1] + vz_v[it,ix]*vx[ix]) * dx[ix]
                vv[it,2] = vv[it,2] + integ_vz
            vv[it] = vv[it] / rho[it] 

        print('Density, bulk flow speed computed in spherical geometry.') 
        return rho, vv
 
    else:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
 
def compute_temp(vdf, vx, vy, vz, rho, vv, mass, kb, geom=None, components=False):
    """
    Computes the average temperature of a given VDF.
    
    Parameters
    ----------
    vdf : `numpy.array`
        f(t,vx,vy,vz) in Cartesian or spherical geometry.
    
    vx, vy, vz : `numpy.array` 
        set of coordinates.
    
    rho, vv : `numpy.array` 
        density and bulk speed of the VDF (can be calculated with comp_rho_vv).
    
    geom : `str`
        Default = None. Choose the geometry in which to calculate TT.
            Implemented geometries: 
                `cart` : cartesian geometry or `spher` -> spherical geometry.

    components : `bool`
        Default = False. Flag for returning the temperature "vector"
                    
    Returns 
    -------
    TT : `numpy.array` 
        (t), 2nd order moment.
    
    """
    nt = vdf.shape[0]
    nx = vx.size ; ny = vy.size ; nz = vz.size
    dx = np.zeros(nx) ; dx[:-1] = np.diff(vx) ; dx[-1] = dx[-2]
    dy = np.zeros(ny) ; dy[:-1] = np.diff(vy) ; dy[-1] = dy[-2]
    dz = np.zeros(nz) ; dz[:-1] = np.diff(vz) ; dz[-1] = dz[-2]
    if geom == None:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
    
    elif geom == 'cart':
        # Calculating temperature tensor
        TT = np.zeros([nt])
        T_tensor = np.zeros([nt,3])
        for it in range(nt):
            Tx_x = np.trapz(vdf[it], vz, axis=2)
            Tx_xy = np.trapz(Tx_x, vy, axis=1)
            vmx = (vx - vv[it,0])**2. # (vx - vv[it,0])
            T_tensor[it,0] = np.trapz(Tx_xy*vmx, vx, axis=0)

            Ty_x = np.trapz(vdf[it], vz, axis=2)
            Ty_xy = np.trapz(Ty_x, vx, axis=0)
            vmy = (vy - vv[it,1])**2. # (vy - vv[it,1])
            T_tensor[it,1] = np.trapz(Ty_xy*vmy, vy, axis=0)

            Tz_x = np.trapz(vdf[it], vx, axis=0)
            Tz_xy = np.trapz(Tz_x, vy, axis=0)
            vmz = (vz - vv[it,2])**2. # (vz - vv[it,2])
            T_tensor[it,2] = np.trapz(Tz_xy*vmz, vz, axis=0)

            T_tensor[it,:] = mass * T_tensor[it,:] / (kb*rho[it])
            TT[it] = np.sum(T_tensor[it,:]) / 3

        print('Cartesian temperature tensor and trace computed.')
        if components == True:
            return T_tensor
        else: 
            return TT
    
    elif geom == 'spher':
        integ_Tx  = 0.0 ; integ_Ty  = 0.0 ; integ_Tz  = 0.0
        TT = np.zeros([nt])
        # Chunk integration terms into the integrand (vdf sperical)
        vdfspx = np.zeros([nt,nx,ny,nz])
        vdfspy = np.zeros([nt,nx,ny,nz])
        vdfspz = np.zeros([nt,nx,ny,nz])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz):
                        vdfspx[it,ix,iy,iz] = vdf[it,ix,iy,iz] * np.sin(vy[iy]) * vx[ix]**2 \
                            * (vx[ix]*np.sin(vy[iy])*np.cos(vz[iz]) - vv[it,0])**2

                        vdfspy[it,ix,iy,iz] = vdf[it,ix,iy,iz] * np.sin(vy[iy]) * vx[ix]**2 \
                            * (vx[ix]*np.sin(vy[iy])*np.sin(vz[iz]) - vv[it,1])**2

                        vdfspz[it,ix,iy,iz] = vdf[it,ix,iy,iz] * np.sin(vy[iy]) * vx[ix]**2 \
                            * (vx[ix]*np.cos(vy[iy]) - vv[it,2])**2
        # Integration along phi
        Tx_v_t = np.zeros([nt,nx,ny])
        Ty_v_t = np.zeros([nt,nx,ny])
        Tz_v_t = np.zeros([nt,nx,ny])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz - 1):
                        integ_Tx = 0.5 * (vdfspx[it,ix,iy,iz+1]+vdfspx[it,ix,iy,iz]) * dz[iz]
                        Tx_v_t[it,ix,iy] = Tx_v_t[it,ix,iy] + integ_Tx
    
                        integ_Ty = 0.5 * (vdfspy[it,ix,iy,iz+1]+vdfspy[it,ix,iy,iz]) * dz[iz]
                        Ty_v_t[it,ix,iy] = Ty_v_t[it,ix,iy] + integ_Ty
    
                        integ_Tz = 0.5 * (vdfspz[it,ix,iy,iz+1]+vdfspz[it,ix,iy,iz]) * dz[iz]
                        Tz_v_t[it,ix,iy] = Tz_v_t[it,ix,iy] + integ_Tz
        # Integration along theta
        Tx_v  = np.zeros([nt,nx])
        Ty_v  = np.zeros([nt,nx])
        Tz_v  = np.zeros([nt,nx])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny - 1):
                    integ_Tx = 0.5 * (Tx_v_t[it,ix,iy+1]+Tx_v_t[it,ix,iy]) * dy[iy]
                    Tx_v[it,ix] = Tx_v[it,ix] + integ_Tx
 
                    integ_Ty = 0.5 * (Ty_v_t[it,ix,iy+1]+Ty_v_t[it,ix,iy]) * dy[iy]
                    Ty_v[it,ix] = Ty_v[it,ix] + integ_Ty
 
                    integ_Tz = 0.5 * (Tz_v_t[it,ix,iy+1]+Tz_v_t[it,ix,iy]) * dy[iy]
                    Tz_v[it,ix] = Tz_v[it,ix] + integ_Tz
        # Correction along theta
        for it in range(nt):
            for ix in range(nx):
                corr_st  = 0.5 * Tx_v_t[it,ix,0] * dy[iy]
                corr_end = 0.5 * Tx_v_t[it,ix,ny-1] * dy[iy]
                Tx_v[it,ix] = Tx_v[it,ix] + corr_st + corr_end

                corr_st  = 0.5 * Ty_v_t[it,ix,0] * dy[iy]
                corr_end = 0.5 * Ty_v_t[it,ix,ny-1] * dy[iy]
                Ty_v[it,ix] = Ty_v[it,ix] + corr_st + corr_end
 
                corr_st  = 0.5 * Tz_v_t[it,ix,0] * dy[iy]
                corr_end = 0.5 * Tz_v_t[it,ix,ny-1] * dy[iy]
                Tz_v[it,ix] = Tz_v[it,ix] + corr_st + corr_end
        # Integration along v
        Txyz = np.zeros([nt,3])
        for it in range(nt):
            for ix in range(nx - 1):
                integ_Tx = 0.5 * (Tx_v[it,ix+1]+Tx_v[it,ix]) * dx[ix]
                Txyz[it,0] = Txyz[it,0] + integ_Tx
 
                integ_Ty = 0.5 * (Ty_v[it,ix+1]+Ty_v[it,ix]) * dx[ix]
                Txyz[it,1] = Txyz[it,1] + integ_Ty
 
                integ_Tz = 0.5 * (Tz_v[it,ix+1]+Tz_v[it,ix]) * dx[ix]
                Txyz[it,2] = Txyz[it,2] + integ_Tz
 
            Txyz[it] = mass*Txyz[it] / (kb*rho[it])
            TT[it] = np.sum(Txyz[it,:]) / 3
        print('Temperature in spherical coordinates computed.')
        if components == True:
            return Txyz
        else:
            return TT
 
    else:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")

def compute_synth_maxw_1D(vx, TT, v_drift, vTh, particle='p'):
    """
    Generates a 1D Maxwellian distribution in Cartesian geometry.
    Uses Maxwellian_1D from plasmapy.formulary.distribution.

    Parameters
    ---------
    vx : `numpy.array`
         Coordinates.
 
    TT : `numpy.array`
        (nt) Temperature.

    v_drift : `numpy.array`
        (nt) mean velocity in each direction.

    vTh : `numpy.array`
        (nt) Thermal velocity. Required to calculate non-dimensional vdf. 
        .. math::
            vTh = \sqrt{2. * kb TT / mass}
    
    particle: `str`
        default = `p`. Choose the particles for which construct the Maxwellian.
        Options : 
            `p` : ions
            `e` : electrons.
    Returns
    -------
    mBo: `numpy.array`
        (nt,vx), Maxwellian function
    
    """
    nt = TT.size
    nx = vx.size

    mBo = np.zeros([nt,nx])
    for it in range(nt):
        mBo[it] = pl.formulary.distribution.Maxwellian_1D(vx, TT[it], particle=particle, \
            v_drift=v_drift[it], vTh=vTh[it], units='unitless')

    return mBo

def compute_synth_maxw_2D(vx, vy, TT, v_drift, vTh, particle='p'):
    """
    Generates a 2D Maxwellian distribution in Cartesian geometry.
    Uses Maxwellian_2D from plasmapy.formulary.distribution.

    Parameters
    ---------
    vx, vy : `numpy.array`
         Coordinates.
 
    TT : `numpy.array`
        (nt) Temperature.

    v_drift : `numpy.array`
        (nt,2) mean velocity in x- and y- direction.

    vTh : `numpy.array`
        (nt) Thermal velocity. Required to calculate non-dimensional vdf. 
        .. math::
            vTh = \sqrt{2. * kb TT / mass}
    
    particle: `str`
        default = `p`. Choose the particles for which construct the Maxwellian.
        Options : 
            `p` : ions
            `e` : electrons.
    Returns
    -------
    mBo: `numpy.array`
        (nt,vx), Maxwellian function
    
    """
    nt = TT.size
    nx = vx.size
    ny = vy.size
    vx_drift = v_drift[:,0]
    vy_drift = v_drift[:,1]

    #Generating the 2D mesh
    vx_2D, vy_2D = np.meshgrid(vx, vy)

    mBo = np.zeros([nt,nx,ny])
    for it in range(nt):
        mBo[it] = pl.formulary.distribution.Maxwellian_velocity_2D(vx_2D, vy_2D, TT[it], \
            particle=particle, vx_drift=vx_drift[it], vy_drift=vy_drift[it], \
            vTh=vTh[it], units='unitless')

    return mBo

def compute_synth_maxw_3D(vx, vy, vz, TT, v_drift, vTh, particle='p', geom=None):
    """
    Generates a Maxwellian distribution on a given grid.
    For Cartesian geometry uses Maxwellian_velocity_1D 
    from plasmapy.formulary.distribution.

    Parameters
    ---------
    vx, vy, vz : `numpy.array`
         Set of coordinates.
 
    TT : `numpy.array`
        (nt) Temperature.

    v_drift : `numpy.array`
        (nt,3) mean velocity in each direction.

    vTh : `numpy.array`
        (nt) Thermal velocity. Required to calculate non-dimensional vdf. 
        .. math::
            vTh = \sqrt{2. * kb TT / mass}

    particle : `str`
        default = `p`. Choose the particles for which construct the Maxwellian.
        Options : 
            `p` : ions
            `e` : electrons.

    geom : `str` 
        default = None. 
        Choose the geometry in which  to calculate math:`\epsilon`. 
        Implemented geometries:
            `cart` : Cartesian geometry. 
            `spher` : spherical geometry.
    
    Returns
    -------
    mBo: `numpy.array`
        (nt,vx,vy,vz), Maxwellian function
    
    """
    nt = TT.size
    nx = vx.size ; ny = vy.size ; nz = vz.size
    vx_drift = v_drift[:,0]
    vy_drift = v_drift[:,1]
    vz_drift = v_drift[:,2]

    mBo = np.zeros([nt,nx,ny,nz])
    if geom == None:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")

    elif geom == 'cart':
        # Generating the 3D mesh
        vx_3D, vy_3D, vz_3D = np.meshgrid(vx, vy, vz)
        for it in range(nt):
            mBo[it] = pl.formulary.distribution.Maxwellian_velocity_3D(vx_3D, vy_3D, vz_3D, \
                TT[it], particle=particle, vx_drift=vx_drift[it], vy_drift=vy_drift[it], \
                vz_drift=vz_drift[it], vTh=vTh[it], units='unitless')

        print('Synthetic Cartesian Maxwellian computed.')
        return mBo

    elif geom == 'spher':
        gaussK2 = (np.pi*vTh**2.)**(-3./2.)
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz):
                        mBo[it,ix,iy,iz] = gaussK2[it] \
                            * np.exp(-(vTh[it]**(-2.)) \
                            * ((vx[ix]*np.cos(vz[iz])*np.sin(vy[iy]) - v_drift[it,0])**2 \
                            + (vx[ix]*np.sin(vz[iz])*np.sin(vy[iy]) - v_drift[it,1])**2 \
                            + (vx[ix]*np.cos(vy[iy]) - v_drift[it,2])**2))

        print('Synthetic spherical  Maxwellian computed.')
        return mBo

    else:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")

def comp_Kdistr(vv, rho, T, kb, mass, kappa, vx, vy, vz, geom=None):
    """
    Compute a K-distribution with the formula given in
    Perrard & Lazar 2010, Sol. Phys., 267, 153.
 
    Parameters
    ---------
    vv, rho, T : `numpy.arrays`
        mean velocity, density and temperature
 
    mass : `float`
        mass of the particles' species
 
    kappa : `numpy.array`
         exponent of the K-distr.  Must have same dimensions as rho

    vx, vy, vz : `numpy.array`
         set of coordinates

    geom: `str`
        default = None.  Choose the geometry in which to calculate Fk.
            Implemented geometries:
                `cart` : cartesian geometry or `spher` : spherical geometry

    Returns
    -------
    Fk : `numpy.array`
        (t,vx,vy,vz), K-distribution
    """
    nt = rho.shape[0] ; nx = vx.size ; ny = vy.size ; nz = vz.size
    gamma_const = np.zeros([nt])
    w_ki = np.zeros([nt])
    const = np.zeros([nt])
    Fk = np.zeros([nt,nx,ny,nz])
 
    if geom == None:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
 
    elif geom == 'cart':
        for it in range(nt):
            gamma_const[it] = sci.gamma(kappa[it]+1) / (sci.gamma(kappa[it]-1/2) * sci.gamma(3/2))
            w_ki[it] = np.sqrt((2.*kappa[it]-3)*kb*T[it] / (kappa[it]*mass))
            const[it] = rho[it] / ((2.*np.pi) * (kappa[it]*w_ki[it]*w_ki[it])**(3/2))
            for ix in range(nx):
                    for iy in range(ny):
                        for iz in range(nz):
                            Fk[it,ix,iy,iz] = const[it] * gamma_const[it] \
                                * (1. + ((vx[ix]-vv[it,0])**2 + (vy[iy]-vv[it,1])**2 \
                                + (vz[iz]-vv[it,2])**2 ) / (kappa[it]*w_ki[it]*w_ki[it]))**(-(kappa[it]+1.))

        print('K-distribution in cartesian geometry calculated.')
        return Fk

    elif geom == 'spher':
        for it in range(nt):
            gamma_const[it] = sci.gamma(kappa[it]+1) / (sci.gamma(kappa[it]-1/2) * sci.gamma(3/2))
            w_ki[it] = np.sqrt((2.*kappa[it]-3)*kb*T[it] / (kappa[it]*mass))
            const[it] = rho[it] / ((2.*np.pi) * (kappa[it]*w_ki[it]*w_ki[it])**(3/2))
            for ix in range(nx):
                    for iy in range(ny):
                        for iz in range(nz):
                            Fk[it,ix,iy,iz] = const[it] * gamma_const[it] \
                                * (1. + ((vx[ix]*np.cos(vz[iz])*np.sin(vy[iy]) - vv[it,0])**2 \
                                + (vx[ix]*np.sin(vz[iz])*np.sin(vy[iy]) - vv[it,1])**2 \
                                + (vx[ix]*np.cos(vy[iy]) - vv[it,2])**2 ) \
                                / (kappa[it]*w_ki[it]*w_ki[it]))**(-(kappa[it]+1.))
 
        print('K-distr in spherical geometry computed.')
        return Fk
 
    else:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")

def integrate_mms_th_phi(vdf, vx, vy, vz):
    """
    Integrates the VDF from MMS (in spherical coordinates) in the angle.
    Uses the trapezoid method for integration.
    
    Parameters
    ----------
    vdf : `numpy.array` 
        Function to integrate
    
    vx, vy, vz : `numpy.array`
        Set of coordinates
    
    Returns
    -------
    F_v : `numpy.array`
        Function integrated in the angular coordinates
    
    """
    nt = vdf.shape[0] ; nx = vx.size ; ny = vy.size ; nz = vz.size
    dx = np.diff(vx)  ; dy = np.diff(vy); dz = np.diff(vz)
 
    # Chunk integration terms into the integrand (vdf sperical)
    vdfsp = np.zeros([nt,nx,ny,nz])
    for it in range(nt):
        for ix in range(nx):
            for iy in range(ny):
                for iz in range(nz):
                    vdfsp[it,ix,iy,iz] = vdf[it,ix,iy,iz] * np.sin(vy[iy])
    # Integration along phi
    F_v_t = np.zeros([nt,nx,ny])
    for it in range(nt):
        for ix in range(nx):
            for iy in range(ny):
                for iz in range(nz - 1):
                    integ = 0.5 * (vdfsp[it,ix,iy,iz+1]+vdfsp[it,ix,iy,iz]) * dz[iz]
                    F_v_t[it,ix,iy] = F_v_t[it,ix,iy] + integ
    # Integration along theta
    F_v = np.zeros([nt,nx])
    for it in range(nt):
        for ix in range(nx):
            for iy in range(ny-1):
                integ = 0.5 * (F_v_t[it,ix,iy+1]+F_v_t[it,ix,iy]) * dy[iy]
                F_v[it,ix] = F_v[it,ix] + integ
                # Corrections at the borders
                corr_st  = 0.5 * F_v_t[it,ix,0] * dy[iy]
                corr_end = 0.5 * F_v_t[it,ix,ny-1] * dy[iy]
                F_v[it,ix] = F_v[it,ix] + corr_st + corr_end
 
    print('f(v_r,v_{\theta},v_{\phi}) integrated: f(v) returned.')
    return F_v

def epsilon(vdf, mBo, vx, vy, vz, rho, geom=None, correct=False):
    """
    Calculates departure from Maxwellian distribution
    for a given VDF.

    .. math:: 
    
        \epsilon (t) = \frac{1}{rho} \sqrt{\int (f(t,v) - g(t,v))^2 dV}

    where :math:`rho` is the 0th order moment, 
        math:`g(t,v) is the associated Maxwellian of math:`f(t,v)`,
        math: `dV` is the differential on the volume.

    Parameters
    ---------
    vdf : `numpy.array`
        f(t,vx,vy,vz) in Cartesian or spherical geometry.
    
    mBo: `numpy.array` 
        Associated Maxwellian of f(t,vx,vy,vz).
        Can be calculated with comp_synth_maxw in this module.
    
    vx, vy, vz : `numpy.array`
        Set of coordinates

    rho : `numpy.array`
        Density

    geom: `str` 
        default = None. 
        Choose the geometry in which  to calculate math:`\epsilon`. 
        Implemented geometries:
            `cart` : Cartesian geometry or `spher` : spherical geometry.
   
    correct: `bool`
        default = None. 
        Corrects the data for missing range in math:`\theta` and v 

    Returns
    -------
    epsilon : `numpy.array`

    """
    nt = vdf.shape[0] ; nx = vx.size ; ny = vy.size ; nz = vz.size
    dx = np.zeros(nx) ; dx[:-1] = np.diff(vx) ; dx[-1] = dx[-2]  
    dy = np.zeros(ny) ; dy[:-1] = np.diff(vy) ; dy[-1] = dy[-2]
    dz = np.zeros(nz) ; dz[:-1] = np.diff(vz) ; dz[-1] = dz[-2]
    
    if geom == None:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
 
    elif geom == 'cart':
        epsilon_xyz = np.zeros([nt])
        epsilon = np.zeros([nt])
        for it in range(nt):
            func = (vdf[it]-mBo[it])**2
            epsilon_x = np.trapz(func, vx, axis=0)
            epsilon_xy = np.trapz(epsilon_x, vy, axis=0)
            epsilon_xyz[it] = np.trapz(epsilon_xy, vz, axis=0)
        epsilon = np.sqrt(epsilon_xyz) / rho

        print('epsilon in cartesian geometry computed.')
        return epsilon

    elif geom == 'spher':
        eps = np.zeros([nt])
        epsarg = np.zeros([nt,nx,ny,nz])
        epsilon_xyz = np.zeros([nt])
        for it in range(nt):
            for ix in range(nx):
                for iy in range(ny):
                    for iz in range(nz):
                        epsarg[it,ix,iy,iz] = (vdf[it,ix,iy,iz]-mBo[it,ix,iy,iz])**2 \
                            * np.sin(vy[iy]) * vx[ix]**2
            epsilon_z = np.trapz(epsarg[it], vz, axis=2)
            epsilon_yz = np.trapz(epsilon_z, vy, axis=1)
            if correct == True:
                for ix in range(nx):
                    corr_st  = 0.5 * epsilon_z[it,ix,0] * dy[0]
                    corr_end = 0.5 * epsilon_z[it,ix,ny-1] * dy[-1]
                    epsilon_yz[it,ix] = epsilon_yz[it,ix] + corr_st + corr_end
 
            epsilon_xyz[it] = np.trapz(epsilon_yz, vx, axis=0)
            if correct == True:
                epsilon_xyz[it] = epsilon_xyz[it] + 0.5*epsilon_yz[it,0]*dx[0]

        epsilon = np.sqrt(epsilon_xyz) / rho

        print('epsilon in spherical geometry computed.') 
        return epsilon
 
    else:
        return print("Unknown geometry. Implemented: geom='cart' or geom='spher'.")
