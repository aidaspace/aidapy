.. -*- mode: rst -*-

.. |LicenseMIT| image:: https://img.shields.io/badge/License-MIT-yellow.svg
.. _LicenseMIT: https://opensource.org/licenses/MIT

.. |LicenseCC| image:: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
.. _LicenseCC: https://creativecommons.org/licenses/by/4.0/

.. |Pipeline| image:: https://gitlab.com/aidaspace/aidapy/badges/master/pipeline.svg
.. _Pipeline: https://gitlab.com/aidaspace/aidapy/commits/master

.. |CoverageReport| image:: https://codecov.io/gl/aidaspace/aidapy/branch/master/graph/badge.svg
.. _CoverageReport: https://codecov.io/gl/aidaspace/aidapy

.. |PylintScore| image:: https://aidaspace.gitlab.io/aidapy/pylint.svg
.. _PylintScore: https://gitlab.com/aidaspace/aidapy/commits/master

.. |Maintenance| image:: https://img.shields.io/badge/Maintained%3F-yes-green.svg
.. _Maintenance: https://gitlab.com/aidaspace/aidapy/commits/master

.. |DocSphinx| image:: https://img.shields.io/static/v1.svg?label=sphinx&message=documentation&color=blue
.. _DocSphinx: https://gitlab.com/aidaspace/aidapy/commits/master

.. |PyPi| image:: https://img.shields.io/badge/install_with-pypi-brightgreen.svg
.. _PyPi: https://pypi.org/project/aidapy/


AIDApy
=======


|LicenseMit|_ |LicenseCC|_ |Pipeline|_ |PyPi|_ |CoverageReport|_ |PylintScore|_ |DocSphinx|_ |Maintenance|_

The Python package ``aidapy`` centralizes and simplifies access to:

- Spacecraft data from heliospheric missions
- Space physics simulations
- Advanced statistical tools
- Machine Learning, Deep Learning algorithms, and applications

The ``aidapy`` package includes four main sub-packages which are explained in detail in the Reference Manual section:

- Mission tool
- Event search tool
- Velocity distribution function tool
- Machine learning tool


The ``aidapy`` package is part of the project AIDA (Artificial Intelligence Data Analysis) in Heliophysics funded from
the European  Unions  Horizon  2020  research  and  innovation  programme under grant agreement No 776262.
It is distributed under the open-source MIT license.

Full documentation can be found `here <https://aidapy.readthedocs.io>`_


.. end-marker-intro-do-not-remove


.. start-marker-install-do-not-remove


Description
------------

AIDApy's high level interface has been created in order to combine
simplicity with workability. It provides a simple mechanism for downloading multi-dimensional
time-series from various sources as well as to easilly analyse them by extracting from them different meaningfull statistics.
The core of the simplicity streams from the fact that inherits many features from high-level data structures such as xarray,
and improves them by adding new ones.

A second advantage of AIDApy interface is the easiness to understand and run the supported use cases that comes with the 
installation of the python package. Heliophysics applications of high importance are included and have optimized with various ways in the 
current version. 

Summing up, some of the features of AIDApy are:
      
      1. **A module to easilly download heliophysics data**
      2. **Xarray data structures to extract statistics.**
      3. **More sophisticated statistics are included such as events identification.**
      4. **Multiple helliophysics use-cases ready to use.**
      5. **Simple interface based on configuration files to run them.**
      6. **Easy mechanism to add custom metrics, losses and datasets**
      7. **Visualization and logging of the training/validation procedure.**
      8. **Optimization techniques such us Hyper-Parameter Optimization (HPO) and pruning are provided.**


Installation
------------

The package aidapy has been tested for Linux (Ubuntu 16.04 / 18.04) and Windows 10.


Using PyPi
^^^^^^^^^^^^^^^

``aidapy`` is available for pip.

.. code-block:: bash

        pip install aidapy


From sources
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The sources are located on **GitLab**:

    https://gitlab.com/aidaspace/aidapy

1) Clone the GitLab repo:

Open a terminal and write the below command to clone the
AIDApy repo:

.. code-block:: bash

    git clone https://gitlab.com/aidaspace/aidapy.git
    cd aidapy


2) Create a anaconda env

AIDApy needs a significant number of dependencies. The easiest
way to get everything installed is to use a virtual environment.

-  Anaconda

You can create an anaconda environment and install all the dependencies
with the following commands:

.. code-block:: bash

    conda create -n aidapy python=3.6 
    conda activate aidapy

.. _conda: http://conda.io/



3) Install the version you want via the commands:

For the "basic" version:

.. code-block:: bash

        python setup.py install


For the "full" version which also includes the ML use cases:

.. code-block:: bash

        pip3 install -e .[ml]


4) Test the installation in your PC by running. (**Install the "full" version before running the tests**)

.. code-block:: bash

        python setup.py test

5) (Optional) Generate the docs: install the extra dependencies of doc and run
the `setup.py` file:

.. code-block:: bash

        pip3 install -e .[doc] 
        python setup.py build_sphinx

Once installed, the doc can be generated with:

.. code-block:: bash

        cd doc
        make html


Usage
--------


**Download Data & Extract Statistics**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the example below, the end user
downloads data from the MMS space mission for a specific time range and
afterwards extracts the *mean* of these. Finally the timeseries are
ploted in the screen.

.. code:: python

    from datetime import datetime
    #AIDApy Modules
    from aidapy import load_data

    ###############################################################################
    # Define data parameters
    ###############################################################################
    # Time Interval
    start_time = datetime(2018, 4, 8, 0, 0, 0)
    end_time = datetime(2018, 4, 8, 0, 1, 0)

    # Dictionary of data settings: mission, product, probe, coordinates
    # Currently available products: 'dc_mag', 'i_dens', and 'all'
    settings = {'prod': ['dc_mag'], 'probes': ['1', '2'], 'coords': 'gse'}

    ###############################################################################
    # Download and load desired data as aidapy timeseries
    ###############################################################################
    xr_mms = load_data(mission='mms', start_time=start_time, end_time=end_time, **settings)

    ###############################################################################
    # Extract a Statistical Measurement of the data
    ###############################################################################
    xr_mms['dc_mag1'].statistics.mean()

    ###############################################################################
    # Plot the loaded aidapy timeseries
    ###############################################################################
    xr_mms['dc_mag1'].graphical.peek()



**Runing a variety of Use Cases**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The end-user can also run different applications with AIDApy's easy to use interface. 
Currently we provide several use-cases as the automatically detection of coronal holes
in SDO images as well as predicting various important solar indexes such as the DST Index.

Also if you are interested of adding your use-case in AIDApy, we have created a README in the machine learning 
module (aidapy/ml/README.md) that describes analytically the steps of how to add them.

Please also check the **configs** folder in the **root directory** of **aidapy** to see more details of how
you declare the options you want to use and what is supported right now.

.. code:: bash

    #For training a UNet for detecting the coronal holes in SDO images.
    aidapy --config examples/04_coronal_holes/config_coronal_holes.yml
   
    #This enables the Hyper-Parameter Optimization (HPO) feature for finding 
    #automatickly the best parameters.
    aidapy --config examples/04_coronal_holes/config_coronal_holes_with_hpo.yml

    #This enables pruning a UNet to optimize it's memory
    #and computation complexity.
    aidapy --config examples/04_coronal_holes/config_coronal_holes_with_pruning.yml

    #For training a LSTM / MLP for  predicting the DST index.
    aidapy --config examples/05_omni_regressor_lstm/config_dst_index_lstm.yml
    aidapy --config examples/05_omni_regressor_lstm/config_dst_index_mlp.yml

    #Enables the hyper-parameter optimization for finding
    #automatically the best hyper-parameters.
    aidapy --config examples/05_omni_regressor_lstm/config_dst_index_lstm_with_hpo.yml
    aidapy --config examples/05_omni_regressor_lstm/config_dst_index_mlp_with_hpo.yml
..


Contributing
------------

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

All the code must follow the instructions of STYLEGUIDE.rst. Please make sure to update tests as
appropriate.

Licenses
--------

This software (AIDApy) and the database of the AIDA project (AIDAdb) are
distributed under the `MIT <https://www.gnu.org/licenses/gpl-3.0>`__
license.

The data collections included in the AIDAdb are distributed under the
Creative Commons `CC BT
4.0 <https://creativecommons.org/licenses/by/4.0/>`__ license.



.. |license-mit| image:: https://img.shields.io/badge/License-MIT-yellow.svg
   :target: https://opensource.org/licenses/MIT
.. |license-cc| image:: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
   :target: https://creativecommons.org/licenses/by/4.0/
.. |pipeline-status| image:: https://gitlab.com/aidaspace/aidapy/badges/master/pipeline.svg
   :target: https://gitlab.com/aidaspace/aidapy/commits/master
.. |coverage-report| image:: https://codecov.io/gl/aidaspace/aidapy/branch/master/graph/badge.svg
   :target: https://codecov.io/gl/aidaspace/aidapy
.. |pylint-score| image:: https://aidaspace.gitlab.io/aidapy/pylint.svg
   :target: https://gitlab.com/aidaspace/aidapy/commits/master
.. |maintenance-yes| image:: https://img.shields.io/badge/Maintained%3F-yes-green.svg
   :target: https://gitlab.com/aidaspace/aidapy/commits/master
.. |doc-sphinx| image:: https://img.shields.io/static/v1.svg?label=sphinx&message=documentation&color=blue
   :target: https://gitlab.com/aidaspace/aidapy/commits/master
.. |pypi| image:: https://img.shields.io/badge/install_with-pypi-brightgreen.svg
   :target: https://pypi.org/project/aidapy/

