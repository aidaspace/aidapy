DST Index forecasting using LSTM network.
==============================================

This section contains different configuration files for you to train a LSTM for forecasting DST Index in 
Omni Mission available data.

-  To run the use-cases you need to initially configure the parameters to download data from OMNI Mission. This is being handled automatically from the Interface by utilizing AIDApy Data Engine:
      -   Configure the available parameters in the config file.

-  Finally run the following command to start the training:
      -  aidapy --config examples/05_omni_regressor_lstm/config_omni_web.yml
