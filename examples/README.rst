Examples
==================

This directory contains a list of examples showing the use of the AIDApy package:

- timeseries: it shows how AIDApy timeseries can be built, how xarray is handled, how to download and load data from space missions such as OmniwebData, MMS, and cluster
- mlp: build linear regression and multi layer perceptron on Omniweb Data
- coronal holes: trains an semantic segmentation CNN for identifying coronal holes in SDO images.

