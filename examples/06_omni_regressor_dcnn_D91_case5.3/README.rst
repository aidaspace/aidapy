DST Index forecasting using LSTM network.
==============================================

This section contains different configuration files for you to train a DCNN for forecasting DST Index in 
Omni Mission available data. It was developed in the context of the Deliverable 9.1 in order the end-user to be able to reproduce the results.

-  To run the use-cases you need to initially configure the parameters to download data from OMNI Mission. This is being handled automatically from the Interface by utilizing AIDApy Data Engine:
   -   Configure the available parameters in the config file.

-  Run the following command to train a DST forecaster:
   -  aidapy --config  examples/06_omni_regressor_dcnn_D91_case5.3/config_omni_web_D91_case53.yml

-  To visualize metric and loss functions with tensorboard after training a DST forecaster:
   -  go into the Experiments/DST_case53 folder 
   -  run the following command: tensorboard --logdir .

-  To train a DST forecaster with Hyper Parameters Optimization using optuna engine run:
   -  aidapy --config examples/06_omni_regressor_dcnn_D91_case5.3/config_omni_web_D91_case53_hpo.yml

-  To prune dcnn model for forecasting DST index run:
   -  aidapy --config examples/06_omni_regressor_dcnn_D91_case5.3/config_omni_web_D91_case53_prune.yml

-  Finally run the following command to reproduce our results:
   - aidapy --config  examples/06_omni_regressor_dcnn_D91_case5.3/config_omni_web_D91_case53_test.yml