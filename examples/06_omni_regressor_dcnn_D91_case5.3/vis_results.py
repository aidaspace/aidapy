""" Visualization function that inherits the functionalities from AIDApy
"""
import os, sys, argparse
import torch
import numpy as np
from tqdm import tqdm
# AIDApy modules
from aidapy.ml.builders import *
from aidapy.ml.callbacks.preprocesscallback import Preprocessdata
from aidapy.ml.models.dcnn import DCNN
from aidapy.ml.visualization.dst_vis import plot_set_of_dataset
from aidapy.ml.utils.config_parser import ConfigParser

#Debuger
import pdb

parser = argparse.ArgumentParser(description='AIDApy Main Configuration script')
parser.add_argument('--config', type=str, default='config_omni_web_D91_case53_test.yml',
                    help='location of the configuration file')
args = parser.parse_args()

def main():
    #Configuration of the experiments
    cfg = ConfigParser(args.config)
    #Device to run the experiments
    device = torch.device('cpu' if not cfg.getUseGPU() else 'cuda')
    #Build the DataLoader module
    loader = build_dataloader(cfg, None)
    loader_test  = loader.loader_test
    preprocess = Preprocessdata(device)
    ## Model in evaluation mode
    model = DCNN(cfg)
    model.to(device)
    model.load_state_dict(torch.load(cfg.getPreTrained()))
    model.eval()
    # Iterate to the dataset.
    results = dict()
    for _, d in enumerate(loader_test):
        results[d.dataset.name] = dict()
        results[d.dataset.name]['timeset'] = d.dataset.val_times
        results[d.dataset.name]['pred'] = np.zeros(len(d.dataset.val_times))
        results[d.dataset.name]['truth'] = np.zeros(len(d.dataset.val_times))
        for itr, (inputs, targets, _) in enumerate(tqdm(d, ncols=80)):
            inp = preprocess.prepare(inputs)[0]
            output = model(inp)
            results[d.dataset.name]['pred'][
                itr*inp.shape[0]:((itr+1)*inp.shape[0])] = \
                            output.cpu().squeeze().detach().numpy()
            results[d.dataset.name]['truth'][
                itr*inp.shape[0]:((itr+1)*inp.shape[0])] = \
                            targets.cpu().squeeze().detach().numpy()

    #Visualize the data
    horizon = cfg.getDataVars('transform')['horizon']
    plot_set_of_dataset(results[d.dataset.name]['truth'], results[d.dataset.name]['pred'], 
        results[d.dataset.name]['timeset'].astype('datetime64[ns]'), horizon,    
            save=True, fname='case53_predictions')

  
if __name__ == '__main__':
   main()
