# Configuration file for the AIDApy package.
# The user can choose the parameters of the:
#     1. Mission module responsible for downloading and manipulating the data
#     2. Training procedure as hyper-parameter optimization
#     3. Model optimization (as pruning and quantization)
interface: 0.5
    

################## System Params  ##################
system:
    use_gpu: true
    gpu_id: 1
    experiment_dir: "Experiments"
    experiment_name: "Exp_dst_forecast_LSTM"
    reset: true
    random_seed: 42 #10

################## Dataset Params  ##################
datasets:
    #Where to find the data
    datamode: 'download' ###############'download' #['download', 'memory']
    #If True, uses the data from one source as trainval. Meaning
    #to split the data into 2 parts and train-val with those.
    #If False, you can have separate sources for the training data
    #and validation.
    trainval: True #[True, False]
    #split_parts: "80-20" #We use custom splitting in this use case
    train_datasets: [Aidapy_OmniWeb]
    #test_datasets: [OmniWebData] #In trainval mode you don't need it.
    

datamode:
    #If you want to load the data from the memory
    memory: 
        #Path that the data have been downloaded.
        data_path: "/aida/data"
    #If you want to download the data.
    download:
        #Name of the Mission
        mission: 'omni'
        #Datetime period to download
        datetime: ['2001-01-13 23:00:00', '2003-01-01 00:00:00']
        #Extra settings to add as arguments in the mission module.
        #Please advise the module documentation for all the options.
        settings:
            prod: ['all']
        #Features to download
        features: ['DST', 'F', 'BZ_GSM', 'N', 'V']
    #How to tranform the dataset into Input-Target
    transform:
        #Time-series to use as inputs
        input_feats: ['DST', 'F', 'BZ_GSM', 'N', 'V']
        #Time-series to use for target / prediction
        target_feats: ['DST']
        #How many timesteps in the future to predict (from the current time).
        horizon: 6
        #How many timesteps to look back (from the current time).
        window: 6
        # What transformation to perform to the input tensors
        # DCNN_Format for dcnn and LSTM_Format for lstm or mlp         
        data_mixin: 'LSTM_Format'    
        
################## Visualization Params #############
visualization:
    #It depends from the type of problem e.g. 
    #img segmentation (vis-seg), time-series forecasting (vis-fc)
    visualizer: 'vis_fc' #['vis_seg', 'vis_fc'] 
    model_graph: true
    losses: true 
    metrics: true
    val_images: true #Uses a callback to visualize the images in the tensorboard. 
    num_images: 10 #How many images to visualize. Takes the same images every epoch.
    #If the number is less than the actual images, it takes them uniformly. 
    #If the number is bigger it takes them all.
    save_images: true #Uses a callback to save the above images also in the memory
    #Choose what images to save 
    #e.g. [Input, Target, Pred]
    #save_mode: [Input, Target, Pred] #Does not work in this example

################## Training Params ##################
training:
    case: "dst_index" # [coronal_holes, dst_index]
    mode: "train" # [train, test, demo]
    n_epochs: 20  
    batchSize: 64
    print_every: 10 #Distance of iters to print
    loss: "1*MSE" # [L1, MSE, BCEWithLogitsLossPadding, BCEWithLogitsLoss, DiceLoss, IoULoss]
    metrics: "MSE" # Metrics to use for your problem
    direction: "minimize" # ["minimize"]
    #The metric for deciding which is the best model in the case of multiple is the 1st.
    postprocess: "for_1cls"

  
optimizer:
    lr: 3e-3
    weight_decay: 5e-5
    lr_policy: "step"
    lr_decay_iters: '90-160'
    gamma: 0.1
    optim: 'RMSprop' #['SGD', 'ADAM', 'RMSprop']
    #For SGD
    momentum: 0.8
    #For ADAM
    betas: [0.9, 0.999]
    epsilon: 1e-8 #For ADAM + RMSprop

################## Model Params  #######################
model:
    type: LSTM
    input_nc: 5
    output_nc: 6
    depth: 3 #Number of layers of the model
    resume: false
    pretrained_dir: ""
    #If true, saves the model of each epoch else only the best and the latest
    save_all: false 
