Coronal Holes segmentation in SDO images.
==============================================

This section contains different configuration files for you to train a UNet for identifying
Coronal Holes in SDO Images.

-  To run the use-cases you need to initially download and store the data in your local machine. To do so run:

      -   Add a repository on your local machine.
      -   Split your data to IMGS/MASKS folders respectivelly to the content of tha data.

      Available data are `here <https://osf.io/48jyb/files/>`_.There are 3 choices, which contains different 
      amount of images to inference on:

      1) **CoronalHoles.zip**: 300 images
      2) **CoronalHoles_test.zip**: 28 images
      3) **Coronal_Holes_unsupervised_case**

      - The first 2 options are for the coronal holes supervised engine. The 3rd option is for the unsupervised use-case
      
-  Then modify the data path with yours in the configuration file (.yml) that you want to run.

-  Run the following command to start the training:
      -  aidapy --config examples/04_coronal_holes/config_coronal_holes.yml

-  To visualize metric and loss functions with tensorboard after training:
      -  go into the Experiments/CH_v0.5 folder
      -  tensorboard --logdir .

- For running training using Hyper Parameter Optimizations (HPO) run:
      -  aidapy --config examples/04_coronal_holes/config_coronal_holes_with_hpo.yml

- For pruning the model run:
      -  aidapy --config exampels/04_coronal_holes/config_coronal_holes_prune.yml

-  Finally run the following command to reproduce our results:
      -  aidapy --config exampels/04_coronal_holes/config_coronal_holes_test.yml


