.. _extreme_events:

Extreme events
============

Introduction
-------------
The ``xevents`` module provides the user with high order analysis tools for the detection of extreme events in spacecraft observations. This module makes use of high-order statistics such as intermittency measurements and a newly developed technique known as Partial Variance of Increments (PVI), applied here to several fluctuating quantities. The technique has extensively been used to identify magnetic reconnection events in the solar wind. This package will lead to the identification of large amounts of structures such as magnetic reconnection events, coronal mass ejections, shocks, fluid-like vortices, discontinuities, as well as large amplitude waves. The analysis is tested here on direct numerical simulations of turbulence, by using the virtual spacecraft technique (WP5), and to satellite data such as Wind, Magnetospheric Multiscale (**MMS**) mission, and Parker Solar Probe.

List of implemented routines
-------------

Here is a list of the implemented routines for extreme events analyses.

xevents.py
**********

+---------------------------------+---------------------------------+---------------------------------+---------------------------------+
|             Function            |                Input            |                 Output          |                Notes            |
+=================================+=================================+=================================+=================================+
| ``increm``                      | Time series stored in xarray    | An xarray containing the values | Calculates the increments of an |
|                                 | scale for the increments        | of the increments of the input  | xarray DataArray.               |
|                                 | computation.                    | time series in the requested    |                                 | 
|                                 | ``coord``: 3 spatial dimensions.| coordinate with length          |                                 |
|                                 | ``scale``: a scalar containing  | *N = Np -* ``scale``, with *Np* |                                 |
|                                 | the scale to compute.           | being the length of the input   |                                 |
|                                 |                                 | time series.                    |                                 |
|                                 |                                 |                                 |                                 |
|                                 |                                 |                                 |                                 |
|                                 |                                 | Kurtosis of the field increments|                                 |
|                                 |                                 | at scale ``scale``.             |                                 |
+---------------------------------+---------------------------------+---------------------------------+---------------------------------+
| ``pvi``                         | Time series stored in xarray    | An xarray containing the values | Calculates the PVI of an xarray |
|                                 | scale for the increments        | of the PVI of the total field   | DataArray.                      |
|                                 | computation.                    | vector with length              |                                 | 
|                                 | ``coord``: 3 spatial dimensions.| *N = Np -* ``scale``, with *Np* |                                 |
|                                 | ``scale``: a scalar containing  | being the length of the input   |                                 |
|                                 | the scale to compute.           | time series.                    |                                 |
+---------------------------------+---------------------------------+---------------------------------+---------------------------------+
| ``threshold``                   | Time series of the PVI (as      | A numpy array containing the    | Returns the locations at which  |
|                                 | computed from the PVI routine)  | location at which the PVI peaks | the PVI peaks exceeds the theta |
|                                 | stored in xarray format.        | above *theta*.                  | threshold.                      | 
|                                 |                                 |                                 |                                 |
|                                 | ``theta``: ``int`` is the       |                                 |                                 |
|                                 | threshold value for PVI.        |                                 |                                 |
+---------------------------------+---------------------------------+---------------------------------+---------------------------------+

statistics.py
*************

+---------------------------------+---------------------------------+---------------------------------+---------------------------------+
|             Function            |                Input            |                 Output          |                Notes            |
+=================================+=================================+=================================+=================================+
| ``autocorr``                    | Time series stored in xarray    | An xarray containing the value  | Calculates the autocorrelation  |
|                                 |                                 | of autocorrelation, where the   | of the xarray per column.       |
|                                 |                                 | index is the lag and the units  |                                 | 
|                                 | ``coord``: 3 spatial dimensions.| are the same as the input vector| This routine is included in     |
|                                 | ``scale``: a scalar containing  |                                 | ``statistics.py``, and has been |
|                                 | the scale to compute.           | The autocorrelation scale.      | updated for the computation     |
|                                 |                                 |                                 | of the autocorrelation scale,   |
|                                 |                                 |                                 | i.e., the scale at which the    |
|                                 |                                 |                                 | autocorrelation function decays |
|                                 |                                 |                                 | to a value *1/e*. This scale    |
|                                 |                                 |                                 | represents in turbulence the    |
|                                 |                                 |                                 | typical energy containg scale.  |  
+---------------------------------+---------------------------------+---------------------------------+---------------------------------+

