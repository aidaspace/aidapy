.. _energetic_particles_analyses:

Energetic particles analyses
============

Introduction
-------------
Below we describe a toolbox, based on two different algorithms, aimed at the identification of energetic particles and nonthermal effects. First, we recommmend to use the a code for the computation of the *\epsilon* parameter, a quick way to detect of non-Maxwellian features in particle distributions. These tools can be found in the ``epsilon.py`` function set. It is also possible to carry out a novel, a refined analysis, based on the spectral Hankel decomposition, in order to detect high energy particles and the process of entropy cascade in the velocity space. The analysis has been tested via synthetic data, direct numerical simulations of plasma turbulence at shocks, and via several space missions of the AIDA database.

List of implemented routines
-------------

Here is a list of the implemented routines for non-maxwellianity and energetic particles analyses.

epsilon.py
**********

+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
|             Function            |                Input            |                 Output          |                Notes             |
+=================================+=================================+=================================+==================================+
| ``compute_synth_maxw_1D``       | 1D velocity, temperature,       | A numpy array containing the    | Calculates the Maxwell-Boltzmann |
|                                 | mean velocity, thermal          | one dimensional                 | distribution for given density,  |
|                                 | velocity, particles' species.   | Maxwell-Boltzmann distribution. | mean velocity and temperature.   | 
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | The thermal velocity is defined  |
|                                 |                                 |                                 | as: .. math::                    |
|                                 |                                 |                                 | v_{th} = \sqrt{2 k_B TT/m}.      |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``particle`` can be *'p'* for    |
|                                 |                                 |                                 | ions or *'e'* for electrons.     |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``compute_synth_maxw_2D``       | ``vx``, ``vy``, temperature,    | A numpy array containing the    | Calculates the Maxwell-Boltzmann |
|                                 | mean velocity, thermal          | two dimensional                 | distribution for given density,  |
|                                 | velocity, particles' species.   | Maxwell-Boltzmann distribution. | mean velocity and temperature.   | 
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``vdrift`` must contain the mean |
|                                 |                                 |                                 | velocity in the two directions.  |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | The thermal velocity is defined  |
|                                 |                                 |                                 | as: .. math::                    |
|                                 |                                 |                                 | v_{th} = \sqrt{2 k_B TT/m}.      |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``particle`` can be *'p'* for    |
|                                 |                                 |                                 | ions or *'e'* for electrons.     |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``compute_synth_maxw_3D``       | ``vx``, ``vy``, ``vz``,         | A numpy array containing the    | Calculates the Maxwell-Boltzmann |
|                                 | temperature, mean velocity,     | three dimensional               | distribution for given density,  |
|                                 | thermal velocity, particles'    | Maxwell-Boltzmann distribution. | mean velocity and temperature.   | 
|                                 | species,                        |                                 |                                  |
|                                 | ``geom`` (default = ``None``).  |                                 | ``vdrift`` must contain the mean |
|                                 |                                 |                                 | velocity in the 3 directions.    |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | The thermal velocity is defined  |
|                                 |                                 |                                 | as: .. math::                    |
|                                 |                                 |                                 | v_{th} = \sqrt{2 k_B TT/m}.      |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``particle`` can be *'p'* for    |
|                                 |                                 |                                 | ions or *'e'* for electrons.     |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``geom``:``str`` chooses the     |
|                                 |                                 |                                 | geometry (``cart`` or ``spher``) |
|                                 |                                 |                                 | for calculating *epsilon*.       |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``comp_Kdistr``                 | Mean velocity, density,         | A numpy array containing the    | Calculates the K-distribution    |
|                                 | temperature, Boltzmann's        | K-distribution for a given *K*. | for given density, mean          |
|                                 | constant *kB*, mass, *kappa*,   |                                 | velocity, temperature and *K*.   | 
|                                 | coordinates,                    |                                 |                                  |
|                                 | ``geom`` (default = ``None``).  |                                 | ``geom`` must be set to ``cart`` |
|                                 |                                 |                                 | or ``spher`` for Cartesian or    |
|                                 |                                 |                                 | spherical geometry, respectively.|
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``compute_rho_vv``              | *VDF*, coordinates,             | A scalar and a numpy array      | Calculates the moments           |
|                                 | ``geom`` (default = ``None``).  | containing the density and the  | corresponding to density and     |
|                                 |                                 | three components of the mean    | mean velocity for a given VDF.   | 
|                                 |                                 | velocity, respectively.         |                                  |
|                                 |                                 |                                 | Uses trapezoidal integration     |
|                                 |                                 |                                 | method for ``geom`` = ``spher``. |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``geom`` must be set to ``cart`` |
|                                 |                                 |                                 | or ``spher`` for Cartesian or    |
|                                 |                                 |                                 | spherical geomety, respectively. |  
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``compute_temp``                | *VDF*, coordinates, mean        | A scalar containing *1/3* of    | Calculates the second order      |
|                                 | velocity, mass,                 | the trace of the temperature    | moment corresponding to          |
|                                 | constant *kB*,                  | tensor.                         | temperature. Outputs scalar *T*. | 
|                                 | ``geom`` (default= ``None``),   |                                 |                                  |
|                                 | ``components``                  |                                 | Uses trapezoidal integration     |
|                                 | (default= ``False``).           |                                 | method for ``geom`` = ``spher``. |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | ``geom`` must be set to ``cart`` |
|                                 |                                 |                                 | or ``spher`` for Cartesian or    |
|                                 |                                 |                                 | spherical geomety, respectively. | 
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | If ``components`` is set to      |
|                                 |                                 |                                 | ``True`` returns the values of   |
|                                 |                                 |                                 | temperature in the 3 directions. |
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | **NOTE** : to use it, the first  |
|                                 |                                 |                                 | order moments are needed. They   |
|                                 |                                 |                                 | can be computed with             |
|                                 |                                 |                                 | ``compute_rho_vv``.              |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``integrate_mms_th_phi``        | *VDF*, coordinates              | An array containing the VDF     | Integrates a VDF from **MMS**    |
|                                 |                                 | integrated in the angles        | catalogue in spherical           |
|                                 |                                 | *theta* and *phi*.              | coordinates.                     |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+
| ``epsilon``                     | *VDF*, associated Maxwell-      | A scalar containing the         | Calculates the deviation of the  |
|                                 | Boltzmann distribution,         | *epsilon* parameter.            | *VDF* from the equilibrium state,|
|                                 | coordinates, density,           |                                 | with respect to its associated   | 
|                                 | ``geom`` (default = ``None``),  |                                 | Maxwellian.                      |
|                                 | ``correct``                     |                                 |                                  |
|                                 | (default = ``False``).          |                                 |                                  |
|                                 |                                 |                                 | ``geom`` must be set to ``cart`` |
|                                 |                                 |                                 | or ``spher`` for Cartesian or    |
|                                 |                                 |                                 | spherical geomety, respectively. | 
|                                 |                                 |                                 |                                  |
|                                 |                                 |                                 | If ``correct`` is set to         |
|                                 |                                 |                                 | ``True`` corrects for missing    |
|                                 |                                 |                                 | data in *theta* or *v*.          |
+---------------------------------+---------------------------------+---------------------------------+----------------------------------+

hankel.py                                                                                                                              
*********

+---------------------------------+-----------------------------------+---------------------------------+-----------------------------------+
|             Function            |                Input              |                 Output          |                Notes              |
+=================================+===================================+=================================+===================================+
| ``interpolate_hankel``          | *VDF* integrated in               | An array containing the function| Finds *kmax* as the reciprocal    |
|                                 | (*f = f(v)*), coordinates,        | interpolated on the zeros of    | of minimum real-space spacing,    |
|                                 | factor for the *k* -range,        | *J0 (kmax v)*, a numpy array    | then builds the array of zeros    | 
|                                 | ``ntry``                          | containing the zeros of         | of *J0(kmax v)* and interpolates  |
|                                 | (default = ``100000000``).        | *J0(kmax v)*, a numpy array     | the function on the new grid.     |
|                                 |                                   | containing the zeros in the dual|                                   |
|                                 |                                   | space.                          | ``ntry`` is the number of         |
|                                 |                                   |                                 | attempts to find the zeros of     |
|                                 |                                   |                                 | *J0(kmax v)*.                     |
|                                 |                                   |                                 | This operation is to ensure       |
|                                 |                                   |                                 | the quadrature for the Hankel     |
|                                 |                                   |                                 | transform.                        | 
+---------------------------------+-----------------------------------+---------------------------------+-----------------------------------+
| ``integrate_hankel``            | *f* interpolated on the zeros of  | An array containing the         | Decomposes the one-dimensional    |
|                                 | *J0(kmax v)*, zeros of            | coefficients *H(k)* resulting   | *VDF* in Hankel functions.        |
|                                 | *J0(kmax v)*,.vector of the dual  | from the Hankel transform of    |                                   | 
|                                 | space (see ``interpolate_hankel`` | one-dimensional *VDF* *f(v)*.   | **Note**: only 1D VDFs can be     |
|                                 | function),                        |                                 | used, depending only on *v*.      |
|                                 | ``norm`` (default = ``False``),   | If ``inverse`` = ``True``       |                                   |
|                                 | ``inverse`` (dafult = ``False``). | returns the inverse trnsform    | If ``norm`` = ``True``, the       |
|                                 |                                   | *g(v)*.                         | normalised transform is performed.|
|                                 |                                   |                                 |                                   | 
+---------------------------------+-----------------------------------+---------------------------------+-----------------------------------+

